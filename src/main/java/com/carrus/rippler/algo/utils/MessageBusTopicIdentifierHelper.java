package com.carrus.rippler.algo.utils;

import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class MessageBusTopicIdentifierHelper {


    final static Logger logger = Logger.getLogger(MessageBusTopicIdentifierHelper.class);

    public static String getTopicIdentifer(Class exchangeClassName, CurrencyPair currencyPair, Enum operation){


        //Don't include classes package in topic label
        String prettyExchangeClassName =  exchangeClassName.getName().substring((exchangeClassName.getName().lastIndexOf(".")+1),exchangeClassName.getName().length());

        return prettyExchangeClassName + "_" + currencyPair.toString().replace("/", "") + (operation!=null?"_"+operation.name():"");

    }

    public static String getSummariesTopicIdentifier(Class exchangeClassName){

        String prettyExchangeClassName =  exchangeClassName.getName().substring((exchangeClassName.getName().lastIndexOf(".")+1),exchangeClassName.getName().length());

        return prettyExchangeClassName+"_MarketSummaries";

    }

    public static String getOrderBookTargetTopicIdentifier(Class exchangeClassName, CurrencyPair currencyPair, Enum side){

        String prettyExchangeClassName =  exchangeClassName.getName().substring((exchangeClassName.getName().lastIndexOf(".")+1),exchangeClassName.getName().length());

        String topic = prettyExchangeClassName+"_"+currencyPair.toString().replace("/","")+"_"+side.name()+"OrderBookMetric";
        logger.info("OrderTopic: " + topic);

        return topic;


    }


}
