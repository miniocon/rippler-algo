package com.carrus.rippler.algo.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class PriceRoundingHelper {


    public static BigDecimal getRoundedAndFormattedPrice(BigDecimal price){


        return price.setScale(8, RoundingMode.HALF_UP).stripTrailingZeros();
    }

}
