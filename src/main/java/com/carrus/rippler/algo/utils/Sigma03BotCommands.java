package com.carrus.rippler.algo.utils;

import java.util.HashMap;
import java.util.Map;

public class Sigma03BotCommands {

    public static final String BALANCE = "/BALANCE";
    public static final String SELL = "/SELL";
    public static final String VROC = "/VROCS";
    public static final String PNL = "/PNL";
    public static final String STATUS = "/STATUS";
    public static final String ALGO_TRADE_UPDATE = "/ALGO_TRADE_UPDATE";
    public static final String ALGO_BUY = "/BUY";
    public static final String ALGO_CANCEL = "/CANCEL";


    public static Map<String, String> BOT_COMMANDS = new HashMap<String, String>();


    static{


        BOT_COMMANDS.put(BALANCE, BALANCE);
        BOT_COMMANDS.put(SELL, SELL);
        BOT_COMMANDS.put(VROC, VROC);
        BOT_COMMANDS.put(PNL, PNL);
        BOT_COMMANDS.put(STATUS, STATUS);
        BOT_COMMANDS.put(ALGO_TRADE_UPDATE,ALGO_TRADE_UPDATE);
        BOT_COMMANDS.put(ALGO_BUY,ALGO_BUY);
        BOT_COMMANDS.put(ALGO_CANCEL,ALGO_CANCEL);





    }




}
