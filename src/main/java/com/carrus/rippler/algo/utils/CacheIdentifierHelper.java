package com.carrus.rippler.algo.utils;

import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class CacheIdentifierHelper {

    final static Logger logger = Logger.getLogger(CacheIdentifierHelper.class);

    public static String getCacheIdentifer(Class exchangeClassName, CurrencyPair currencyPair, Enum cacheName){
        return exchangeClassName.getName() + "_" + currencyPair.toString().replace("/", "") + "_" + cacheName.name();
    }



    public static String getAlgoCacheMetricKey(String cacheId, Enum direction, Enum keySuffix){

        return cacheId+"_"+direction.name()+"_"+keySuffix.name();

    }


    public static String getCacheBalanceKey(Class exchangeSpecification, String walletId){


        return exchangeSpecification.getName()+"_"+walletId;
    }

    public static String getAlgoMultiplesKeyForCurrencyPair(Class algoStrategy, Enum side, CurrencyPair currencyPair){


        return algoStrategy.getName() + "_"+side.name()+"_"+currencyPair.toString().replace("/", "");

    }

    public static String getAlgoSpreadKeyForCurrencyPair(Class algoStrategy, CurrencyPair currencyPair){


        return algoStrategy.getName() + "_"+currencyPair.toString().replace("/", "");

    }

    public static String getBuyingStatusIdForCurrencyPairAndExchange(Class exchangeClassName,  CurrencyPair currencyPair){



        return exchangeClassName.getName() + "_" + currencyPair.toString().replace("/", "");

    }

    public static String getExchangeCurrencyBuysVolumeMapId(Class exchangeClassName, CurrencyPair currencyPair){


        return exchangeClassName.getName() + "_"+ currencyPair.toString().replace("/", "") + "_Buys_volume";

    }

    public static String getExchangeCurrencySellsVolumeMapId(Class exchangeClassName, CurrencyPair currencyPair){


        return exchangeClassName.getName() + "_"+ currencyPair.toString().replace("/", "") + "_Sells_volume";



    }


    public static String getExchangeCurrencyBuysFillCountMapId(Class exchangeClassName, CurrencyPair currencyPair){


        return exchangeClassName.getName() + "_"+ currencyPair.toString().replace("/", "") + "_Buys_fill_count";

    }

    public static String getExchangeCurrencySellsFillCountMapId(Class exchangeClassName, CurrencyPair currencyPair){


        return exchangeClassName.getName() + "_"+ currencyPair.toString().replace("/", "") + "_Sells_fill_count";



    }


    public static String getMarketSummaryMapId(Class exchangeClassName){


        return exchangeClassName.getName()+"_marketSummaryMap";

    }

    public static String getVROCForCurrencyExchange(Class exchangeClassName){


        return exchangeClassName.getName()+"_vrocCurrencyMap";

    }



    public static String getExchangeVolumeTrackerId(Class exchangeClassName){


        return exchangeClassName.getName()+"_volumeMap";

    }

    public static String getExchangeVolume5SecROCTrackerId(Class exchangeClassName){


        return exchangeClassName.getName()+"_volumeMap_FIVESECROC";

    }

    public static String getExchangeVolume50SecROCTrackerId(Class exchangeClassName){


        return exchangeClassName.getName()+"_volumeMap_FIFTYSECROC";

    }

    public static String getExchangeVolume5minSecROCTrackerId(Class exchangeClassName){


        return exchangeClassName.getName()+"_volumeMap_FIVEMINROC";

    }

    public static String getExchangeAggregatedSignalCount(Class exchangeClassName){

        return exchangeClassName.getName()+"_volumeMap_AGG_CURR_MAP";

    }

    public static String getMarketSummaryStatusTimerCache(Class exchangeClassName){

        return exchangeClassName.getSimpleName()+"_marketsummaryupdatedtimestamp";

    }

    public static String getOrderFeedStatusTimerCache(Class exchangeClassName){

        return exchangeClassName.getSimpleName()+"_orderfeedupdatedtimestamp";

    }

    public static String getAlgoReportForExchange(Class exchangeClassName){

        return exchangeClassName.getSimpleName()+"_algoMetrics";

    }

    public static String getSignalDetailMapForAlgoOne(Class exchangeClassName){


        return exchangeClassName.getSimpleName()+"_signalDetailsMapForAlgoOne";
    }

}
