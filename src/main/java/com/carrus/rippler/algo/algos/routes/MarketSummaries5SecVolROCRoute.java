package com.carrus.rippler.algo.algos.routes;


import com.carrus.rippler.algo.algos.processors.MarketSummaries5SecROCAggregatorProcessor;
import com.carrus.rippler.algo.algos.processors.MarketSummaries5SecROCProcessor;
import com.carrus.rippler.algo.algos.processors.MarketSummariesProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummaries5SecVolROCRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(MarketSummaries5SecVolROCRoute.class);


    private String targetEndpoint;
    private Class exchange;



    public MarketSummaries5SecVolROCRoute(Class exchange, String targetEndpoint){
        super();

        this.targetEndpoint = targetEndpoint;
        this.exchange = exchange;

    }


    public void configure() throws Exception {



        logger.info("*****Setting up MarketSummaries5SecVroc Route..with: *******" + exchange.getName() + ", " + this.targetEndpoint);



        from("timer://evaluateMultiples"+new Date().getTime()+"?fixedRate=true&period=5000&delay=30000").process(new MarketSummaries5SecROCProcessor(exchange)).choice().when().simple("${in.body} != null").marshal().json(JsonLibrary.Jackson).to(this.targetEndpoint);

        ///from("timer://evaluateMultiples"+new Date().getTime()+"?fixedRate=true&period=5000&delay=30000").process(new MarketSummaries5SecROCProcessor(exchange)).marshal().json(JsonLibrary.Jackson).to(this.targetEndpoint);


        logger.info("*****Completed setting up Route for MarketSummaries5SecVRoc Route...with ****" + exchange.getName()  + ", "+ this.targetEndpoint);



    }


}
