package com.carrus.rippler.algo.algos.transactioncostanalysis;

import com.carrus.rippler.algo.RipplerAlgo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TCACalculator {


    /**
     *
     *
     * @param counterCurrencyBalance - e.g. how much BTC we have right now
     * @param initialLeadingCurrencyBalance  - e.g. how much USDT we had before we bought BTC
     * @param priceSellingAt - e.g. the BTC price we are buying USDT at
     * @return
     */

    public static BigDecimal getTCAValue(BigDecimal counterCurrencyBalance, BigDecimal initialLeadingCurrencyBalance, BigDecimal priceSellingAt){


        if(counterCurrencyBalance==null || priceSellingAt==null || initialLeadingCurrencyBalance==null){

            return null;
        }
        /**
         * Amount to sell:
         * E.g. We hold btc, we are now looking to buy back USDT - hence selling btc - so we perform the calc:
         * btc balance * usdt Bid Price  e.g. 0.0054 * 7500 * 0.9975
         */
        BigDecimal amountToSell = calculateAmountToSellUsingBalanceForPNL(counterCurrencyBalance, priceSellingAt);

        if(amountToSell==null){

            return null;
        }


        /**
         * Balance Spread
         * e.g. What is the difference between the orginal USDT balance and the new USDT balance based on selling the countercurrency
         */
        BigDecimal balanceSpread = amountToSell.subtract(initialLeadingCurrencyBalance);

        if(balanceSpread==null){

            return null;
        }


        /**
         *
         * What is the percentage delta of the balance spread
         *
         */
        BigDecimal pnL = balanceSpread.divide(initialLeadingCurrencyBalance,8,RoundingMode.HALF_UP);


        return pnL;

    }

    /***
     * Example of calculating amount to buy:
     * 1. Say the available balance is in USDT of $95
     * 2. Say the price is 7500
     * 3. the calculated balance = 95/7500
     * 4. Including the fee = (95/7500) * ExchangeFeeConstant
     *
     *
     * @param availableBalance
     * @param price
     * @return
     */

    public static BigDecimal calculateAmountToBuyUsingBalance(BigDecimal availableBalance, BigDecimal price){

        if(availableBalance==null || price==null){

            return null;
        }else{

            //do none null logic
        }


            BigDecimal calculatedBalance = availableBalance.divide(price,8, RoundingMode.HALF_UP);

        //No need to do this as the profit takes this into account calculatedBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

            return calculatedBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

    }

    public static BigDecimal calculateAmountToSellUsingBalance(BigDecimal availableBalance){



        //No need to do this as the profit takes this into account calculatedBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

        return availableBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

    }




    /***
     * Example of calculating amount to sell:
     * 1. Say the available balance is in BTC 0.00543
     * 2. Say the price is 7500
     * 3. the calculated balance to buy in the leading currency = 0.00543 * 7500
     * 4. Including the fee = (0.00543*7500) * ExchangeFeeConstant
     *
     *
     * @param availableBalance
     * @param price
     * @return
     */

    private static BigDecimal calculateAmountToSellUsingBalanceForPNL(BigDecimal availableBalance, BigDecimal price){

        if(availableBalance==null || price == null){
            return null;
        }

        BigDecimal calculatedBalance = availableBalance.multiply(price);

        ///No need to do this as the profit target takes fee into account calculatedBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

        return calculatedBalance.multiply(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

    }


    public static BigDecimal getTargetPNLPrice(BigDecimal originalPrice){

        if(originalPrice==null){

            return null;
        }


        BigDecimal one = new BigDecimal(1);

        BigDecimal exchangeFee = one.subtract(RipplerAlgo.BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC).setScale(8, BigDecimal.ROUND_HALF_UP);

        BigDecimal PNLPercentageTarget = RipplerAlgo.RIPPLER_HARVEST_PROFIT;

        BigDecimal combinedPNLFactor = exchangeFee.add(PNLPercentageTarget).setScale(8, BigDecimal.ROUND_HALF_UP);;

        return originalPrice.multiply(combinedPNLFactor).setScale(8, BigDecimal.ROUND_HALF_UP);


    }


}
