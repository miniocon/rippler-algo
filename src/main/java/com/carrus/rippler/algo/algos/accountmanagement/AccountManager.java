package com.carrus.rippler.algo.algos.accountmanagement;


import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.log4j.Logger;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.dto.account.AccountInfo;
import org.knowm.xchange.dto.account.Balance;
import org.knowm.xchange.service.account.AccountService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

/**
 * AccountManager per wallet per exchange
 *
 * e.g. Bittrex will have one Account Manager per wallet on the Exchange
 */

public class AccountManager {


    //Todo - extend to support multiple wallets

    final static Logger logger = Logger.getLogger(AccountManager.class);

    private String apiKey;
    private String apiSecret;
    private Class exchangeSpecification;
    private Exchange exchange;
    private HazelcastInstance distributedCache;
    private AccountService accountService;
    private String walletId;


    public AccountManager(Class exchangeSpecification, String apiKey, String apiSecret, HazelcastInstance distributedCache, String walletId){

        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
        this.exchangeSpecification = exchangeSpecification;
        this.exchange = initialiseExchange(exchangeSpecification,apiKey,apiSecret);
        this.distributedCache = distributedCache;
        this.accountService = exchange.getAccountService();

        try {
            if(walletId == null) {
                this.walletId = this.accountService.getAccountInfo().getWallet().getId();
            }else{
                this.walletId = walletId;
            }

            intialiseCurrencyBalances();

        } catch (IOException e) {
           logger.error("Error Obtaining wallet Id");
        }

    }


    public AccountInfo getAccountInfo(){


        try {
            AccountInfo accountInfo = accountService.getAccountInfo();
            logger.info(accountInfo);
            return accountService.getAccountInfo();

        } catch (IOException e) {
            logger.error("Account info not obtained", e);
            return null;
        }


    }



    public void updateInternalCacheBalanceForCurrencyFromExchange(Currency currency){

        logger.info("**UPDATING** balance cache from exchange for currency :" + currency + "current Balance = " +  getBalanceForExchangeWalletAndCurrencyFromInternalCache(currency));

        updateInternalCacheBalanceForIndividualCurrency(currency, getAvailableBalanceForCurrencyFromExchange(currency));

        logger.info("**UPDATED** balance cache from exchange for currency :" + currency + "new Balance = " +  getBalanceForExchangeWalletAndCurrencyFromInternalCache(currency));

    }



    public BigDecimal getBalanceForExchangeWalletAndCurrencyFromInternalCache(Currency currency){

        String cacheBalanceKey = CacheIdentifierHelper.getCacheBalanceKey(exchangeSpecification,  walletId);
        IMap<String, BigDecimal> currencyBalanceCache = distributedCache.getMap(cacheBalanceKey);

        return currencyBalanceCache.get(currency.getCurrencyCode());
    }


    /**
     * Maintain an internal balance cache to minimise contacting exchange
     * @param currency
     * @param decimal
     */
    private void updateInternalCacheBalanceForIndividualCurrency(Currency currency, BigDecimal decimal){
        logger.info("Updating Internal Cache Balance " + currency);

        String cacheBalanceKey = CacheIdentifierHelper.getCacheBalanceKey(exchangeSpecification,  walletId);

        IMap<String, BigDecimal> currencyBalanceCache = distributedCache.getMap(cacheBalanceKey);


        currencyBalanceCache.put(currency.getCurrencyCode(),decimal);

    }

    private BigDecimal getAvailableBalanceForCurrencyFromExchange(Currency currency) {
        logger.info("Obtaining Balance for " + currency);

        try {

            Balance availableBalance = accountService.getAccountInfo().getWallet().getBalance(currency);

            logger.info("Balance Obtained for " + currency + " is: " + availableBalance);
            if (availableBalance != null) {

                logger.info("available balance for currency: " + currency + " is: " + availableBalance.getAvailable());

                return availableBalance.getAvailable();
            } else {
                logger.info("no balance found for currency " + currency);
                return null;
            }


        } catch (IOException e) {
            logger.error("Problem obtaining balance for currency: " + currency, e);
            return null;
        }
    }

    public void intialiseCurrencyBalances(){

        try {

            logger.info("Initialising Balance Caches");
            Map<Currency, Balance> balancesForWallet =  accountService.getAccountInfo().getWallet(walletId).getBalances();
            String cacheBalanceKey = CacheIdentifierHelper.getCacheBalanceKey(exchangeSpecification,  walletId);

            IMap<String, BigDecimal> currencyBalanceCache = distributedCache.getMap(cacheBalanceKey);

            currencyBalanceCache.clear();


            for(Currency currency : balancesForWallet.keySet()){

               Balance aBalance = balancesForWallet.get(currency);
               currencyBalanceCache.put(currency.getCurrencyCode(),aBalance.getAvailable());
               logger.info("Currency: " + currency.getCurrencyCode() + " Balance: " + aBalance.getAvailable());
            }
            logger.info("Finished Initialising Balance Caches");

        } catch (IOException e) {
            logger.error("Error obtaining balances for Wallet", e);
        }


    }



    private  Exchange initialiseExchange(Class ExchangeSpecification, String apiKey, String apiSecret) {
        logger.info("Exchange Initialising.. ");

        ExchangeSpecification exSpec = new ExchangeSpecification(ExchangeSpecification);
        exSpec.setApiKey(apiKey);
        exSpec.setSecretKey(apiSecret);

        Exchange aExchange = ExchangeFactory.INSTANCE.createExchange(exSpec);
        logger.info("Exchange initialised.. " + aExchange);

        return aExchange ;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public Class getExchangeSpecification() {
        return exchangeSpecification;
    }

    public String getWalletId() {
        return walletId;
    }
}
