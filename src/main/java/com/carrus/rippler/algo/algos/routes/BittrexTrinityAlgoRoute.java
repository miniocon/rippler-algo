package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.tradingstrategies.TrinitySignalStrategy;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by usmankhan on 25/12/2017.
 */
public class BittrexTrinityAlgoRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(BittrexTrinityAlgoRoute.class);

    private String sourceRoute;
    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private Class exchange;


    public BittrexTrinityAlgoRoute(Class exchange, String sourceRoute, AccountManager accountManager, ExecutionEngine executionEngine){
        super();
        this.sourceRoute = sourceRoute;
        this.accountManager = accountManager;
        this.executionEngine = executionEngine;
        this.exchange = exchange;

    }


    public void configure() throws Exception {


        logger.info("Setting up route");

        //from("timer://evaluateOrderBooks"+new Date().getTime()+"?fixedRate=true&period=1000&delay=10000").

        from(sourceRoute)
                .unmarshal().json(JsonLibrary.Jackson)
                .process(new TrinitySignalStrategy(exchange, accountManager,executionEngine)).to("log:console");

        logger.info("completed Setting up route");
    }
}


