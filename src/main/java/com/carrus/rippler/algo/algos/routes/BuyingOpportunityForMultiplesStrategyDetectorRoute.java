package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.processors.BuyingOpportunityForMultiplesStrategyDetector;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.util.Date;

/**
 * Created by usmankhan on 25/11/2017.
 */
public class BuyingOpportunityForMultiplesStrategyDetectorRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(BuyingOpportunityForMultiplesStrategyDetectorRoute.class);

    private Class exchangeClassName;
    private CurrencyPair internalCurrencyPair;

    private String bidOrderBookCacheId;
    private String askOrderBookCacheId;

    private int pageSize;
    private String pollingFrequency;
    private String targetEndpoint;


    public BuyingOpportunityForMultiplesStrategyDetectorRoute(Class exchangeClassName, CurrencyPair internalCurrencyPair, String bidOrderBookCacheId, String askOrderBookCacheId, int pageSize, String pollingFrequency, String targetEndpoint){
        this.exchangeClassName = exchangeClassName;
        this.internalCurrencyPair = internalCurrencyPair;
        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.pageSize = pageSize;
        this.pollingFrequency = pollingFrequency;
        this.targetEndpoint = targetEndpoint;

    }


    public void configure() throws Exception {


        from("timer://evaluateBuyingOpportunity"+new Date().getTime()+"?fixedRate=true&period="+pollingFrequency+"&delay=30000").process(new BuyingOpportunityForMultiplesStrategyDetector(exchangeClassName, internalCurrencyPair,  bidOrderBookCacheId, askOrderBookCacheId,  pageSize)).to(this.targetEndpoint);


        logger.info("Successfully setup BuyingOpportunityForMultiplesStrategyDetectorRoute for " + internalCurrencyPair);





    }
}
