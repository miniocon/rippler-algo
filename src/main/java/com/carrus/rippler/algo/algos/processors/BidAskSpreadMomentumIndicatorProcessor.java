package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.Sigma03BotCommands;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BidAskSpreadMomentumIndicatorProcessor implements Processor {

    final static Logger logger = Logger.getLogger(BidAskSpreadMomentumIndicatorProcessor.class);

    private Class exchangeClassName;
    private CurrencyPair currencyPair;
    private BigDecimal spreadDelta = new BigDecimal(0.2);
    private BigDecimal fiveSecVRocThreshold = new BigDecimal(0.2);

    public BidAskSpreadMomentumIndicatorProcessor(Class exchangeClassName, CurrencyPair currencyPair){
        super();
        this.exchangeClassName = exchangeClassName;
        this.currencyPair = currencyPair;
        logger.info("Instantiated for: " + currencyPair);

    }





    @Override
    public void process(Exchange exchange) throws Exception {

        //logger.info("Process..");

        try {


            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

            IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            Boolean boostrapStatus = bootstrapStatusMap.get(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            IMap<String, BigDecimal> exchangeVolumes = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchangeClassName));

            IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

            IMap<String, MarketSummary> marketSummaryIMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getMarketSummaryMapId(exchangeClassName));

            IMap<String, BigDecimal> vrocCurrencyExchangeMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getVROCForCurrencyExchange(exchangeClassName));

            String bidCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.BIDS_SUM_ORDER_BOOK); // Constants.BID is direction
            String askCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.ASKS_SUM_ORDER_BOOK); // Constants.ASK is direction

            String topBidLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.BEST_LEVEL);

            String topAskLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.BEST_LEVEL);

            String top10BidSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.TOP_10_SUM);
            String top10AskSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.TOP_10_SUM);

            BigDecimal topBid = algoMetricsCache.get(topBidLeveKey);
            BigDecimal bestAsk = algoMetricsCache.get(topAskLeveKey);

            BigDecimal top10BidSum = algoMetricsCache.get(top10BidSumKey);
            BigDecimal top10AskSum = algoMetricsCache.get(top10AskSumKey);

            if(bestAsk==null || topBid==null){
                return;
            }else{

                //continue
            }


            BigDecimal bidAskSpread = bestAsk.subtract(topBid);

            if(bidAskSpread==null){

                return;
            }else{

                //continue
            }

            BigDecimal spreadDelta = bidAskSpread.divide(bestAsk, 8, BigDecimal.ROUND_HALF_UP);

            BigDecimal spreadPercentage = spreadDelta.multiply(new BigDecimal(100));

            BigDecimal bidAskMultiple = BigDecimal.ZERO;

            BigDecimal currencyPairMarketCap = exchangeVolumes.get(getMarketSummaryFormattedCurrencyPair(currencyPair));

            MarketSummary marketSummary = marketSummaryIMap.get(getMarketSummaryFormattedCurrencyPair(currencyPair));

            BigDecimal vrocForCurencyPair = vrocCurrencyExchangeMap.get(getMarketSummaryFormattedCurrencyPair(currencyPair));

            BigDecimal lastExecutedPrice = marketSummary != null ? marketSummary.getLast() : null;

            Map<String, Object> signalMap = new HashMap<String, Object>();

            if (top10BidSum != null && top10AskSum != null) {

                bidAskMultiple = top10BidSum.divide(top10AskSum, 8, BigDecimal.ROUND_HALF_UP);

                if (boostrapStatus) {

                    if (currencyPair.counter.equals(Currency.BTC)) {


                        if (sendSignal(currencyPair, bidAskMultiple, currencyPairMarketCap, spreadPercentage)) {


                            if (vrocForCurencyPair != null && ((vrocForCurencyPair.compareTo(fiveSecVRocThreshold) == 1) || (vrocForCurencyPair.compareTo(fiveSecVRocThreshold) == 0))) {

                                logger.info("CurrencyPair: " + currencyPair.toString() + ", Spread percentage: " + spreadPercentage + ", bidAskMultiple: " + bidAskMultiple + ", MCAP: " + currencyPairMarketCap + " LP: " + lastExecutedPrice + ", 5SecVroc: " + vrocForCurencyPair);

                                signalMap.put("exchange", exchangeClassName);
                                signalMap.put("COMMAND", Sigma03BotCommands.ALGO_BUY);
                                signalMap.put("currencyPair", currencyPair);
                                signalMap.put("baseCurrency", currencyPair.base.getSymbol());
                                signalMap.put("counterCurrency", currencyPair.counter.getSymbol());
                                signalMap.put("timestamp", new Date());

                                Map<String, BigDecimal> detailsMap = new HashMap<String, BigDecimal>();

                                detailsMap.put("spreadPercentage", spreadPercentage);
                                detailsMap.put("bidAskMultiple", bidAskMultiple);
                                detailsMap.put("MCAP", currencyPairMarketCap);
                                detailsMap.put("lastExecutedPrice", lastExecutedPrice);
                                detailsMap.put("vrocDelta", vrocForCurencyPair);


                                signalMap.put("details", detailsMap);


                                //Add to Map of values.. and send downstream..

                            } else {

                                //VROC to high

                            }
                        } else {
                            //Don't send signal yet
                        }


                    } else {

                        //Only supporting BTC pairs for now
                    }
                } else {
                    logger.info("CurrencyPair: " + currencyPair.toString() + " Bootstrapping so pausing on analysis");
                }
            } else {

                logger.info("top10BidSum or top10AskSum is empty");
            }


            /**
             * For this currency pair
             * Send a signal if the spread is < 0.02%
             *
             * Set the multiple
             *
             *
             * **/

            BigDecimal bidAskMultipleForCurrencyPair = RipplerAlgoAutoMultiCurrency.CURRENCY_PAIR_MULTIPLES.get(currencyPair.toString());
            if (bidAskMultipleForCurrencyPair == null) {
                bidAskMultipleForCurrencyPair = RipplerAlgoAutoMultiCurrency.DEFAULT_CURRENCY_PAIR_MUTLIPLE;

            } else {

                logger.info("BID ASK MULTIPLE Set for Currency: " + currencyPair + ": " + bidAskMultipleForCurrencyPair);


            }
            //TODO do same for DT Threshold


            if (signalMap.keySet().size() > 0) {

                exchange.getOut().setBody(signalMap);



            } else {
               // exchange.getOut().setBody(null);
            }


            //logger.info("Process.. end");
        }catch(Exception e){

            logger.error("Error processing.. ", e);
        }
    }


    private String getMarketSummaryFormattedCurrencyPair(CurrencyPair currencyPair){

        return currencyPair.counter + "-"+currencyPair.base;

    }


    private boolean sendSignal(CurrencyPair currencyPair, BigDecimal bidAskMultiple, BigDecimal marketCap, BigDecimal spreadPercentage){

        //TODO - If spread holds then Compare TopBid with next 4 Bids - confirm Top Bid to be < 0.5% different to the stack
        //If bid / ask multiple >1 & < 3 check that the market is > 300
        if(
                ((bidAskMultiple.compareTo(new BigDecimal(1)) == 1) || (bidAskMultiple.compareTo(new BigDecimal(1)) == 0)) &&

                        bidAskMultiple.compareTo(new BigDecimal(3)) == -1

                ){

            if(marketCap!=null && marketCap.compareTo(new BigDecimal(300)) ==1){

                if(spreadPercentage!=null && (spreadPercentage.compareTo(spreadDelta) ==-1 && spreadPercentage.compareTo(new BigDecimal(0)) ==1)) {

                    return true;
                }else{
                    //spread percentage still too high
                }

            }else{

                //conditions not met
            }


        }else if (

                ((bidAskMultiple.compareTo(new BigDecimal(3)) == 1) || (bidAskMultiple.compareTo(new BigDecimal(3)) == 0))      &&
                            bidAskMultiple.compareTo(new BigDecimal(6)) == -1

                ){

            if(marketCap!=null && marketCap.compareTo(new BigDecimal(150)) ==1){

                if(spreadPercentage!=null && (spreadPercentage.compareTo(spreadDelta) ==-1 && spreadPercentage.compareTo(new BigDecimal(0))==1) ) {

                    return true;
                }else{
                    //spread percentage still too high
                }

            }else{

                //conditions not met
            }




        }else if (

                ((bidAskMultiple.compareTo(new BigDecimal(6)) == 1)  || (bidAskMultiple.compareTo(new BigDecimal(6)) == 0))     &&
                        bidAskMultiple.compareTo(new BigDecimal(12)) == -1

                ){

            if(marketCap!=null && marketCap.compareTo(new BigDecimal(100)) ==1){

                if(spreadPercentage!=null && (spreadPercentage.compareTo(spreadDelta) ==-1 && spreadPercentage.compareTo(new BigDecimal(0)) ==1)  ) {

                    return true;
                }else{
                    //spread percentage still too high
                }

            }else{

                //conditions not met
            }




        }else if (

                ((bidAskMultiple.compareTo(new BigDecimal(12)) == 1)   ||  (bidAskMultiple.compareTo(new BigDecimal(12)) == 0))  &&
                        bidAskMultiple.compareTo(new BigDecimal(100)) == -1

                ){

            if(marketCap!=null && marketCap.compareTo(new BigDecimal(30)) ==1){

                if(spreadPercentage!=null && (spreadPercentage.compareTo(spreadDelta) ==-1 && spreadPercentage.compareTo(new BigDecimal(0)) ==1)  ) {

                    return true;
                }else{
                    //spread percentage still too high
                }

            }else{

                //conditions not met
            }




        }else{

            //conditions
        }



        return false;
    }
}
