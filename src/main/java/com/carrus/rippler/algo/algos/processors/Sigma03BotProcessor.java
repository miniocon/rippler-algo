package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.Sigma03BotCommands;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.util.CharArrayMap;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Sigma03BotProcessor implements Processor {

    final static Logger logger = Logger.getLogger(Sigma03BotProcessor.class);

    private Class exchangeClass;
    private String walletId;
    private PagingPredicate descendingVrocPredicate;

    public Sigma03BotProcessor(Class exchangeClass, String walletId){
        super();
        this.exchangeClass = exchangeClass;
        this.walletId = walletId;

        BigDecimal zero = new BigDecimal(0);

        //TDOO - how to account for null values in entry and value..

        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {



                return ((BigDecimal) e2.getValue()).compareTo((BigDecimal) e1.getValue());

            }
        };





        descendingVrocPredicate = new PagingPredicate(descendingComparator, 10);


    }


    @Override
    public void process(Exchange exchange) throws Exception {

        try {

            if (exchange != null && exchange.getIn() != null && exchange.getIn().getBody() != null) {
                logger.info(exchange.getIn().getBody().getClass());

                Map<String, Object> sigma03Report = new HashMap<String, Object>();


                HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

                IMap<String, BigDecimal> vrocCurrencyExchangeMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getVROCForCurrencyExchange(exchangeClass));

                IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

                IMap<String, BigDecimal> exchangeVolumes = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchangeClass));


                try {


                    //Collection<BigDecimal> descendingVrocKeys = vrocCurrencyExchangeMapNoneNulls.values(descendingVrocPredicate);


                    //Good example of filtering
                    //EntryObject e = new PredicateBuilder().getEntryObject();
                    //Predicate predicate = e.is( "active" ).and( e.get( "age" ).lessThan( 30 ) );


                    Map<String, Object> commandMessage = (Map<String, Object>) exchange.getIn().getBody();


                    logger.info("Command recieved from Sigma03Bot: " + commandMessage);

                    String command = (String) commandMessage.get("COMMAND");

                    logger.info("Command received: " + command);

                    Integer chatId = (Integer) commandMessage.get("CHAT_ID");

                    logger.info("Command: " + command);
                    logger.info("chatId:" + chatId);

                    sigma03Report = new HashMap<String, Object>();

                    if (Sigma03BotCommands.BALANCE.equals(command)) {

                        String cacheBalanceKey = CacheIdentifierHelper.getCacheBalanceKey(exchangeClass, walletId);
                        IMap<String, BigDecimal> currencyBalanceCache = distributedCacheInstance.getMap(cacheBalanceKey);

                        BigDecimal zeroBalance = new BigDecimal(0);

                        Map<String, BigDecimal> noneZeroCurrencyBalances = new HashMap<String, BigDecimal>();

                        for (String currencyKey : currencyBalanceCache.keySet()) {

                            BigDecimal aBalance = currencyBalanceCache.get(currencyKey);

                            if (aBalance.compareTo(zeroBalance) == 1) {
                                noneZeroCurrencyBalances.put(currencyKey, aBalance);
                            }

                        }

                        sigma03Report.put("CHAT_ID", chatId);
                        sigma03Report.put("ORIGINAL_COMMAND", command);
                        sigma03Report.put("BALANCES", noneZeroCurrencyBalances);


                    } else if (Sigma03BotCommands.VROC.equals(command)) {

                        Set<Map.Entry<String, BigDecimal>> vrocSet = vrocCurrencyExchangeMap.entrySet(descendingVrocPredicate);


                        Map<String, BigDecimal> top10DescendingVrocs = new HashMap<String, BigDecimal>();

                        String vrocs = new String();

                        for (Map.Entry<String, BigDecimal> entry : vrocSet) {

                            logger.info(entry);

                            CurrencyPair currencyPair = getCurrencyPairFromStringKey(entry.getKey());


                            String bidCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.BIDS_SUM_ORDER_BOOK); // Constants.BID is direction
                            String askCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.ASKS_SUM_ORDER_BOOK); // Constants.ASK is direction

                            String top10BidSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.TOP_10_SUM);
                            String top10AskSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.TOP_10_SUM);

                            BigDecimal top10BidSum = algoMetricsCache.get(top10BidSumKey);
                            BigDecimal top10AskSum = algoMetricsCache.get(top10AskSumKey);

                            BigDecimal bidAskMultiple = BigDecimal.ZERO;

                            if (top10BidSum != null && top10AskSum != null) {

                                bidAskMultiple = top10BidSum.divide(top10AskSum, 8, BigDecimal.ROUND_HALF_UP);

                            } else {

                                //cant calc bidAskMultiple
                            }


                            BigDecimal mcap = exchangeVolumes.get(entry.getKey());


                            vrocs += entry.getKey() + " : " + entry.getValue().setScale(2, BigDecimal.ROUND_UP) + "% ," + bidAskMultiple.setScale(2, BigDecimal.ROUND_UP) + "," + (mcap != null ? mcap.setScale(2, RoundingMode.HALF_UP) : "") + "\n";

                            top10DescendingVrocs.put(entry.getKey(), entry.getValue());

                        }

                        String algoMetricsId = CacheIdentifierHelper.getAlgoReportForExchange(BittrexExchange.class);
                        IMap<String, Object> algoMetrics = distributedCacheInstance.getMap(algoMetricsId);

                        sigma03Report.put("CHAT_ID", chatId);
                        sigma03Report.put("ORIGINAL_COMMAND", command);
                        sigma03Report.put("ALGO_STATUS", algoMetrics.get("ALGO_STATE"));
                        sigma03Report.put("VROCS", vrocs);


                    } else if (Sigma03BotCommands.PNL.equals(command)) {


                        String algoMetricsId = CacheIdentifierHelper.getAlgoReportForExchange(BittrexExchange.class);

                        IMap<String, Object> algoMetrics = distributedCacheInstance.getMap(algoMetricsId);


                        sigma03Report.put("CHAT_ID", chatId);
                        sigma03Report.put("ORIGINAL_COMMAND", command);
                        sigma03Report.put("ALGO_STATUS", algoMetrics.get("ALGO_STATE"));
                        sigma03Report.put("ALGO_PNL", algoMetrics.get("PNL"));
                        sigma03Report.put("CURRENCY_PAIR", algoMetrics.get("CURRENCY_PAIR"));
                        sigma03Report.put("ORIGINAL_PRICE", algoMetrics.get("ORIGINAL_PRICE"));
                        sigma03Report.put("TARGET_PRICE", algoMetrics.get("TARGET_PRICE"));
                        sigma03Report.put("PURCHASED_TIME", algoMetrics.get("PURCHASED_TIME"));


                    } else if (Sigma03BotCommands.STATUS.equals(command)) {


                        String algoMetricsId = CacheIdentifierHelper.getAlgoReportForExchange(BittrexExchange.class);

                        IMap<String, Object> algoMetrics = distributedCacheInstance.getMap(algoMetricsId);

                        sigma03Report.put("CHAT_ID", chatId);
                        sigma03Report.put("ORIGINAL_COMMAND", command);
                        sigma03Report.put("CURRENCY_PAIR", algoMetrics.get("CURRENCY_PAIR"));
                        sigma03Report.put("ALGO_STATUS", algoMetrics.get("ALGO_STATE"));


                    } else {

                        logger.info("Command not understood");
                    }

                    exchange.getOut().setBody(sigma03Report);

                } catch (Exception e) {

                    logger.error("Error processing: ", e);

                }


            }

        }catch(Exception e){

            logger.error("Error processing..", e);
        }

    }


    private CurrencyPair getCurrencyPairFromStringKey(String key){

        String [] keys = key.split("-");

        return new CurrencyPair(keys[1], keys[0]);


    }
}

