package com.carrus.rippler.algo.algos.routes;


import com.carrus.rippler.algo.algos.processors.Sigma03BotAlgoSignalResponseConsumer;
import com.carrus.rippler.algo.algos.processors.Sigma03BotProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;


/**
 * TODO: This route listens to chat bot messages that will specify multiples in realtime
 */
public class  Sigma03BoAlgoSignalResponseConsumertRoute extends RouteBuilder {

    final static Logger logger = Logger.getLogger(Sigma03BoAlgoSignalResponseConsumertRoute.class);

    private String sourceEndPoint;
    private String targetEndpoint;
    private Class exchangeClass;


    public Sigma03BoAlgoSignalResponseConsumertRoute(String sourceEndpoint, String targetEndpoint, Class exchangeClass){

        this.sourceEndPoint = sourceEndpoint;
        this.targetEndpoint = targetEndpoint;
        this.exchangeClass = exchangeClass;


    }


    public void configure() throws Exception {



        //Route which recieves chat commands
        from(sourceEndPoint).process(new Sigma03BotAlgoSignalResponseConsumer(exchangeClass)).choice().when().simple("${in.body} != null").marshal().json(JsonLibrary.Jackson).to(targetEndpoint);




        logger.info("Route setup complete: " + sourceEndPoint + ", " + targetEndpoint);




    }
}
