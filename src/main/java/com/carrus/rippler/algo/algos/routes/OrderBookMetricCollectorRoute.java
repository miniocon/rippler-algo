package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.processors.OrderBookMetricCollector;
import com.carrus.rippler.algo.algos.processors.OrderBookViewerProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

/**
 * Created by usmankhan on 28/10/2017.
 */
public class OrderBookMetricCollectorRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(OrderBookMetricCollectorRoute.class);

    private String sourceRoute;
    private String orderBookCacheId;
    private Enum direction;


    public OrderBookMetricCollectorRoute(String sourceRoute, String orderBookCacheId, Enum direction){
        super();
        this.sourceRoute = sourceRoute;
        this.orderBookCacheId = orderBookCacheId;
        this.direction = direction;

    }


    public void configure() throws Exception {

        logger.info("ROUTE Created for " + this.sourceRoute);

        from("direct:"+this.sourceRoute).process(new OrderBookMetricCollector(orderBookCacheId, direction)).to("log:console");


    }
}
