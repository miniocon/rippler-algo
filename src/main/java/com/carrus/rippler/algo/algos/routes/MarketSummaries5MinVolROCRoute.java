package com.carrus.rippler.algo.algos.routes;


import com.carrus.rippler.algo.algos.processors.MarketSummaries5MinROCProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummaries5MinVolROCRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(MarketSummaries5MinVolROCRoute.class);


    private String targetEndpoint;
    private Class exchange;



    public MarketSummaries5MinVolROCRoute(Class exchange, String targetEndpoint){
        super();

        this.targetEndpoint = targetEndpoint;
        this.exchange = exchange;

    }


    public void configure() throws Exception {



        logger.info("*****Setting up MarketSummaries5MinVroc Route..with: *******" + exchange.getName() + ", " + this.targetEndpoint);



        from("timer://evaluateMultiples5min"+new Date().getTime()+"?fixedRate=true&period=300000&delay=30000").process(new MarketSummaries5MinROCProcessor(exchange)).to(this.targetEndpoint);


        logger.info("*****Completed setting up Route for MarketSummaries5MinVRoc Route...with ****" + exchange.getName()  + ", "+ this.targetEndpoint);



    }


}
