package com.carrus.rippler.algo.algos.statemanagement;

import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by usmankhan on 05/11/2017.
 */
public class StateEngine {

    private Enum currentState;
    private BigDecimal priceBoughtAt;
    private BigDecimal amountBought;
    private BigDecimal amountToSell;
    private BigDecimal originalLeadingCurrencyBalance;
    private String uuIdOfBidLimitOrderToProcess;
    private String uuIdOfAskLimitOrderToProcess;
    private Date timeBoughtAt;
    private Date timeSoldAt;
    private CurrencyPair currencyPair;


    public StateEngine(){

        currentState = AlgoStates.ANALYSING_MARKET;
    }


    public void setCurrentState(Enum algoState){


        this.currentState = algoState;

    }


    public Enum getCurrentState(){

        return currentState;

    }

    public BigDecimal getPriceBoughtAt() {
        return priceBoughtAt;
    }

    public void setPriceBoughtAt(BigDecimal priceBoughtAt) {
        this.priceBoughtAt = priceBoughtAt;
    }

    public BigDecimal getAmountBought() {
        return amountBought;
    }

    public void setAmountBought(BigDecimal amountBought) {
        this.amountBought = amountBought;
    }

    public BigDecimal getOriginalLeadingCurrencyBalance() {
        return originalLeadingCurrencyBalance;
    }

    public void setOriginalLeadingCurrencyBalance(BigDecimal originalLeadingCurrencyBalance) {
        this.originalLeadingCurrencyBalance = originalLeadingCurrencyBalance;
    }

    public String getUuIdOfBidLimitOrderToProcess() {
        return uuIdOfBidLimitOrderToProcess;
    }

    public void setUuIdOfBidLimitOrderToProcess(String uuIdOfBidLimitOrderToProcess) {
        this.uuIdOfBidLimitOrderToProcess = uuIdOfBidLimitOrderToProcess;
    }

    public String getUuIdOfAskLimitOrderToProcess() {
        return uuIdOfAskLimitOrderToProcess;
    }

    public void setUuIdOfAskLimitOrderToProcess(String uuIdOfAskLimitOrderToProcess) {
        this.uuIdOfAskLimitOrderToProcess = uuIdOfAskLimitOrderToProcess;
    }

    public BigDecimal getAmountToSell() {
        return amountToSell;
    }

    public void setAmountToSell(BigDecimal amountToSell) {
        this.amountToSell = amountToSell;
    }

    public Date getTimeBoughtAt() {
        return timeBoughtAt;
    }

    public void setTimeBoughtAt(Date timeBoughtAt) {
        this.timeBoughtAt = timeBoughtAt;
    }

    public Date getTimeSoldAt() {
        return timeSoldAt;
    }

    public void setTimeSoldAt(Date timeSoldAt) {
        this.timeSoldAt = timeSoldAt;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public void reset(){

        currentState = AlgoStates.ANALYSING_MARKET;
        priceBoughtAt = null;
        amountBought = null;
        originalLeadingCurrencyBalance = null;
        uuIdOfAskLimitOrderToProcess = null;
        uuIdOfBidLimitOrderToProcess = null;
        amountToSell = null;
        this.timeBoughtAt = null;
        this.timeSoldAt = null;
        this.currencyPair = null;


    }
}
