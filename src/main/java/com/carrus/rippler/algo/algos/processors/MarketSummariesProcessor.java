package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummariesProcessor implements Processor {


    final static Logger logger = Logger.getLogger(MarketSummariesProcessor.class);

    private Gson gson;
    private Class exchangeClass;

    public MarketSummariesProcessor(Class exchangeClass){

        this.exchangeClass = exchangeClass;
        gson = new Gson();

    }


    public void process(Exchange exchange) throws Exception {

        try {



            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

           IMap<String, BigDecimal> exchangeVolumes = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchangeClass));

            IMap<String, MarketSummary> marketSummaryIMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getMarketSummaryMapId(exchangeClass));

            IMap<String, Date> marketSummaryUpdateMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getMarketSummaryStatusTimerCache(exchangeClass));



            JsonElement jsonElement = gson.toJsonTree((Map)exchange.getIn().getBody());


            MarketSummary marketSummary = gson.fromJson(jsonElement, MarketSummary.class);

            marketSummaryIMap.put(marketSummary.getMarketName(), marketSummary);

            exchangeVolumes.put(marketSummary.getMarketName(), marketSummary.getBaseVolume());

            logger.debug(marketSummary.getMarketName()+ ", " + exchangeVolumes.get(marketSummary.getMarketName()));

            marketSummaryUpdateMap.put(Constants.LAST_MARKET_SUMMARY_UPDATE.name(), new Date());


        }catch(Exception e){

            logger.error("Problem with market summaries", e);
        }


    }
}
