package com.carrus.rippler.algo.algos.statemanagement;

/**
 * Created by usmankhan on 05/11/2017.
 */
public enum AlgoStates {

    ANALYSING_MARKET,
    BID_LIMIT_ORDER_PROCESSING,
    ASK_LIMIT_ORDER_PROCESSING,
    TEST_LIMIT_ORDER_PROCESSING,
    BOUGHT,
    SOLD,
    STOP_TRADING

}
