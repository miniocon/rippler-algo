package com.carrus.rippler.algo.algos.execution;

import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.statemanagement.AlgoStates;
import com.carrus.rippler.algo.algos.statemanagement.StateEngine;
import com.carrus.rippler.algo.algos.transactioncostanalysis.TCACalculator;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;

public class ManualLimitOrderPlacementProcessor implements Processor {

    final static Logger logger = Logger.getLogger(ManualLimitOrderPlacementProcessor.class);

    private CurrencyPair currencyPair;
    private Currency leadingCurrency;
    private Currency counterCurrency;
    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private StateEngine stateEngine;
    private BigDecimal quantity;
    private BigDecimal price;
    private Enum side;

    public ManualLimitOrderPlacementProcessor(CurrencyPair internalCurrencyPairId, Currency leadingCurrency, Currency counterCurrency, AccountManager accountManager, BigDecimal quantity, BigDecimal price, Enum side){

        this.currencyPair  = internalCurrencyPairId;
        this.leadingCurrency = leadingCurrency;
        this.counterCurrency = counterCurrency;
        this.accountManager = accountManager;
        this.quantity = quantity;
        this.price = price;
        this.side = side;
        this.executionEngine = new ExecutionEngine(accountManager);
        stateEngine = new StateEngine();

    }


    public void process(Exchange exchange) throws Exception {

        try{



            if(             AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState())

                    ){


                       logger.info("Place Limit Order: " + side + " " + currencyPair + ", quantity: " + quantity+ ", price: " + price);

                       //place Limit Order Bid
                        String bidUuId = null;

                        Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.BID,currencyPair,quantity,price);

                        bidUuId = limitOrderResult.get("id");

                        logger.info("Successfully placed BID Limit order, uuId recieved: " + bidUuId);

                        stateEngine.setCurrentState(AlgoStates.TEST_LIMIT_ORDER_PROCESSING);
                        stateEngine.setUuIdOfBidLimitOrderToProcess(bidUuId);
                        stateEngine.setAmountBought(quantity);
                        stateEngine.setPriceBoughtAt(price);
                        stateEngine.setOriginalLeadingCurrencyBalance(accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(leadingCurrency));





            }else if(


                            AlgoStates.TEST_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())


                    ){


                       //check state of the limit order - once it is filled, the balances will update
                        if(executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfBidLimitOrderToProcess(), currencyPair, stateEngine.getAmountBought())){

                            //Make sure balances for currencies are upto date
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(leadingCurrency);
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(counterCurrency);


                            logger.info(leadingCurrency + " Balance: " + accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(leadingCurrency));
                            logger.info(counterCurrency + "Balance: " + accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(counterCurrency));

                            stateEngine.reset();
                            stateEngine.setCurrentState(AlgoStates.STOP_TRADING);

                        }else{

                            logger.info("order still not filled");
                        }



            }else if(AlgoStates.STOP_TRADING.equals(stateEngine.getCurrentState())){

                logger.info("******LIMIT ORDER TEST COMPLETED _ ALGO HAS STOPPED TRADING & NEEDS TO BE RESTARTED WITH THIS TEST SET TO OFF ****");




            }else{


                logger.debug("Still Analysing Market as Buying conditions not met");
            }




        }catch(Exception e){

            logger.error("Error processing multiples Algo strategy ", e);
        }



    }
}
