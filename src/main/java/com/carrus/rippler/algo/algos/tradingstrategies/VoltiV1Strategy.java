package com.carrus.rippler.algo.algos.tradingstrategies;

import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.statemanagement.AlgoStates;
import com.carrus.rippler.algo.algos.statemanagement.StateEngine;
import com.carrus.rippler.algo.algos.transactioncostanalysis.TCACalculator;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;

public class VoltiV1Strategy implements Processor {

    final static Logger logger = Logger.getLogger(VoltiV1Strategy.class);

    private CurrencyPair currencyPair;
    private Currency leadingCurrency;
    private Currency counterCurrency;
    private String bidOrderBookCacheId;
    private String askOrderBookCacheId;
    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private PagingPredicate bidPagingPredicate;
    private PagingPredicate askPagingPredicate;
    private int pageSize;
    private StateEngine stateEngine;

    public VoltiV1Strategy(CurrencyPair internalCurrencyPairId, Currency leadingCurrency, Currency counterCurrency, String bidOrderBookCacheId, String askOrderBookCacheId, AccountManager accountManager, int pageSize){

        this.currencyPair  = internalCurrencyPairId;
        this.leadingCurrency = leadingCurrency;
        this.counterCurrency = counterCurrency;
        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.accountManager = accountManager;
        this.executionEngine = new ExecutionEngine(accountManager);
        this.pageSize = pageSize;
        stateEngine = new StateEngine();

        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {


                return ((BigDecimal)e2.getKey()).compareTo((BigDecimal)e1.getKey());

            }
        };

        Comparator<Map.Entry> ascendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {

                return ((BigDecimal)e1.getKey()).compareTo((BigDecimal)e2.getKey());

            }
        };

        askPagingPredicate = new PagingPredicate(ascendingComparator, pageSize);
        bidPagingPredicate = new PagingPredicate(descendingComparator, pageSize);



    }


    public void process(Exchange exchange) throws Exception {

        try{

            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

            /**
            IMap<BigDecimal, BigDecimal> bidOrderBook = distributedCacheInstance.getMap(bidOrderBookCacheId);
            IMap<BigDecimal, BigDecimal> askOrderBook = distributedCacheInstance.getMap(askOrderBookCacheId);

            Collection<BigDecimal> bidOrderBookTopPrices = bidOrderBook.keySet(bidPagingPredicate);

            Collection<BigDecimal> askOrderBookTopPrices = askOrderBook.keySet(askPagingPredicate);


            Collection<BigDecimal> bidOrderBookTopQuantities = bidOrderBook.values(askPagingPredicate);

            Collection<BigDecimal> askOrderBookTopQuantities = askOrderBook.values(askPagingPredicate);
             **/

            IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

            String algoBestBidLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidOrderBookCacheId,Constants.BID,Constants.BEST_LEVEL);
            String algoBestAskLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askOrderBookCacheId,Constants.ASK,Constants.BEST_LEVEL);
            String algoTop10BidSum = CacheIdentifierHelper.getAlgoCacheMetricKey(bidOrderBookCacheId,Constants.BID,Constants.TOP_10_SUM);
            String algoTop10AskSum = CacheIdentifierHelper.getAlgoCacheMetricKey(askOrderBookCacheId,Constants.ASK,Constants.TOP_10_SUM);

            BigDecimal bestBid = algoMetricsCache.get(algoBestBidLevelKey);
            BigDecimal bestAsk = algoMetricsCache.get(algoBestAskLevelKey);

            BigDecimal top10BidSum = algoMetricsCache.get(algoTop10BidSum);
            BigDecimal top10AskSum = algoMetricsCache.get(algoTop10AskSum);

            BigDecimal bidAskMultiple = top10BidSum.divide(top10AskSum,8,BigDecimal.ROUND_HALF_UP);


            BigDecimal bidAskSpread = bestBid.subtract(bestAsk);





            BigDecimal bidAskMultipleConstant = RipplerAlgo.BID_ALGO_MULTIPLES.get(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(VoltiV1Strategy.class, Constants.BID, currencyPair));

            BigDecimal spreadConstant = RipplerAlgo.ALGO_SPREAD_CONSTANT.get(CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(VoltiV1Strategy.class, currencyPair));


            logger.info("Best Bid for  "+ currencyPair+" "+ bestBid);
            logger.info("Best Ask for  " + currencyPair+" "+ bestAsk);
            logger.debug("top 10 Bid Sum " +currencyPair+" "+ top10BidSum);
            logger.debug("top 10 Ask Sum " + currencyPair+" "+top10AskSum);
            logger.info("Bid/Ask spread: " + currencyPair+" "+bidAskSpread);
            logger.info("Multiple for bid/ask Sum: " + currencyPair+" "+bidAskMultiple);
            logger.info("BidASKMultipleConstant:" + currencyPair+" "+ bidAskMultipleConstant);
            logger.info("spreadConstant:" + currencyPair+" "+ spreadConstant);
            logger.info("balance for: " + leadingCurrency + " " + accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(leadingCurrency));
            logger.info("balance for: " + counterCurrency + " " + accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(counterCurrency));
            /**
             *
             * If the Algo is in the Analysing Market State, check that the bid Ask multiple is >= the Bid ALGO multiple for this algo AND
             * check that the bid/ask spread is >= the spreadConstant for this algo
             *
             */

            if(             AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState()) &&

                            (bidAskMultiple.compareTo(bidAskMultipleConstant) ==0 || bidAskMultiple.compareTo(bidAskMultipleConstant) ==1) &&

                            (bidAskSpread.compareTo(spreadConstant) ==0 || bidAskSpread.compareTo(spreadConstant)==1)

                    ){


                        //Create a limitorder to buy at the current Ask

                        //Check the balance - do we have enough to buy at the current ask
                       BigDecimal balanceOfLeadingCurrency =  accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(leadingCurrency);


                       BigDecimal amountToBuy = TCACalculator.calculateAmountToBuyUsingBalance(balanceOfLeadingCurrency,bestAsk);

                       //THINK THIS IS WRONG _ SHOULD JUST BE ACTUAL BALANCE - BigDecimal amountToBuy = TCACalculator.calculateAmountToBuyUsingBalance(balanceOfLeadingCurrency, bestAsk);

                       logger.info("Place Limit Order: " + Constants.BID + " " + currencyPair + ", amountToBuy: " + amountToBuy+ ", askPrice: " + bestAsk);

                       //place Limit Order Bid
                        String bidUuId = null;

                        Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.BID,currencyPair,amountToBuy,bestAsk);

                        bidUuId = limitOrderResult.get("id");

                        logger.info("Successfully placed BID Limit order, uuId recieved: " + bidUuId);

                        stateEngine.setCurrentState(AlgoStates.BID_LIMIT_ORDER_PROCESSING);
                        stateEngine.setUuIdOfBidLimitOrderToProcess(bidUuId);
                        stateEngine.setAmountBought(amountToBuy);
                        stateEngine.setPriceBoughtAt(bestAsk);
                        stateEngine.setOriginalLeadingCurrencyBalance(accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(leadingCurrency));





            }else if(
                            AlgoStates.BOUGHT.equals(stateEngine.getCurrentState())


                    ){



                    BigDecimal balanceOfCounterCurrency = accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(counterCurrency);

                    BigDecimal pnl = TCACalculator.getTCAValue(balanceOfCounterCurrency, stateEngine.getOriginalLeadingCurrencyBalance(), bestBid);

                    logger.info("PNL val: " + pnl);

                    /*** If PNL is greater= to the harvest amount- place sell limit order and reset state ***/
                    if(pnl!=null && (pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT)==0 || pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT)==1)  ){

                        //THINK THIS IS WRONG _ SHOULD JUST BE ACTUAL BALANCE - BigDecimal amountToSell = TCACalculator.calculateAmountToSellUsingBalance(balanceOfCounterCurrency,bestBid);


                        //Amount to sell is always in counter currency
                        ////Dont need this - exchange takes care of fee BigDecimal amountToSell = TCACalculator.calculateAmountToSellUsingBalance(balanceOfCounterCurrency);

                        logger.info("PNL Target reached with PNL of " + pnl);
                        logger.info("Place Limit Order: " + Constants.ASK + " " + currencyPair + ", amountToSell: " + balanceOfCounterCurrency+ ", bidPrice: " + bestBid);

                        //place Limit Order Bid
                        String askUuId = null;

                        Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.ASK,currencyPair,balanceOfCounterCurrency,bestBid);

                        askUuId = limitOrderResult.get("id");

                        logger.info("Successfully places ASK Limit order, uuId recieved: " + askUuId);

                        stateEngine.setUuIdOfAskLimitOrderToProcess(askUuId);
                        stateEngine.setCurrentState(AlgoStates.ASK_LIMIT_ORDER_PROCESSING);
                        stateEngine.setAmountToSell(balanceOfCounterCurrency);


                    }else{

                        logger.info("pnl still not hit as pnl calculated is: " + pnl + " where target is " + RipplerAlgo.RIPPLER_HARVEST_PROFIT);

                    }


            }else if(


                            AlgoStates.BID_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())


                    ){


                       //check state of the limit order - once it is filled, the balances will update
                        if(executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfBidLimitOrderToProcess(), currencyPair, stateEngine.getAmountBought())){

                            //Make sure balances for currencies are upto date
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(leadingCurrency);
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(counterCurrency);
                            //Order has filled so update state to BOUGHT
                            stateEngine.setCurrentState(AlgoStates.BOUGHT);

                        }else{

                            logger.debug("BID order still not filled");
                        }



            }else if(


                        AlgoStates.ASK_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())

                    ){


                //check state of the limit order - once it is filled, the balances will update
                if(executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfAskLimitOrderToProcess(),currencyPair,stateEngine.getAmountToSell())){

                    //Make sure balances for currencies are upto date
                    accountManager.updateInternalCacheBalanceForCurrencyFromExchange(leadingCurrency);
                    accountManager.updateInternalCacheBalanceForCurrencyFromExchange(counterCurrency);

                    //Order has filled so reset State Engine, which defaults state to Analysing Market
                    stateEngine.reset();


                }else{

                    logger.debug("Ask order still not filled");
                }



            }else{


                logger.debug("Still Analysing Market as Buying conditions not met");
            }




        }catch(Exception e){

            logger.error("Error processing multiples Algo strategy ", e);
        }



    }
}
