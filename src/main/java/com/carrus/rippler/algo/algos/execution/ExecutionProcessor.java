package com.carrus.rippler.algo.algos.execution;

import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.trade.LimitOrder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Map;

public class ExecutionProcessor implements Processor {

    final static Logger logger = Logger.getLogger(ExecutionProcessor.class);

    private CurrencyPair currencyPair;
    private String bidOrderBookCacheId;
    private String askOrderBookCacheId;
    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private PagingPredicate bidPagingPredicate;
    private PagingPredicate askPagingPredicate;
    private int pageSize;

    public ExecutionProcessor(CurrencyPair currencyPair, String bidOrderBookCacheId, String askOrderBookCacheId, AccountManager accountManager, int pageSize){

        this.currencyPair  = currencyPair;
        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.accountManager = accountManager;
        this.executionEngine = new ExecutionEngine(accountManager);
        this.pageSize = pageSize;

        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {


                return ((BigDecimal)e2.getKey()).compareTo((BigDecimal)e1.getKey());

            }
        };

        Comparator<Map.Entry> ascendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {

                return ((BigDecimal)e1.getKey()).compareTo((BigDecimal)e2.getKey());

            }
        };

        askPagingPredicate = new PagingPredicate(ascendingComparator, pageSize);
        bidPagingPredicate = new PagingPredicate(descendingComparator, pageSize);



    }


    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        /**
        IMap<BigDecimal, BigDecimal> bidOrderBook = distributedCacheInstance.getMap(bidOrderBookCacheId);
        IMap<BigDecimal, BigDecimal> askOrderBook = distributedCacheInstance.getMap(askOrderBookCacheId);

        Collection<BigDecimal> bidOrderBookTopPrices = bidOrderBook.keySet(bidPagingPredicate);

        Collection<BigDecimal> askOrderBookTopPrices = askOrderBook.keySet(askPagingPredicate);


        Collection<BigDecimal> bidOrderBookTopQuantities = bidOrderBook.values(askPagingPredicate);

        Collection<BigDecimal> askOrderBookTopQuantities = askOrderBook.values(askPagingPredicate);
         **/





        IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

        String algoBesAskLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askOrderBookCacheId,Constants.ASK,Constants.BEST_LEVEL);

        BigDecimal bestAsk = algoMetricsCache.get(algoBesAskLevelKey);

        logger.info("Best Ask for " + askOrderBookCacheId + ": " + bestAsk);

        /**

        BigDecimal availableBalanceBTC = accountManager.getAvailableBalanceForCurrency(Currency.BTC);
        BigDecimal availableBalanceNEO = accountManager.getAvailableBalanceForCurrency(Currency.NEO);

        //lets buy some currency using the BTC balance
        BigDecimal amountToBuy = availableBalanceBTC.divide(bestAsk,8, RoundingMode.HALF_UP);

        logger.info ("Amount to buy " + amountToBuy);

       String uuId =  executionEngine.placeLimitOrder(Constants.BID,currencyPair,amountToBuy,bestAsk);

       if(uuId==null){
           logger.error ("order did not get placed");
       }

        availableBalanceBTC = accountManager.getAvailableBalanceForCurrency(Currency.BTC);
        availableBalanceNEO = accountManager.getAvailableBalanceForCurrency(Currency.NEO);

         **/







    }
}
