package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.Sigma03BotCommands;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Sigma03BotAlgoSignalResponseConsumer implements Processor {

    final static Logger logger = Logger.getLogger(Sigma03BotAlgoSignalResponseConsumer.class);

    private Class exchangeClass;


    public Sigma03BotAlgoSignalResponseConsumer(Class exchangeClass){
        super();
        this.exchangeClass = exchangeClass;
    }


    @Override
    public void process(Exchange exchange) throws Exception {

        if (exchange != null && exchange.getIn() != null && exchange.getIn().getBody() != null) {

            try {

                logger.info(exchange.getIn().getBody().getClass());

                HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");


                IMap<String, Object> reportToSendAlgo = distributedCacheInstance.getMap(Constants.REPORT_TO_SEND_TO_BOT.name());


                Map<String, Object> reportToSendAlgoCopy = new HashMap<String, Object>();

                Map<String, Object> sigma03ReportToSend = new HashMap<String, Object>();


                if(reportToSendAlgo.size()>0) {

                    sigma03ReportToSend.put("ORIGINAL_COMMAND", Sigma03BotCommands.ALGO_TRADE_UPDATE);


                    //copy the report to Send Algo and then blank it - so that it resets for next message..


                    for(String key : reportToSendAlgo.keySet()){

                        reportToSendAlgoCopy.put(key, reportToSendAlgo.get(key));
                    }

                    reportToSendAlgo.clear();

                    sigma03ReportToSend.put("ALGO_TRADE_REPORT", reportToSendAlgoCopy);

                    logger.info("ATTACHING REPORT TO EXCHANGE BODY FOR BOT: " + reportToSendAlgo);
                    exchange.getOut().setBody(sigma03ReportToSend);
                    logger.info("ATTACHED! ");
                }else{

                    logger.info("Report is empty");
                }



                /***
                if(exchange.getIn().getBody() instanceof Map){

                    Map<String, Object> sigma03Report = (Map<String, Object>) exchange.getIn().getBody();


                    logger.info("ALGO TRADING UPDATE RECEIVED from the ALgo");

                    logger.info("Message is: " + sigma03Report);


                    sigma03Report.put("ORIGINAL_COMMAND", Sigma03BotCommands.ALGO_TRADE_UPDATE);


                    sigma03Report.put("ALGO_TRADE_REPORT", sigma03Report);

                    //exchange.getOut().setBody(sigma03Report);

                }else{

                    logger.info("Body is not of type map");

                }***/
            }catch(Exception e){

                logger.error("Error when sending Algo response to Bot", e);
            }



        }else{

            logger.info("Exchange body is null..");
        }

    }


}

