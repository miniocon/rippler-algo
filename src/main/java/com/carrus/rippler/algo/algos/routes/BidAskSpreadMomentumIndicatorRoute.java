package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.processors.BidAskSpreadMomentumIndicatorProcessor;
import com.carrus.rippler.algo.algos.processors.BuyingOpportunityForMultiplesStrategyDetector;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.util.Date;

public class BidAskSpreadMomentumIndicatorRoute extends RouteBuilder {


    final static Logger logger = Logger.getLogger(BidAskSpreadMomentumIndicatorRoute.class);


    private Class exchangeClassName;
    private CurrencyPair currencyPair;
    private String targetEndpoint;

    public BidAskSpreadMomentumIndicatorRoute(Class exchangeClassName, CurrencyPair currencyPair, String targetEndpoint){
        super();
        this.exchangeClassName = exchangeClassName;
        this.currencyPair = currencyPair;
        this.targetEndpoint = targetEndpoint;


    }


    @Override
    public void configure() throws Exception {

        from("timer://evaluateBuyingOpportunity"+new Date().getTime()+"?fixedRate=true&period=5000&delay=120000").process(new BidAskSpreadMomentumIndicatorProcessor(exchangeClassName,currencyPair)).marshal().json(JsonLibrary.Jackson).to(this.targetEndpoint);;

        logger.info("Successfully setup BidAskSpreadMomentumIndicatorRoute for " + currencyPair);



    }
}
