package com.carrus.rippler.algo.algos.routes;


import com.carrus.rippler.algo.algos.processors.MarketSummaries50SecROCProcessor;
import com.carrus.rippler.algo.algos.processors.MarketSummaries5SecROCProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummaries50SecVolROCRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(MarketSummaries50SecVolROCRoute.class);


    private String targetEndpoint;
    private Class exchange;



    public MarketSummaries50SecVolROCRoute(Class exchange, String targetEndpoint){
        super();

        this.targetEndpoint = targetEndpoint;
        this.exchange = exchange;

    }


    public void configure() throws Exception {



        logger.info("*****Setting up MarketSummaries50SecVroc Route..with: *******" + exchange.getName() + ", " + this.targetEndpoint);



        from("timer://evaluateMultiples50"+new Date().getTime()+"?fixedRate=true&period=50000&delay=30000").process(new MarketSummaries50SecROCProcessor(exchange)).to(this.targetEndpoint);


        logger.info("*****Completed setting up Route for MarketSummaries50SecVRoc Route...with ****" + exchange.getName()  + ", "+ this.targetEndpoint);



    }


}
