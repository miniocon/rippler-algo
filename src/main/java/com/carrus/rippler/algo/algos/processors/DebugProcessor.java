package com.carrus.rippler.algo.algos.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

/**
 * Created by usmankhan on 02/12/2017.
 */
public class DebugProcessor implements Processor {

    final static Logger logger = Logger.getLogger(DebugProcessor.class);


    public void process(Exchange exchange) throws Exception {

        logger.info(exchange.getIn().getBody());

    }
}
