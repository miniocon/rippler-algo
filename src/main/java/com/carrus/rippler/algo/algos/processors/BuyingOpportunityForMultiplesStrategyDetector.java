package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.RipplerAlgo;

import com.carrus.rippler.algo.algos.tradingstrategies.MultiplesStrategy;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;

public class BuyingOpportunityForMultiplesStrategyDetector implements Processor {

    final static Logger logger = Logger.getLogger(BuyingOpportunityForMultiplesStrategyDetector.class);

    private Class exchangeClassName;
    private CurrencyPair currencyPair;

    private String bidOrderBookCacheId;
    private String askOrderBookCacheId;

    private PagingPredicate bidPagingPredicate;
    private PagingPredicate askPagingPredicate;
    private int pageSize;


    public BuyingOpportunityForMultiplesStrategyDetector(Class exchangeClassName, CurrencyPair internalCurrencyPairId,  String bidOrderBookCacheId, String askOrderBookCacheId,  int pageSize){

        this.exchangeClassName = exchangeClassName;
        this.currencyPair  = internalCurrencyPairId;

        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.pageSize = pageSize;

        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {


                return ((BigDecimal)e2.getKey()).compareTo((BigDecimal)e1.getKey());

            }
        };

        Comparator<Map.Entry> ascendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {

                return ((BigDecimal)e1.getKey()).compareTo((BigDecimal)e2.getKey());

            }
        };

        askPagingPredicate = new PagingPredicate(ascendingComparator, pageSize);
        bidPagingPredicate = new PagingPredicate(descendingComparator, pageSize);



    }


    public void process(Exchange exchange) throws Exception {

        try{

            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");



            IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

            String algoBestBidLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidOrderBookCacheId,Constants.BID,Constants.BEST_LEVEL);
            String algoBestAskLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askOrderBookCacheId,Constants.ASK,Constants.BEST_LEVEL);
            String algoTop10BidSum = CacheIdentifierHelper.getAlgoCacheMetricKey(bidOrderBookCacheId,Constants.BID,Constants.TOP_10_SUM);
            String algoTop10AskSum = CacheIdentifierHelper.getAlgoCacheMetricKey(askOrderBookCacheId,Constants.ASK,Constants.TOP_10_SUM);

            BigDecimal bestBid = algoMetricsCache.get(algoBestBidLevelKey);
            BigDecimal bestAsk = algoMetricsCache.get(algoBestAskLevelKey);

            BigDecimal top10BidSum = algoMetricsCache.get(algoTop10BidSum);
            BigDecimal top10AskSum = algoMetricsCache.get(algoTop10AskSum);

            BigDecimal bidAskMultiple = top10BidSum.divide(top10AskSum,8,BigDecimal.ROUND_HALF_UP);


            BigDecimal bidAskSpread = bestBid.subtract(bestAsk);





            BigDecimal bidAskMultipleConstant = RipplerAlgo.BID_ALGO_MULTIPLES.get(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.BID, currencyPair));

            BigDecimal spreadConstant = RipplerAlgo.ALGO_SPREAD_CONSTANT.get(CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(MultiplesStrategy.class, currencyPair));


            logger.debug("Best Bid for  "+ currencyPair+" "+ bestBid);
            logger.debug("Best Ask for  " + currencyPair+" "+ bestAsk);
            logger.debug("top 10 Bid Sum " +currencyPair+" "+ top10BidSum);
            logger.debug("top 10 Ask Sum " + currencyPair+" "+top10AskSum);
            logger.debug("Bid/Ask spread: " + currencyPair+" "+bidAskSpread);
            logger.debug("Multiple for bid/ask Sum: " + currencyPair+" "+bidAskMultiple);
            logger.debug("BidASKMultipleConstant:" + currencyPair+" "+ bidAskMultipleConstant);
            logger.debug("spreadConstant:" + currencyPair+" "+ spreadConstant);

            /**
             *
             * If the condition is met, then set the buying status to true
             *
             */

            if(
                            (bidAskMultiple.compareTo(bidAskMultipleConstant) ==0 || bidAskMultiple.compareTo(bidAskMultipleConstant) ==1) &&

                            (bidAskSpread.compareTo(spreadConstant) ==0 || bidAskSpread.compareTo(spreadConstant)==1)
                    ){


                IMap<String, Boolean> buyingOpportunityPerCurrencyPairCache = distributedCacheInstance.getMap(Constants.BUY_OPPORTUNITY_FOR_MULTIPLES_STRATEGY_DETECTOR_CACHE.name());

                String key = CacheIdentifierHelper.getBuyingStatusIdForCurrencyPairAndExchange(exchangeClassName, currencyPair);

                buyingOpportunityPerCurrencyPairCache.put(key,true);


                logger.info("Multiples condition for currency pair  ***MET**** for " + key );



            }else{

                IMap<String, Boolean> buyingOpportunityPerCurrencyPairCache = distributedCacheInstance.getMap(Constants.BUY_OPPORTUNITY_FOR_MULTIPLES_STRATEGY_DETECTOR_CACHE.name());

                String key = CacheIdentifierHelper.getBuyingStatusIdForCurrencyPairAndExchange(exchangeClassName, currencyPair);

                buyingOpportunityPerCurrencyPairCache.put(key,false);

                logger.debug("Multiples condition for currency pair *****not met**** for " + key );
            }




        }catch(Exception e){

            logger.error("Error processing multiples Algo strategy ", e);
        }



    }
}
