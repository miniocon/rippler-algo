package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by usmankhan on 28/10/2017.
 */
public class OrderBookMetricCollector implements Processor{

    final static Logger logger = Logger.getLogger(OrderBookMetricCollector.class);

    private String orderBookCacheId;
    private Enum direction;
    private PagingPredicate top10PagingPredicate;
    private PagingPredicate top100PagingPredicate;


    public OrderBookMetricCollector(String orderBookCacheId, Enum direction){
        super();
        this.orderBookCacheId = orderBookCacheId;
        this.direction = direction;


        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {


                return ((BigDecimal)e2.getKey()).compareTo((BigDecimal)e1.getKey());

            }
        };

        Comparator<Map.Entry> ascendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {

                return ((BigDecimal)e1.getKey()).compareTo((BigDecimal)e2.getKey());

            }
        };

        top10PagingPredicate =  Constants.ASK.equals(direction)?new PagingPredicate(ascendingComparator, 10):new PagingPredicate(descendingComparator, 10);
        top100PagingPredicate =  Constants.ASK.equals(direction)?new PagingPredicate(ascendingComparator, 100):new PagingPredicate(descendingComparator, 100);

    }



    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

        Boolean boostrapStatus = bootstrapStatusMap.get(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

        if(boostrapStatus) {

            IMap<BigDecimal, BigDecimal> orderBook = distributedCacheInstance.getMap(orderBookCacheId);

            IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

            Collection<BigDecimal> orderBookTop10Quantities = orderBook.values(top10PagingPredicate);
            Collection<BigDecimal> orderBookTop10Prices = orderBook.keySet(top10PagingPredicate);

            Collection<BigDecimal> orderBookTop100Quantities = orderBook.values(top100PagingPredicate);


            /** START Collect the Top 10 Sum Multiple***/
            BigDecimal top10sumTotal = new BigDecimal(0);
            for (BigDecimal qty : orderBookTop10Quantities) {
                top10sumTotal = top10sumTotal.add(qty);
            }
            String algoTop10SumCacheKey = CacheIdentifierHelper.getAlgoCacheMetricKey(orderBookCacheId, direction, Constants.TOP_10_SUM);
            algoMetricsCache.put(algoTop10SumCacheKey, top10sumTotal);
            logger.debug("Total 10 sum for : " + algoTop10SumCacheKey + ": " + top10sumTotal);
            /** END Collect the Top 10 Sum Multiple***/


            /*** START Collect the Top 100 Sum Multiple ***/
            BigDecimal top100sumTotal = new BigDecimal(0);
            for (BigDecimal qty : orderBookTop100Quantities) {
                top100sumTotal = top100sumTotal.add(qty);
            }
            String algoTop100SumCacheKey = CacheIdentifierHelper.getAlgoCacheMetricKey(orderBookCacheId, direction, Constants.TOP_100_SUM);
            algoMetricsCache.put(algoTop100SumCacheKey, top100sumTotal);
            logger.debug("Total 100 sum for : " + algoTop100SumCacheKey + ": " + top100sumTotal);
            /*** END Collect the Top 100 Sum Multiple ***/

            /** START Collect best level ***/
            String algoBestLevelKey = CacheIdentifierHelper.getAlgoCacheMetricKey(orderBookCacheId, direction, Constants.BEST_LEVEL);
            algoMetricsCache.put(algoBestLevelKey, orderBookTop10Prices.iterator().next());
            logger.debug("Best Level for : " + algoBestLevelKey + ": " + algoMetricsCache.get(algoBestLevelKey));
            /** END Collect best level ***/


            /** START Collect best qty ***/
            String algoBestQtyKey = CacheIdentifierHelper.getAlgoCacheMetricKey(orderBookCacheId, direction, Constants.BEST_QTY);
            algoMetricsCache.put(algoBestQtyKey, orderBookTop10Quantities.iterator().next());
            logger.debug("Best Qty for : " + algoBestQtyKey + ": " + algoMetricsCache.get(algoBestLevelKey));
            /** END Collect best qty ***/

        }else{

            //Bootstrap not complete yet
        }




    }
}
