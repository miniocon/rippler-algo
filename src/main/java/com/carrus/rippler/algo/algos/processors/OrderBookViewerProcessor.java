package com.carrus.rippler.algo.algos.processors;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by usmankhan on 22/10/2017.
 */
public class OrderBookViewerProcessor implements Processor {

    final static Logger logger = Logger.getLogger(OrderBookViewerProcessor.class);

    private String orderBookCacheId;
    private String sortOrder;
    private boolean shouldSum;
    private PagingPredicate pagingPredicate;

    public OrderBookViewerProcessor(String orderBookCacheId, String sortOrder, boolean shouldSum){

        super();
        this.orderBookCacheId = orderBookCacheId;
        this.shouldSum = shouldSum;
        this.sortOrder = sortOrder;

        Comparator<Map.Entry> descendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {


                return ((BigDecimal)e2.getKey()).compareTo((BigDecimal)e1.getKey());

            }
        };

        Comparator<Map.Entry> ascendingComparator = new Comparator<Map.Entry>() {

            public int compare(Map.Entry e1, Map.Entry e2) {

                return ((BigDecimal)e1.getKey()).compareTo((BigDecimal)e2.getKey());

            }
        };

        pagingPredicate = sortOrder.equals("asc")? new PagingPredicate(ascendingComparator, 10):new PagingPredicate(descendingComparator, 10);

    }

    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        IMap<BigDecimal, BigDecimal> orderBook = distributedCacheInstance.getMap(orderBookCacheId);

        Collection<BigDecimal> orderBookQuantities = orderBook.values(pagingPredicate);
        Collection<BigDecimal> orderBookPrices  = orderBook.keySet(pagingPredicate);


        if(shouldSum) {
            BigDecimal sumTotal = new BigDecimal(0);

            for (BigDecimal qty : orderBookQuantities) {

                sumTotal = sumTotal.add(qty);

            }

            logger.debug("total sum for : " +orderBookCacheId +": "+ sumTotal);
        }





    }
}
