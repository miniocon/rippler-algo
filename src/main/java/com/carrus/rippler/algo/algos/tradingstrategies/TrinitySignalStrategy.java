package com.carrus.rippler.algo.algos.tradingstrategies;

import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.statemanagement.AlgoStates;
import com.carrus.rippler.algo.algos.statemanagement.StateEngine;
import com.carrus.rippler.algo.algos.transactioncostanalysis.TCACalculator;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;


/**
 * Created by usmankhan on 25/12/2017.
 */
public class TrinitySignalStrategy implements Processor {


    final static Logger logger = Logger.getLogger(TrinitySignalStrategy.class);

    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private Class exchange;
    private String testCurrencyPairKey = "BTC-WINGS";
    private StateEngine stateEngine;
    private String currencyKeyToAnalyse;
    private Map<String, BigDecimal> marketStatsToAnalyse;

    private BigDecimal spreadThreshold = new BigDecimal(0.02);

    private BigDecimal bidAskMultipleThreshold = new BigDecimal(3);


    public TrinitySignalStrategy(Class exchange, AccountManager accountManager, ExecutionEngine executionEngine){
        super();
        this.accountManager = accountManager;
        this.executionEngine = executionEngine;
        this.exchange = exchange;
        this.stateEngine = new StateEngine();


    }

//** *  Need to be able to send this algo command messages - need to get algo status updates and send to bot - if algo gets stuck - let it sell

    // track how much made
    // track losses
    //track number of trades




    public void process(Exchange exchange) throws Exception {

        //Receive currency pair to invest in - State has to be in WAITING mode


        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        processSignals(currencyKeyToAnalyse, marketStatsToAnalyse, distributedCacheInstance);


        IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

        Boolean boostrapStatus = bootstrapStatusMap.get(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

        if(boostrapStatus==true) {

            /*** UNCOMMENT FOR TESTING - processSignals(testCurrencyPairKey, null, distributedCacheInstance); ****/

            //Must have enough Bitcoin balance
            if (exchange != null && exchange.getIn() != null && exchange.getIn().getBody() != null) {
                logger.info(exchange.getIn().getBody().getClass());


                try {
                    Map<String, Object> marketSummaryDeltaMap = (Map<String, Object>) exchange.getIn().getBody();

                    if (marketSummaryDeltaMap != null) {

                        logger.info("Map size: " + marketSummaryDeltaMap.size());

                        //Only process signal if Algo is in Analysing Market mode

                        if (marketSummaryDeltaMap != null && AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState())) {

                            ///TODO - process multiple signals by sorting by DT and market cap - for now get first one

                            currencyKeyToAnalyse = marketSummaryDeltaMap.keySet().iterator().next();


                            marketStatsToAnalyse = (Map<String, BigDecimal>) marketSummaryDeltaMap.get(currencyKeyToAnalyse);



                            //metrics.put("vroc", deltaPercentage);
                            //metrics.put("marketCap", baseVolume);
                            //metrics.put("lastPrice", marketSummary.getLast());

                            logger.info(currencyKeyToAnalyse + " " + marketStatsToAnalyse.get("vroc"));

                        } else {
                            //do nothing  // if Algo not analysing market, then it must be working on a trade
                        }
                    } else {
                        //do nothing
                    }


                } catch (Exception e) {
                    logger.error("Error in Trinity processor", e);
                }

            } else {

                //do nothing
            }
            logger.info("process...");

        }else{

            logger.info("Waiting for bootstrap....to complete..");
        }




    }


    private void processSignals(String currencyKey, Map<String, BigDecimal> marketSummaryStats, HazelcastInstance distributedCacheInstance){

        try {

            if (currencyKey != null && marketSummaryStats != null) {

                IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

                //get top Ask for this currency Pair //BTC-2GIVE is example currenecy pair from marketsummary  - however, internal currency pairs are in the form 2GIVE/BTC - so flip around
                String[] currencyKeySplit = currencyKey.split("-");
                CurrencyPair currencyPair = new CurrencyPair(currencyKeySplit[1], currencyKeySplit[0]);


                logger.info("Analysing trading opportunity for Base Currency: " + currencyPair.base + " and Counter Currency: " + currencyPair.counter);

                String bidCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.BIDS_SUM_ORDER_BOOK); // Constants.BID is direction
                String askCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.ASKS_SUM_ORDER_BOOK); // Constants.ASK is direction

                String topBidLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.BEST_LEVEL);

                String topAskLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.BEST_LEVEL);

                String top10BidSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.TOP_10_SUM);
                String top10AskSumKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.TOP_10_SUM);

                BigDecimal topBid = algoMetricsCache.get(topBidLeveKey);
                BigDecimal bestAsk = algoMetricsCache.get(topAskLeveKey);

                BigDecimal top10BidSum = algoMetricsCache.get(top10BidSumKey);
                BigDecimal top10AskSum = algoMetricsCache.get(top10AskSumKey);


                BigDecimal bidAskSpread = bestAsk.subtract(topBid);

                BigDecimal spreadDelta = bidAskSpread.divide(bestAsk,8,BigDecimal.ROUND_HALF_UP);

                BigDecimal spreadPercentage = spreadDelta.multiply(new BigDecimal(100));

                BigDecimal bidAskMultiple = BigDecimal.ZERO;

                if(top10BidSum!=null && top10AskSum!=null) {

                    bidAskMultiple = top10BidSum.divide(top10AskSum, 8, BigDecimal.ROUND_HALF_UP);
                    logger.info("Spread percentage: " + spreadPercentage + ", bidAskMultiple: " + bidAskMultiple);
                }else{

                    logger.info("top10BidSum or top10AskSum is empty");
                }






                if (AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState())) {

                    //Buy it if the spread is tight and the bid wall exists..


                    if (
                            ( bidAskMultiple.compareTo(bidAskMultipleThreshold) ==0 ||
                                    bidAskMultiple.compareTo(bidAskMultipleThreshold) ==1)

                        &&

                            (spreadPercentage.compareTo(spreadThreshold) == 0 ||
                            spreadPercentage.compareTo(spreadThreshold) == -1)

                            ) {

                        logger.info("Spread % threshold met: " + spreadPercentage);

                        BigDecimal balanceOfLeadingCurrency = accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.counter);


                        BigDecimal amountToBuy = TCACalculator.calculateAmountToBuyUsingBalance(balanceOfLeadingCurrency, bestAsk);

                        logger.info("Place Limit Order: " + Constants.BID + " " + currencyPair + ", amountToBuy: " + amountToBuy + ", askPrice: " + bestAsk);


                        //place Limit Order Bid


                        Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.BID, currencyPair, amountToBuy, bestAsk);

                        String bidUuId = limitOrderResult.get("id");

                        stateEngine.setCurrentState(AlgoStates.BID_LIMIT_ORDER_PROCESSING);
                        stateEngine.setUuIdOfBidLimitOrderToProcess(bidUuId);
                        stateEngine.setAmountBought(amountToBuy);
                        stateEngine.setPriceBoughtAt(bestAsk);
                        stateEngine.setOriginalLeadingCurrencyBalance(accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.counter));

                        logger.info("Successfully placed BID Limit order, uuId recieved: " + bidUuId);

                    } else {

                        logger.info("Spread % threshold not met: " + spreadPercentage + ", should be below or equal to: " + spreadThreshold);

                        //do nothing - spread threshold not there
                    }


                } else if (
                        AlgoStates.BOUGHT.equals(stateEngine.getCurrentState())


                        ) {


                    BigDecimal balanceOfCounterCurrency = accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.base);

                    BigDecimal pnl = TCACalculator.getTCAValue(balanceOfCounterCurrency, stateEngine.getOriginalLeadingCurrencyBalance(), topBid);

                    logger.info("PNL val: " + pnl);

                    /*** If PNL is greater= to the harvest amount- place sell limit order and reset state ***/
                    if (pnl != null && (pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT) == 0 || pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT) == 1)) {

                        logger.info("PNL Target reached with PNL " + pnl);
                        logger.info("Place Limit Order: " + Constants.ASK + " " + currencyPair + ", amountToSell: " + balanceOfCounterCurrency + ", bidPrice: " + topBid);

                        //place Limit Order Bid
                        String askUuId = null;

                        Map<String, String> limitOrderResult   = executionEngine.placeLimitOrder(Constants.ASK, currencyPair, balanceOfCounterCurrency, topBid);

                        askUuId = limitOrderResult.get("id");

                        logger.info("Successfully placed ASK Limit order, uuId recieved: " + askUuId);

                        stateEngine.setUuIdOfAskLimitOrderToProcess(askUuId);
                        stateEngine.setCurrentState(AlgoStates.ASK_LIMIT_ORDER_PROCESSING);
                        stateEngine.setAmountToSell(balanceOfCounterCurrency);


                    } else {

                        logger.info("pnl still not hit as pnl calculated is: " + pnl + " where target is " + RipplerAlgo.RIPPLER_HARVEST_PROFIT);

                    }


                } else if (


                        AlgoStates.BID_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())


                        ) {


                    //check state of the limit order - once it is filled, the balances will update
                    if (executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfBidLimitOrderToProcess(), currencyPair, stateEngine.getAmountBought())) {

                        //Make sure balances for currencies are upto date
                        accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.counter);
                        accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.base);
                        //Order has filled so update state to BOUGHT
                        stateEngine.setCurrentState(AlgoStates.BOUGHT);

                    } else {

                        logger.debug("BID order still not filled");
                    }


                } else if (


                        AlgoStates.ASK_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())

                        ) {


                    //check state of the limit order - once it is filled, the balances will update
                    if (executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfAskLimitOrderToProcess(), currencyPair, stateEngine.getAmountToSell())) {

                        //Make sure balances for currencies are upto date
                        accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.counter);
                        accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.base);

                        //Order has filled so reset State Engine, which defaults state to Analysing Market
                        stateEngine.reset();
                        resetAlgoKeyParams();


                    } else {

                        logger.debug("Ask order still not filled");
                    }


                } else {


                    logger.debug("Still Analysing Market as Buying conditions not met");
                }


                //logger.info("Top Bid for " + currencyPair + ": " + topBid);
                //logger.info("Top Ask for " + currencyPair + ": " + bestAsk);


            } else {

                //do nothing
            }

        }catch(Exception e){

            logger.error("Problem processing signal", e);
        }

    }


    private void resetAlgoKeyParams(){

        //reset currencyKey and detlaStats
        currencyKeyToAnalyse = null;
        marketStatsToAnalyse = null;



    }

}
