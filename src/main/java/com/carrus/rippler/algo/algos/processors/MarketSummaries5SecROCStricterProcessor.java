package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummaries5SecROCStricterProcessor implements Processor {


    final static Logger logger = Logger.getLogger(MarketSummaries5SecROCStricterProcessor.class);

    private Class exchangeClass;

    private BigDecimal vrocAlertLimit = new BigDecimal(1);
    private BigDecimal mCapThreshold = new BigDecimal(200);


    public MarketSummaries5SecROCStricterProcessor(Class exchangeClass){
        this.exchangeClass = exchangeClass;



    }

    public void process(Exchange exchange) throws Exception {


        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        IMap<String, BigDecimal> exchangeVolumes = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchangeClass));

        IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5SecROC = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolume5SecROCTrackerId(exchangeClass));

        IMap<String, MarketSummary> marketSummaryIMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getMarketSummaryMapId(exchangeClass));


        //This process will be run every 5 seconds
        //It calculates the 5 second delta
        //If there is a change - it sends the map of changes to Kafka

        Map<String, Object> deltaMap = new HashMap<String, Object>();

        logger.debug(exchangeVolumes.size());

        for(String currencyPairKey : exchangeVolumes.keySet()){

            BigDecimal volume = exchangeVolumes.get(currencyPairKey); // new value

            BigDecimal fiveSecVol = exchangeVolumesPerCurrencyPair5SecROC.get(currencyPairKey); //value 5 seconds ago

            if(fiveSecVol == null){

                exchangeVolumesPerCurrencyPair5SecROC.put(currencyPairKey, volume);


            }else {

                if(fiveSecVol.compareTo(volume)==1 || fiveSecVol.compareTo(volume)==-1){

                    //This is now different - so calc delta

                    BigDecimal difference = volume.subtract(fiveSecVol);
                    BigDecimal delta = difference.divide(fiveSecVol,8,BigDecimal.ROUND_HALF_UP);
                    BigDecimal deltaPercentage = delta.multiply(new BigDecimal(100)).setScale(8,BigDecimal.ROUND_HALF_UP);



                    if(deltaPercentage.compareTo(vrocAlertLimit) ==0 || deltaPercentage.compareTo(vrocAlertLimit) ==1 ){

                        MarketSummary marketSummary = marketSummaryIMap.get(currencyPairKey);

                        BigDecimal baseVolume = marketSummary.getBaseVolume();

                        if(baseVolume.compareTo(mCapThreshold)==0 || baseVolume.compareTo(mCapThreshold) ==1 ) {
                            if(currencyPairKey.contains("BTC") || currencyPairKey.contains("USDT")) {
                                logger.info("ALERT!! 5 Sec Vol ROC for: " + currencyPairKey + "; " + deltaPercentage + ", " + baseVolume);


                                Map<String, BigDecimal> metrics = new HashMap<String, BigDecimal>();
                                metrics.put("vroc", deltaPercentage);
                                metrics.put("marketCap", baseVolume);
                                metrics.put("lastPrice", marketSummary.getLast());

                                //deltaMap.put(currencyPairKey,deltaPercentage);
                                deltaMap.put(currencyPairKey, metrics);
                            }else{
                                //do nothing
                            }

                        }else{

                            //nothing to do
                        }

                    }else{

                        //do nothing
                    }

                   // logger.info("5 Sec Vol ROC for: " + currencyPairKey + "; " + deltaPercentage );



                    //Set the 5 second volume to new volume that has changed
                    exchangeVolumesPerCurrencyPair5SecROC.put(currencyPairKey, volume);





                }else{

                    //Volume has not changed - do nothing
                }

            }

        }

        if(deltaMap.keySet().size()>0) {

            exchange.getOut().setBody(deltaMap);
        }else{
            //do nothing
        }

    }
}
