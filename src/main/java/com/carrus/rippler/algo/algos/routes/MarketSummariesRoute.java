package com.carrus.rippler.algo.algos.routes;


import com.carrus.rippler.algo.algos.processors.MarketSummariesProcessor;
import com.carrus.rippler.algo.model.MarketSummary;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummariesRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(MarketSummariesRoute.class);

    private String messageBusTopic;
    private String messageBrokerURL;
    private String messageBrokerPort;
    private String groupId;
    private String targetEndpoint;
    private Class exchange;


    public MarketSummariesRoute(Class exchange, String messageBusTopic, String messageBrokerURL, String messageBrokerPort, String groupId, String targetEndpoint){
        super();
        this.messageBusTopic = messageBusTopic;
        this.messageBrokerURL = messageBrokerURL;
        this.messageBrokerPort = messageBrokerPort;
        this.groupId = groupId;
        this.targetEndpoint = targetEndpoint;
        this.exchange = exchange;

    }


    public void configure() throws Exception {



        logger.info("*****Setting up MarketSummaries Route..with: *******" + messageBusTopic + ", " + this.targetEndpoint);

        setupBroker();

        from("kafka:"+messageBusTopic+"?groupId="+groupId+"&autoOffsetReset=latest&consumersCount=1")
                .unmarshal().json(JsonLibrary.Jackson).split().body()
                .process(new MarketSummariesProcessor(exchange))
                .to(targetEndpoint);



        logger.info("*****Completed setting up Route for MarketSummaries Route...with ****" + messageBusTopic + ", "+ this.targetEndpoint);



    }


    private void setupBroker(){


        if(this.getContext().getComponent("kafka")==null) {


            KafkaComponent kafka = new KafkaComponent();
            kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);

            this.getContext().addComponent("kafka", kafka);
            logger.info("NEW KAFKA BROKER Component created in CAMEL CONTEXT");
        }else{



            //Check that Kafka component has its brokers set
            KafkaComponent kafka = (KafkaComponent)this.getContext().getComponent("kafka");

            logger.info("KAFKA BROKER FOUND IN CAMEL CONTEXT");

            if(kafka.getBrokers()==null ||  "".equals(kafka.getBrokers()) ){

                kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);
                logger.info("KAFKA BROKERS WERE NOT SET and ARE NOW SET: " + kafka.getBrokers());
            }else{

                logger.info("KAFKA BROKERS DO EXIST: " + kafka.getBrokers());
            }


        }



    }
}
