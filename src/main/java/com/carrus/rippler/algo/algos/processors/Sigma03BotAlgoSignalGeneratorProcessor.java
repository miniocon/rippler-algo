package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.Sigma03BotCommands;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Sigma03BotAlgoSignalGeneratorProcessor implements Processor {

    final static Logger logger = Logger.getLogger(Sigma03BotAlgoSignalGeneratorProcessor.class);

    private Class exchangeClass;


    public Sigma03BotAlgoSignalGeneratorProcessor(Class exchangeClass){
        super();
        this.exchangeClass = exchangeClass;



    }


    @Override
    public void process(Exchange exchange) throws Exception {

        if (exchange != null && exchange.getIn() != null && exchange.getIn().getBody() != null) {
            logger.info(exchange.getIn().getBody().getClass());


                try {


                     Map<String, Object> commandMessage = (Map<String, Object>) exchange.getIn().getBody();


                    String command = (String) commandMessage.get("COMMAND");

                    logger.info("Command received: " + command);

                    String currencyPairKey = null;


                    if (Sigma03BotCommands.ALGO_BUY.equals(command) ) {


                        logger.info("Command recieved from Sigma03Bot: " + commandMessage);

                        Integer chatId = (Integer) commandMessage.get("CHAT_ID");
                        currencyPairKey = (String) commandMessage.get("CRYPTO_PAIR_KEY");

                        logger.info("Command: " + command);
                        logger.info("chatId: " + chatId);
                        logger.info("currencyPairKey: " + currencyPairKey);



                        Map<String, BigDecimal> detailsMap = new HashMap<String, BigDecimal>();


                        commandMessage.put("details", detailsMap);

                        String [] currencyKeyComponents = currencyPairKey.split("-");

                        commandMessage.put("baseCurrency", currencyKeyComponents[1]);
                        commandMessage.put("counterCurrency", currencyKeyComponents[0]);

                        //For some reason The algo can't recieve a map in this context

                        //exchange.getOut().setBody(signalMap);


                        logger.info("Signal Details map created and set");

                    } else {

                        logger.info("Command not understood for this process. The currencyPairKey is: " + currencyPairKey);
                    }



                }catch(Exception e){

                        logger.error("Error processing: ", e);
                }


            }
        }


    private CurrencyPair getCurrencyPairFromStringKey(String key){

        String [] keys = key.split("-");

        return new CurrencyPair(keys[1], keys[0]);


    }
}

