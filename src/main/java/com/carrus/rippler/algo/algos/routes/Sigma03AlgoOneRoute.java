package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.tradingstrategies.Sigma03AlgoOneSignalStrategy;
import com.carrus.rippler.algo.algos.tradingstrategies.TrinitySignalStrategy;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;

/**
 * Created by usmankhan on 25/12/2017.
 */
public class Sigma03AlgoOneRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(Sigma03AlgoOneRoute.class);

    private String sourceRoute;
    private String targetEndpoint;
    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private Class exchange;


    public Sigma03AlgoOneRoute(Class exchange, String sourceRoute, AccountManager accountManager, ExecutionEngine executionEngine, String targetEndpoint){
        super();
        this.sourceRoute = sourceRoute;
        this.accountManager = accountManager;
        this.executionEngine = executionEngine;
        this.targetEndpoint = targetEndpoint;
        this.exchange = exchange;

    }


    public void configure() throws Exception {


        logger.info("Setting up route");

        //from("timer://evaluateOrderBooks"+new Date().getTime()+"?fixedRate=true&period=1000&delay=10000").

        from(sourceRoute)
                .unmarshal().json(JsonLibrary.Jackson)   ///choice().when().simple("${in.body} != null").marshal().json(JsonLibrary.Jackson)
                .process(new Sigma03AlgoOneSignalStrategy(exchange, accountManager,executionEngine)).choice().when().simple("${in.body} != null").to(targetEndpoint);  //choice().when().simple("${in.body} != null").marshal().json(JsonLibrary.Jackson)

        logger.info("completed Setting up route");
    }
}


