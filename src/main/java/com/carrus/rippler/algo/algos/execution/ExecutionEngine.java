package com.carrus.rippler.algo.algos.execution;

import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.model.Constants;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.Order;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.OpenOrders;
import org.knowm.xchange.dto.trade.UserTrade;
import org.knowm.xchange.dto.trade.UserTrades;
import org.knowm.xchange.service.trade.TradeService;
import org.knowm.xchange.service.trade.params.DefaultTradeHistoryParamCurrencyPair;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParam;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParamCurrencyPair;
import org.knowm.xchange.service.trade.params.orders.OpenOrdersParams;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecutionEngine {

    final static Logger logger = Logger.getLogger(ExecutionEngine.class);

    private AccountManager accountManager;
    private TradeService tradeService;
    private int invocationCount = 0;

    public ExecutionEngine(AccountManager accountManager){
        this.accountManager = accountManager;
        tradeService = accountManager.getExchange().getTradeService();

    }

    public Boolean cancleLimitOrder(String orderId){


        try{


           Boolean status =  tradeService.cancelOrder(orderId);

           logger.info("Order Cancelled? " + status);

           return status;


        }catch(Exception e){

            logger.error("Error cancelling.. ", e);
            return null;
        }

    }

    public Map<String, String> placeLimitOrder(Enum orderType, CurrencyPair pair, BigDecimal quantity, BigDecimal price){

        invocationCount++;  // This has been done to prevent the same limit order being placed multiple times.. - only one limit order can be placed at any one time.
        Map<String, String> limitOrderResult = new HashMap<String, String>();
        if(invocationCount==1) {



            Order.OrderType type = orderType.equals(Constants.BID) ? Order.OrderType.BID : Order.OrderType.ASK;

            LimitOrder limitOrder = new LimitOrder.Builder(type, pair).limitPrice(price).remainingAmount(quantity)
                    .build();

            try {


                logger.info("placing limit order for " + pair + "price: " + price + ", qty: " + quantity);

                String uuid = tradeService.placeLimitOrder(limitOrder);

                limitOrderResult.put("id", uuid);
                invocationCount = 0;

                logger.info("invocation set to ZERO CONFIRMED");

                logger.info(type + " limitOrder successfully placed for " + pair + "price: " + price + ", qty: " + quantity + " with uuid " + uuid);

                return limitOrderResult;


            } catch (Exception e) {
                logger.error("Problem placing limit Order: " + type + ", " + pair + " price: " + price + ", qty: " + quantity, e);
                invocationCount = 0;
                limitOrderResult.put("error", e.getMessage());
                return limitOrderResult;
            }

        }else{
            logger.info("Cannot place limit order again for currency pair: " + pair + " until previous limit order has been successfully placed. Invocation count: " + invocationCount);
            return limitOrderResult;
        }
    }

    public Boolean hasLimitOrderFilledAdvancedNOTYETIMPLEMENTEDATEXCHANGE(String limitOrderId){


        try {

            Collection<Order> openOrder = tradeService.getOrder(limitOrderId);

            if(openOrder==null || openOrder.size()==0) {

                throw new Exception("No Open Order found for: " + limitOrderId);

            } else if( openOrder.size()>1){

                throw new Exception("Multiple open orders found for:" + limitOrderId);

            }else{

                Order aOrder = openOrder.iterator().next();

                return Order.OrderStatus.FILLED.equals(aOrder.getStatus());

            }

        } catch (Exception e) {

            logger.error("Error getting open Order for id: " + limitOrderId, e);
            return false;
        }


    }


    public Boolean hasLimitOrderFilledLIMITORDERONLYGOESTOPENDINGNEW(String limitOrderId, CurrencyPair currencyPair){


        try {


            DefaultOpenOrdersParamCurrencyPair defaultOpenOrdersParamCurrencyPair = new DefaultOpenOrdersParamCurrencyPair(currencyPair);

            OpenOrders openOrders = tradeService.getOpenOrders(defaultOpenOrdersParamCurrencyPair);



            List<LimitOrder> openLimitOrders =  openOrders.getOpenOrders();


            for(LimitOrder limitOrder : openLimitOrders){

                if(limitOrderId.equals(limitOrder.getId())){
                    logger.info("Limit Order with ID: " + limitOrderId + "found with status: " + limitOrder.getStatus());

                    return Order.OrderStatus.FILLED.equals(limitOrder.getStatus());



                }else{

                    //Not this limit order
                }

            }
            logger.info("Limit Order with ID: " + limitOrderId + " not found");


            return false;

        } catch (Exception e) {

            logger.error("Error getting open Order for id: " + limitOrderId, e);
            return false;
        }


    }


    public Boolean hasLimitOrderFilled(String limitOrderId, CurrencyPair currencyPair, BigDecimal amountToBeFilled){

        if(limitOrderId==null){

            logger.info("WARNING: LIMIT ORDER IS NULL - THIS SHOULD NOT HAPPEN!! " + currencyPair +" , " + amountToBeFilled);

            return false;

        }else {


            try {

                DefaultTradeHistoryParamCurrencyPair defaultTradeHistoryParamCurrencyPair = new DefaultTradeHistoryParamCurrencyPair(currencyPair);

                //Check every 10 seconds..
                Thread.sleep(10000);
                UserTrades trades = tradeService.getTradeHistory(defaultTradeHistoryParamCurrencyPair);

                List<UserTrade> userTrades = trades.getUserTrades();

                for (UserTrade userTrade : userTrades) {


                    if (limitOrderId.equals(userTrade.getOrderId())) {

//                        logger.info("Order with id: " + limitOrderId + "traded and therefore filled with qty: " + userTrade.getTradableAmount());
//                        if (amountToBeFilled.equals(userTrade.getTradableAmount())) {
//                            logger.info("And this order was filled with the same amount");
//                        } else {
//                            logger.info("WARNING   THIS ORDER WAS ONLY PARTIALLY FILLED");
//                        }
                        return true;


                    } else {

                        //Trade for order ID not found
                    }


                }

            } catch (Exception e) {

                logger.error("Error getting open Order for id: " + limitOrderId, e);

            }
            return false;
        }


    }


    public Boolean hasLimitOrderLeftTheOpenOrderStack(String limitOrderId, CurrencyPair currencyPair, BigDecimal amountToBeFilled){

        if(limitOrderId==null){

            logger.info("WARNING: LIMIT ORDER IS NULL - THIS SHOULD NOT HAPPEN!! " + currencyPair +" , " + amountToBeFilled);

            return false;

        }else {


            try {

               // DefaultOpenOrdersParamCurrencyPair defaultOpenOrdersParamCurrencyPair = new DefaultOpenOrdersParamCurrencyPair(currencyPair);

                OpenOrdersParams defaultOpenOrdersParams = new DefaultOpenOrdersParam() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }
                };

                //Sleep for 10 seconds before checking
                Thread.sleep(10000);

                OpenOrders openOrders = tradeService.getOpenOrders(defaultOpenOrdersParams);


                List<LimitOrder> userOpenOrders = openOrders.getOpenOrders();

                for(LimitOrder limitOrder : userOpenOrders){


                    if(limitOrderId.equals(limitOrder.getId())){

                        return false;
                    }


                }

                return true;


            } catch (Exception e) {

                logger.error("Error getting open Order for id: " + limitOrderId, e);

            }
            return false;
        }


    }




}
