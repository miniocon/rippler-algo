package com.carrus.rippler.algo.algos.tradingstrategies;

import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.statemanagement.AlgoStates;
import com.carrus.rippler.algo.algos.statemanagement.StateEngine;
import com.carrus.rippler.algo.algos.transactioncostanalysis.TCACalculator;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.Sigma03BotCommands;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;
import scala.collection.immutable.Stream;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by usmankhan on 25/12/2017.
 */
public class Sigma03AlgoOneSignalStrategy implements Processor {


    final static Logger logger = Logger.getLogger(Sigma03AlgoOneSignalStrategy.class);

    private AccountManager accountManager;
    private ExecutionEngine executionEngine;
    private Class exchange;
    private String testCurrencyPairKey = "BTC-WINGS";
    private StateEngine stateEngine;
    private CurrencyPair currencyKeyToAnalyse;
    private Map<String, BigDecimal> signalDetails;
    private Map<String, Object> algoTradeReport = new HashMap<String, Object>();
    private Boolean sellOverride = false;



    public Sigma03AlgoOneSignalStrategy(Class exchange, AccountManager accountManager, ExecutionEngine executionEngine){
        super();
        this.accountManager = accountManager;
        this.executionEngine = executionEngine;
        this.exchange = exchange;
        this.stateEngine = new StateEngine();


    }

//** *  Need to be able to send this algo command messages - need to get algo status updates and send to bot - if algo gets stuck - let it sell

    // track how much made
    // track losses
    //track number of trades




    public void process(Exchange exchange) throws Exception {

        //Receive currency pair to invest in - State has to be in WAITING mode
        //logger.info("Message recieved..");
        //logger.info(exchange.getIn().getBody().getClass());


        try {

            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

            String algoMetricsId = CacheIdentifierHelper.getAlgoReportForExchange(BittrexExchange.class);

            IMap<String, Object> algoMetrics = distributedCacheInstance.getMap(algoMetricsId);

            IMap<String, Object> reportToSendAlgo = distributedCacheInstance.getMap(Constants.REPORT_TO_SEND_TO_BOT.name());

            algoMetrics.put("ALGO_STATE", stateEngine.getCurrentState().toString());


            processSignals(currencyKeyToAnalyse, signalDetails, distributedCacheInstance, algoMetricsId, exchange);


            IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            Boolean boostrapStatus = bootstrapStatusMap.get(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            if (boostrapStatus == true) {

                /*** UNCOMMENT FOR TESTING - processSignals(testCurrencyPairKey, null, distributedCacheInstance); ****/

                ///THIS LOGIC UPDATES THE STATE ENGINE APPROPRIATELY SO THAT WHEN the above processSignals method runs again, it takes the state changes into account

                //Must have enough Bitcoin balance
                if (exchange != null && exchange.getIn() != null && exchange.getIn().getBody() != null) {
                    logger.info(exchange.getIn().getBody().getClass());


                    try {
                        Map<String, Object> signalsMap = (Map<String, Object>) exchange.getIn().getBody();

                        if (signalsMap != null) {


                            logger.info("Map size: " + signalsMap.size());


                            ///TODO - process multiple signals by sorting by DT and market cap - for now get first one

                            String command = (String) signalsMap.get("COMMAND");

                            logger.info("COMNMAND RECEIVED: " + command);

                            //Only process signal if Algo is in Analysing Market mode

                            if(command!=null && command.equals(Sigma03BotCommands.ALGO_CANCEL)){

                                accountManager.intialiseCurrencyBalances();


                                //Cancellation recieved of either the bid or the offer..

                                //If the algo attempted to buy, but was not able to, cancel the order and go back to analysing mode

                                if(AlgoStates.BID_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())){

                                    boolean status = executionEngine.cancleLimitOrder(stateEngine.getUuIdOfBidLimitOrderToProcess());

                                    logger.info("Order Cancellation status: " + status);

                                    if(status){

                                        reportToSendAlgo.clear();
                                        reportToSendAlgo.put("CURRENCY_PAIR", stateEngine.getCurrencyPair().toString());
                                        reportToSendAlgo.put("TRADE_TYPE", "BUY_CANCELLED_AND_RESETTING");

                                        accountManager.intialiseCurrencyBalances();

                                        stateEngine.reset();
                                    }else{

                                        reportToSendAlgo.clear();
                                        reportToSendAlgo.put("CURRENCY_PAIR", stateEngine.getCurrencyPair().toString());
                                        reportToSendAlgo.put("TRADE_TYPE", "NOT_CANCELLED_YET_TRY_AGAIN");


                                    }


                                }else if (AlgoStates.ASK_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())){

                                    //If the Algo is attempting to sell, but the sell limit order is not yet placed,
                                    //cancel the sell limit order and put it back into the bought state
                                    boolean status = executionEngine.cancleLimitOrder(stateEngine.getUuIdOfAskLimitOrderToProcess());

                                    if(status){

                                        stateEngine.setUuIdOfAskLimitOrderToProcess(null);
                                        stateEngine.setCurrentState(AlgoStates.BOUGHT);

                                        accountManager.intialiseCurrencyBalances();

                                        reportToSendAlgo.clear();
                                        reportToSendAlgo.put("CURRENCY_PAIR", stateEngine.getCurrencyPair().toString());
                                        reportToSendAlgo.put("TRADE_TYPE", "ALGO_STATE_BACK_IN_BOUGHT_STATE_TRY_SELLING_AGAIN");



                                    }else{

                                        reportToSendAlgo.clear();
                                        reportToSendAlgo.put("CURRENCY_PAIR", stateEngine.getCurrencyPair().toString());
                                        reportToSendAlgo.put("TRADE_TYPE", "NOT_CANCELLED_YET_TRY_AGAIN");

                                    }



                                }else{

                                    logger.info("Cannot cancel, as the algo is not in the Bid/Ask limit order processing state. The state is: " + stateEngine.getCurrentState());
                                }



                            }else if(command!=null && command.equals(Sigma03BotCommands.SELL)){

                                accountManager.intialiseCurrencyBalances();

                                logger.info("Will only SELL if state is BOUGHT: ");
                                logger.info("CURRENT STATE: " +stateEngine.getCurrentState() );

                                if(AlgoStates.BOUGHT.equals(stateEngine.getCurrentState())){



                                    sellOverride = true;

                                    logger.info("SELL OVERRIDE COMMAND RECEIVED!!! SELLING OUT OF POSITION!! - val = " + sellOverride);

                                }else{

                                    logger.info("Algo is not in BOUGHT state, so cannot sell");
                                }


                            }else if(AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState())) {

                                if(command!=null && command.equals(Sigma03BotCommands.ALGO_BUY)){

                                    accountManager.intialiseCurrencyBalances();

                                    String baseCurrency = (String) signalsMap.get("baseCurrency");
                                    String counterCurrency = (String) signalsMap.get("counterCurrency");

                                    if(baseCurrency!=null && counterCurrency!=null) {

                                        currencyKeyToAnalyse = new CurrencyPair(baseCurrency, counterCurrency);

                                        logger.info("Executing signal for :" + currencyKeyToAnalyse);


                                        signalDetails = (Map<String, BigDecimal>) signalsMap.get("details");


                                        logger.info("SIGNAL TO BUY " + currencyKeyToAnalyse + " RECEIVED!!!");
                                    }else{

                                        logger.info("Counter & Base Currency are missing..");
                                    }


                                }else{

                                    logger.info("command not understood");
                                }


                            } else {

                                logger.info("CANNOT BUY AS THE CURRENT STATE IS : " + stateEngine.getCurrentState());//// TODO - check why state did not transition properly
                                //do nothing  // if Algo not analysing market, then it must be working on a trade
                            }
                        } else {

                            logger.info("THE SIGNAL MAP IS EMPTY SO CAN'T DO ANYTHING");
                            //do nothing
                        }


                    } catch (Exception e) {
                        logger.error("Error in Sigma03AlgoOne processor", e);
                    }

                } else {

                    //do nothing
                }
                //
                //logger.info("process...");

            } else {

                logger.info("Waiting for bootstrap....to complete..");
            }


        }catch(Exception e){

            logger.error("Error processing", e);
        }

    }


    private void processSignals(CurrencyPair currencyPair, Map<String, BigDecimal> signalDetails, HazelcastInstance distributedCacheInstance, String algoMetricsId, Exchange exchange ){

        try {

            if (currencyPair != null && signalDetails != null) {

                IMap<String, BigDecimal> algoMetricsCache = distributedCacheInstance.getMap(Constants.ALGO_METRICS_CACHE.name());

                IMap<String, Object> reportToSendAlgo = distributedCacheInstance.getMap(Constants.REPORT_TO_SEND_TO_BOT.name());

                IMap<String, Object> algoMetrics = distributedCacheInstance.getMap(algoMetricsId);




                //logger.info("Analysing trading opportunity for Base Currency: " + currencyPair.base + " and Counter Currency: " + currencyPair.counter);

                String bidCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.BIDS_SUM_ORDER_BOOK); // Constants.BID is direction
                String askCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.ASKS_SUM_ORDER_BOOK); // Constants.ASK is direction

                String topBidLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(bidCacheIdentifier, Constants.BID, Constants.BEST_LEVEL);

                String topAskLeveKey = CacheIdentifierHelper.getAlgoCacheMetricKey(askCacheIdentifier, Constants.ASK, Constants.BEST_LEVEL);



                BigDecimal topBid = algoMetricsCache.get(topBidLeveKey);
                BigDecimal bestAsk = algoMetricsCache.get(topAskLeveKey);





                if (AlgoStates.ANALYSING_MARKET.equals(stateEngine.getCurrentState())) {






                        BigDecimal balanceOfLeadingCurrency = accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.counter);

                        if(balanceOfLeadingCurrency==null){

                            logger.info("BALANCE OF CURRENCY IS NULL: " + currencyPair.counter + ".. so resetting");

                            currencyKeyToAnalyse = null;
                            signalDetails.clear();


                        }


                        BigDecimal amountToBuy = TCACalculator.calculateAmountToBuyUsingBalance(balanceOfLeadingCurrency, bestAsk);

                        logger.info("Place Limit Order: " + Constants.BID + " " + currencyPair + ", amountToBuy: " + amountToBuy + ", askPrice: " + bestAsk);


                        String bidUuId = null;
                        //place Limit Order Bid
                        if(amountToBuy!=null) {
                            Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.BID, currencyPair, amountToBuy, bestAsk);

                            if(limitOrderResult!=null && limitOrderResult.get("error")!=null){

                                currencyKeyToAnalyse = null;
                                signalDetails.clear();
                                bidUuId=null;
                                logger.info("Error detected when placing limit order: " + bidUuId + " so resetting..");
                            }else{

                                if(limitOrderResult!=null && limitOrderResult.get("id")!=null){

                                    bidUuId = limitOrderResult.get("id");
                                }else{
                                    currencyKeyToAnalyse = null;
                                    signalDetails.clear();
                                    bidUuId=null;

                                    logger.info("Unable to get the limitOrderId... so resetting");
                                }

                                //contrinue as normall..
                            }
                        }else{

                            //amount To Buy is null.. so continue..
                            logger.info("Amount to buy is null... so resetting..");
                            signalDetails.clear();
                            bidUuId=null;
                        }

                        if(amountToBuy!=null && bidUuId!=null) {

                            logger.info("Successfully placed BID Limit order, uuId recieved: " + bidUuId);


                            stateEngine.setCurrentState(AlgoStates.BID_LIMIT_ORDER_PROCESSING);
                            stateEngine.setUuIdOfBidLimitOrderToProcess(bidUuId);
                            stateEngine.setAmountBought(amountToBuy);
                            stateEngine.setPriceBoughtAt(bestAsk);
                            stateEngine.setOriginalLeadingCurrencyBalance(accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.counter));
                            stateEngine.setTimeBoughtAt(new Date());
                            stateEngine.setCurrencyPair(currencyPair);

                            algoMetrics.put("CURRENCY_PAIR", currencyPair.toString());


                            reportToSendAlgo.clear();
                            reportToSendAlgo.put("CURRENCY_PAIR", currencyPair.toString());
                            reportToSendAlgo.put("TRADE_TYPE", "BUY");
                            reportToSendAlgo.put("AMOUNT_BOUGHT", amountToBuy);
                            reportToSendAlgo.put("BOUGHT_PRICE", bestAsk);

                            /*** REVIEW THIS CODE _ DOENST WORK

                            Map<String, Object> signalsMap = (Map<String, Object>) exchange.getIn().getBody();

                            if(signalsMap!=null){


                                signalsMap.put("CURRENCY_PAIR", currencyPair.toString());
                                signalsMap.put("TRADE_TYPE", "BUY");
                                signalsMap.put("AMOUNT_BOUGHT", amountToBuy);
                                signalsMap.put("BOUGHT_PRICE", bestAsk);

                            }else{

                                algoTradeReport.clear();
                                algoTradeReport.put("CURRENCY_PAIR", currencyPair.toString());
                                algoTradeReport.put("TRADE_TYPE", "BUY");
                                algoTradeReport.put("AMOUNT_BOUGHT", amountToBuy);
                                algoTradeReport.put("BOUGHT_PRICE", bestAsk);

                                reportToSendAlgo



                                exchange.getIn().setBody(algoTradeReport);
                                exchange.getOut().setBody(algoTradeReport);


                            }

                            ***/



                        }else{

                            logger.info("The bid limit order UUID is still null.. waiting for it to populate..");
                        }



                } else if (
                        AlgoStates.BOUGHT.equals(stateEngine.getCurrentState())


                        ) {



                    BigDecimal balanceOfCounterCurrency = accountManager.getBalanceForExchangeWalletAndCurrencyFromInternalCache(currencyPair.base);

                    BigDecimal pnl = TCACalculator.getTCAValue(balanceOfCounterCurrency, stateEngine.getOriginalLeadingCurrencyBalance(), topBid);

                    //logger.info("PNL val : " + pnl);
                    algoMetrics.put("PNL", pnl);
                    algoMetrics.put("CURRENCY_PAIR", currencyPair.toString());
                    algoMetrics.put("ORIGINAL_PRICE", stateEngine.getPriceBoughtAt() );
                    algoMetrics.put("TARGET_PRICE", TCACalculator.getTargetPNLPrice(stateEngine.getPriceBoughtAt() ) );
                    algoMetrics.put("PURCHASED_TIME", stateEngine.getTimeBoughtAt());

                    logger.info("SELL OVERRIDE VAL: " + sellOverride);

                    /*** If PNL is greater= to the harvest amount- place sell limit order and reset state ***/
                    if (pnl != null && (pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT) == 0 || pnl.compareTo(RipplerAlgo.RIPPLER_HARVEST_PROFIT) == 1)

                            || sellOverride

                            ) {

                        logger.info("PNL Target reached with PNL  " + pnl);
                        logger.info("Place Limit Order: " + Constants.ASK + " " + currencyPair + ", amountToSell: " + balanceOfCounterCurrency + ", bidPrice: " + topBid);


                        String askUuId = null;
                        //place Limit Order Bid
                         Map<String, String> limitOrderResult = executionEngine.placeLimitOrder(Constants.ASK, currencyPair, balanceOfCounterCurrency, topBid);

                        if(limitOrderResult!=null && limitOrderResult.get("id")!=null){

                            askUuId = limitOrderResult.get("id");
                        }else{

                            logger.info("Limit order ID still not available..");
                        }

                        if(askUuId!=null) {
                            logger.info("Successfully placed ASK Limit order, uuId recieved: " + askUuId);

                            stateEngine.setUuIdOfAskLimitOrderToProcess(askUuId);
                            stateEngine.setCurrentState(AlgoStates.ASK_LIMIT_ORDER_PROCESSING);
                            stateEngine.setAmountToSell(balanceOfCounterCurrency);
                            stateEngine.setTimeSoldAt(new Date());
                            sellOverride = false;


                            reportToSendAlgo.clear();
                            reportToSendAlgo.put("CURRENCY_PAIR", currencyPair.toString());
                            reportToSendAlgo.put("TRADE_TYPE", "SELL");
                            reportToSendAlgo.put("PNL", pnl);
                            reportToSendAlgo.put("AMOUNT_SOLD", balanceOfCounterCurrency);
                            reportToSendAlgo.put("SOLD_PRICE", topBid);


                            /*** REVIEW THIS LOGIC _ DOESNT WORK

                            Map<String, Object> signalsMap = (Map<String, Object>) exchange.getIn().getBody();

                            if(signalsMap!=null){
                                signalsMap.put("CURRENCY_PAIR", currencyPair.toString());
                                signalsMap.put("TRADE_TYPE", "SELL");
                                signalsMap.put("PNL", pnl);
                                signalsMap.put("AMOUNT_SOLD", balanceOfCounterCurrency);
                                signalsMap.put("SOLD_PRICE", topBid);


                            }else{



                                algoTradeReport.clear();
                                algoTradeReport.put("CURRENCY_PAIR", currencyPair.toString());
                                algoTradeReport.put("TRADE_TYPE", "SELL");
                                algoTradeReport.put("PNL", pnl);
                                algoTradeReport.put("AMOUNT_SOLD", balanceOfCounterCurrency);
                                algoTradeReport.put("SOLD_PRICE", topBid);


                                exchange.getIn().setBody(algoTradeReport);
                                exchange.getOut().setBody(algoTradeReport);


                            } **/




                        }else{
                            logger.info("The ASK UUID for the limit order was null - waiting for it to have a value..");
                        }


                    } else {

                        logger.info("pnl still not hit as pnl calculated is: " + pnl + " where target is " + RipplerAlgo.RIPPLER_HARVEST_PROFIT);

                    }


                } else if (


                        AlgoStates.BID_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())


                        ) {


                    //check state of the limit order - once it is filled, the balances will  update
                    if (executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfBidLimitOrderToProcess(), currencyPair, stateEngine.getAmountBought())) {


                        try{

                            //Make sure balances for currencies are  upto date
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.counter);
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.base);

                        }catch(Exception e){
                            logger.info("Problem updating balance.. should fix up later.. ", e);
                        }


                        //Order has filled so update state to BOUGHT
                        stateEngine.setCurrentState(AlgoStates.BOUGHT);

                        algoMetrics.put("CURRENCY_PAIR", currencyPair.toString());

                    } else {

                        logger.debug("BID order still not filled");
                    }


                } else if (


                        AlgoStates.ASK_LIMIT_ORDER_PROCESSING.equals(stateEngine.getCurrentState())

                        ) {


                    //check the state of the limit order - once it is filled, the balances will update
                    if (executionEngine.hasLimitOrderFilled(stateEngine.getUuIdOfAskLimitOrderToProcess(), currencyPair, stateEngine.getAmountToSell())) {

                        //Make sure balances for currencies are upto date
                        try {
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.counter);
                            accountManager.updateInternalCacheBalanceForCurrencyFromExchange(currencyPair.base);
                        }catch(Exception e){
                            logger.info("Problem updating balance.. should fix up later.. ", e);
                        }

                        //Order has filled so reset State Engine, which defaults state to Analysing Market
                        stateEngine.reset();
                        resetAlgoKeyParams();
                        algoMetrics.clear();


                    } else {

                        logger.debug("Ask order still not filled");
                    }


                } else {


                    logger.debug("Still Analysing Market as Buying conditions not met");
                }


                //logger.info("Top Bid for " + currencyPair + ": " + topBid);
                //logger.info("Top Ask for " + currencyPair + ": " + bestAsk);


            } else {

                //do nothing
            }

        }catch(Exception e){

            logger.error("Problem processing signal", e);
        }

    }


    private void resetAlgoKeyParams(){

        //reset currencyKey and detlaStats
        currencyKeyToAnalyse = null;
        signalDetails = null;
        algoTradeReport.clear();



    }

}
