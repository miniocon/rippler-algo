package com.carrus.rippler.algo.algos.processors;

import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by usmankhan on 26/11/2017.
 */
public class MarketSummaries5MinROCProcessor implements Processor {


    final static Logger logger = Logger.getLogger(MarketSummaries5MinROCProcessor.class);

    private Class exchangeClass;


    public MarketSummaries5MinROCProcessor(Class exchangeClass){
        this.exchangeClass = exchangeClass;



    }

    public void process(Exchange exchange) throws Exception {


        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        IMap<String, BigDecimal> exchangeVolumes = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchangeClass));

        IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5MinROC = distributedCacheInstance.getMap(CacheIdentifierHelper.getExchangeVolume5minSecROCTrackerId(exchangeClass));


        //This process will be run every 5 seconds
        //It calculates the 5 second delta
        //If there is a change - it sends the map of changes to Kafka

        Map<String, BigDecimal> deltaMap = new HashMap<String, BigDecimal>();

        logger.info(exchangeVolumes.size());

        for(String currencyPairKey : exchangeVolumes.keySet()){

            BigDecimal volume = exchangeVolumes.get(currencyPairKey); // new value

            BigDecimal fiveSecVol = exchangeVolumesPerCurrencyPair5MinROC.get(currencyPairKey); //value 5 seconds ago

            if(fiveSecVol == null){

                exchangeVolumesPerCurrencyPair5MinROC.put(currencyPairKey, volume);


            }else {

                if(fiveSecVol.compareTo(volume)==1 || fiveSecVol.compareTo(volume)==-1){

                    //This is now different - so calc delta

                    BigDecimal difference = volume.subtract(fiveSecVol);
                    BigDecimal delta = difference.divide(fiveSecVol,8,BigDecimal.ROUND_HALF_UP);
                    BigDecimal deltaPercentage = delta.multiply(new BigDecimal(100)).setScale(8,BigDecimal.ROUND_HALF_UP);



                    logger.info("5 Min Vol ROC for: " + currencyPairKey + "; " + deltaPercentage );


                    deltaMap.put(currencyPairKey, deltaPercentage);
                    //Set the 5 second volume to new volume that has changed
                    exchangeVolumesPerCurrencyPair5MinROC.put(currencyPairKey, volume);





                }else{

                    //Volume has not changed - do nothing
                }

            }

        }


        exchange.getOut().setBody(deltaMap);

    }
}
