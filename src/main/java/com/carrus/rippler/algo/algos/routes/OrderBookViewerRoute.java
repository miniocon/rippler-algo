package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.processors.OrderBookViewerProcessor;
import org.apache.camel.builder.RouteBuilder;

/**
 * Created by usmankhan on 22/10/2017.
 */
public class OrderBookViewerRoute extends RouteBuilder{


    private String sourceRoute;
    private String orderBookCacheId;
    private String sortOrder;
    private boolean shouldSum;

    public OrderBookViewerRoute(String sourceRoute, String orderBookCacheId, String sortOrder, boolean shouldSum){

        super();
        this.sourceRoute = sourceRoute;
        this.orderBookCacheId = orderBookCacheId;
        this.sortOrder = sortOrder;
        this.shouldSum = shouldSum;


    }

    public void configure() throws Exception {


        from("direct:"+this.sourceRoute).process(new OrderBookViewerProcessor(orderBookCacheId, sortOrder, shouldSum)).to("log:console");




    }
}
