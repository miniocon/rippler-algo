package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ManualLimitOrderPlacementProcessor;
import com.carrus.rippler.algo.algos.tradingstrategies.MultiplesStrategy;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Date;

public class ManualLimitOrderPlacementRoute extends RouteBuilder {

    final static Logger logger = Logger.getLogger(ManualLimitOrderPlacementRoute.class);

    private CurrencyPair internalCurrencyPairId;
    private Currency leadingCurrency;
    private Currency counterCurrency;
    private AccountManager accountManager;

    private String pollingFrequency;
    private String targetEndpoint;


    private BigDecimal quantity;
    private BigDecimal price;
    private Enum side;

    public ManualLimitOrderPlacementRoute(String pollingFrequency, CurrencyPair internalCurrencyPairId, Currency leadingCurrency, Currency counterCurrency, BigDecimal quantiy, BigDecimal price, Enum side, AccountManager accountManager,  String targetEndpoint){

        this.internalCurrencyPairId = internalCurrencyPairId;
        this.leadingCurrency = leadingCurrency;
        this.counterCurrency = counterCurrency;

        this.accountManager = accountManager;
        this.pollingFrequency = pollingFrequency;
        this.targetEndpoint = targetEndpoint;
        this.quantity = quantiy;
        this.price = price;
        this.side = side;

    }


    public void configure() throws Exception {

        //TODO - run properly - run once for now

        //from("direct:"+this.sourceEndpoint).process(new MultiplesStrategy(internalCurrencyPairId, bidOrderBookCacheId, askOrderBookCacheId,  accountManager,pageSize)).to(this.targetEndpoint);

        //from("timer://runOnce?repeatCount="+pollingFrequency+"&delay=30000").process(new MultiplesStrategy(internalCurrencyPairId, bidOrderBookCacheId, askOrderBookCacheId,  accountManager,pageSize)).to(this.targetEndpoint);

        //timer://evaluateSecondOrderDeriv?fixedRate=true&period=10000&delay=30000"
        from("timer://placeManualLimitOrder"+new Date().getTime()+"?fixedRate=true&period=10000&delay=30000").process(new ManualLimitOrderPlacementProcessor(internalCurrencyPairId, leadingCurrency, counterCurrency, accountManager, quantity, price, side)).to(this.targetEndpoint);


        logger.info("Successfully setup manual limitOrder Route placement for " + internalCurrencyPairId);

    }


}
