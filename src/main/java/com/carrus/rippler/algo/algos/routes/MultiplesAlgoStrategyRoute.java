package com.carrus.rippler.algo.algos.routes;

import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.tradingstrategies.MultiplesStrategy;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;

import java.util.Date;

public class MultiplesAlgoStrategyRoute extends RouteBuilder {

    final static Logger logger = Logger.getLogger(MultiplesAlgoStrategyRoute.class);

    private CurrencyPair internalCurrencyPairId;
    private Currency leadingCurrency;
    private Currency counterCurrency;
    private String bidOrderBookCacheId;
    private String askOrderBookCacheId;
    private AccountManager accountManager;
    private int pageSize;
    private String pollingFrequency;
    private String targetEndpoint;

    public MultiplesAlgoStrategyRoute(String pollingFrequency,CurrencyPair internalCurrencyPairId, Currency leadingCurrency, Currency counterCurrency, String bidOrderBookCacheId, String askOrderBookCacheId, AccountManager accountManager, int pageSize,  String targetEndpoint){

        this.internalCurrencyPairId = internalCurrencyPairId;
        this.leadingCurrency = leadingCurrency;
        this.counterCurrency = counterCurrency;
        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.accountManager = accountManager;
        this.pageSize = pageSize;
        this.pollingFrequency = pollingFrequency;
        this.targetEndpoint = targetEndpoint;

    }


    public void configure() throws Exception {

        //TODO - run properly - run once for now

        //from("direct:"+this.sourceEndpoint).process(new MultiplesStrategy(internalCurrencyPairId, bidOrderBookCacheId, askOrderBookCacheId,  accountManager,pageSize)).to(this.targetEndpoint);

        //from("timer://runOnce?repeatCount="+pollingFrequency+"&delay=30000").process(new MultiplesStrategy(internalCurrencyPairId, bidOrderBookCacheId, askOrderBookCacheId,  accountManager,pageSize)).to(this.targetEndpoint);

        //timer://evaluateSecondOrderDeriv?fixedRate=true&period=10000&delay=30000"
        from("timer://evaluateMultiples"+new Date().getTime()+"?fixedRate=true&period=10000&delay=30000").process(new MultiplesStrategy(internalCurrencyPairId, leadingCurrency, counterCurrency, bidOrderBookCacheId, askOrderBookCacheId,  accountManager,pageSize)).to(this.targetEndpoint);


        logger.info("Successfully setup MultiplesAlgoStrategyRoute for " + internalCurrencyPairId);

    }


}
