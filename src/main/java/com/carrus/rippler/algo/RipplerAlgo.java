package com.carrus.rippler.algo;



import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.routes.*;
import com.carrus.rippler.algo.algos.tradingstrategies.MultiplesStrategy;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookManager;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes.AskOrderBookUpdateRoute;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes.BidOrderBookUpdateRoute;
import com.carrus.rippler.algo.supportedexchanges.SupportedExchangesManager;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.MessageBusTopicIdentifierHelper;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.main.Main;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class  RipplerAlgo{

	public final static String BITTREX_EXECUTION_ENGINE_INSTANCE = "bittrexExecutionEngineInstance";

	final static Logger logger = Logger.getLogger(RipplerAlgo.class);

	final static String EXCHANGE_PROPERTIES_FILE_NAME = "exchange.properties";

	public static BigDecimal BITTREX_TRANSACTION_FEE_CONSTANT_IN_BTC = new BigDecimal(0.9975);  //This is equivalent 0.25% which is 1-0.0025

	//public static BigDecimal RIPPLER_HARVEST_PROFIT = new BigDecimal(0.0075).setScale(4,BigDecimal.ROUND_HALF_UP);
	public static BigDecimal RIPPLER_HARVEST_PROFIT = new BigDecimal(0.01).setScale(4,BigDecimal.ROUND_HALF_UP);

	final static String KAFKA_HOST = "kafka.host";
	final static String KAFKA_PORT = "kafka.port";



	final static String BTC_NEO_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcneobidOrderBookViewer";
	final static String BTC_NEO_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcneoaskOrderBookViewer";

	final static String BTC_OMG_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcomgbidOrderBookViewer";
	final static String BTC_OMG_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcomgaskOrderBookViewer";

	final static String BTC_PAY_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcpaybidOrderBookViewer";
	final static String BTC_PAY_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcpayaskOrderBookViewer";

	final static String BTC_XRP_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcxrpbidOrderBookViewer";
	final static String BTC_XRP_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcxrpaskOrderBookViewer";

	final static String BTC_ETH_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcethbidOrderBookViewer";
	final static String BTC_ETH_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcethaskOrderBookViewer";

	final static String BTC_VTC_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcvtcbidOrderBookViewer";
	final static String BTC_VTC_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcvtcaskOrderBookViewer";

	final static String BTC_XLM_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcxlmbidOrderBookViewer";
	final static String BTC_XLM_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcxlmaskOrderBookViewer";

	final static String BTC_LTC_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcltcbidOrderBookViewer";
	final static String BTC_LTC_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcltcaskOrderBookViewer";


	final static String BTC_USDT_BID_ORDER_BOOK_VIEWER_ENDPOINT = "btcusdtbidOrderBookViewer";
	final static String BTC_USDT_ASK_ORDER_BOOK_VIEWER_ENDPOINT = "btcusdtaskOrderBookViewer";


	final static String BTC_NEO_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcneobidOrderBookMETRIC";
	final static String BTC_NEO_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcneoaskOrderBookMETRIC";

	final static String BTC_OMG_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcomgbidOrderBookMETRIC";
	final static String BTC_OMG_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcomgaskOrderBookMETRIC";

	final static String BTC_PAY_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcpaybidOrderBookMETRIC";
	final static String BTC_PAY_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcpayaskOrderBookMETRIC";

	final static String BTC_XRP_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcxrpbidOrderBookMETRIC";
	final static String BTC_XRP_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcxrpaskOrderBookMETRIC";

	final static String BTC_ETH_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcethbidOrderBookMETRIC";
	final static String BTC_ETH_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcethaskOrderBookMETRIC";

	final static String BTC_VTC_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcvtcbidOrderBookMETRIC";
	final static String BTC_VTC_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcvtcaskOrderBookMETRIC";

	final static String BTC_XLM_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcxlmbidOrderBookMETRIC";
	final static String BTC_XLM_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcxlmaskOrderBookMETRIC";

	final static String BTC_LTC_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcltcbidOrderBookMETRIC";
	final static String BTC_LTC_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcltcaskOrderBookMETRIC";


	final static String BTC_USDT_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcusdtbidOrderBookMETRIC";
	final static String BTC_USDT_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcusdtaskOrderBookMETRIC";

	final static String BTC_BCC_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcbcctbidOrderBookMETRIC";
	final static String BTC_BCC_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcbcctaskOrderBookMETRIC";

	final static String BTC_MER_BID_ORDER_BOOK_METRIC_ENDPOINT = "btcmertbidOrderBookMETRIC";
	final static String BTC_MER_ASK_ORDER_BOOK_METRIC_ENDPOINT = "btcmertaskOrderBookMETRIC";


	final static String BITTREX_MARKET_SUMMARIES_ENDPOINT = "BittrexExchange_MarketSummaries";



	public final static String RIPPLER_ALGO_PROPERTIES_FILE_NAME = "rippleralgo.properties";

	public final static int MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE = 10;

	public final static String MULTIPLES_STRATEGY_POLLING_STRATEGY = "1";




	public static Map<String, BigDecimal> BID_ALGO_MULTIPLES = new HashMap<String, BigDecimal>();

	public static Map<String, BigDecimal> ASK_ALGO_MULTIPLES = new HashMap<String, BigDecimal>();

	public static Map<String, BigDecimal> ALGO_SPREAD_CONSTANT = new HashMap<String, BigDecimal>();

	public static BigDecimal BITTREX_EXCHANGE_FEE = new BigDecimal(1.0025);

	public static BigDecimal BITREX_INVERSE_EXCHANGE_FEE = new BigDecimal(0.9975);

	public static CurrencyPair BTCUSDT;



	public static CurrencyPair BTCNEO;


	public static CurrencyPair BTCOMG;

	public static CurrencyPair BTCPAY;


	public static CurrencyPair BTCXRP;


	public static CurrencyPair BTCETH;


	public static CurrencyPair BTCVTC;

	public static CurrencyPair BTCXLM;

	public static CurrencyPair BTCLTC;

	public static CurrencyPair BTCBCC;

	public static CurrencyPair BTCMER;


	static{

		 BTCUSDT = new CurrencyPair("BTC", "USDT");



		BTCNEO = new CurrencyPair("NEO", "BTC");


		BTCOMG = new CurrencyPair("OMG", "BTC");

		BTCPAY = new CurrencyPair("PAY", "BTC");


		BTCXRP = new CurrencyPair("XRP", "BTC");


		BTCETH = new CurrencyPair("ETH", "BTC");


		 BTCVTC = new CurrencyPair("VTC", "BTC");

		 BTCXLM = new CurrencyPair("XLM", "BTC");

		BTCLTC = new CurrencyPair("LTC", "BTC");

		 BTCBCC = new CurrencyPair("BCC", "BTC");

		 BTCMER = new CurrencyPair("MER", "BTC");


		BID_ALGO_MULTIPLES.put(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.BID, BTCUSDT), new BigDecimal(2));

		BID_ALGO_MULTIPLES.put(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.BID, BTCNEO), new BigDecimal(2));

		BID_ALGO_MULTIPLES.put(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.BID, BTCBCC), new BigDecimal(4));

		BID_ALGO_MULTIPLES.put(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.BID, BTCMER), new BigDecimal(4));

		ASK_ALGO_MULTIPLES.put(CacheIdentifierHelper.getAlgoMultiplesKeyForCurrencyPair(MultiplesStrategy.class, Constants.ASK, BTCUSDT), new BigDecimal(1.5));



		ALGO_SPREAD_CONSTANT.put( CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(MultiplesStrategy.class, BTCUSDT), new BigDecimal(-0.2).setScale(1,BigDecimal.ROUND_HALF_UP));
		ALGO_SPREAD_CONSTANT.put( CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(MultiplesStrategy.class, BTCNEO), new BigDecimal(-0.00000002).setScale(1,BigDecimal.ROUND_HALF_UP));
		ALGO_SPREAD_CONSTANT.put( CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(MultiplesStrategy.class, BTCBCC), new BigDecimal(-0.00002).setScale(1,BigDecimal.ROUND_HALF_UP));
		ALGO_SPREAD_CONSTANT.put( CacheIdentifierHelper.getAlgoSpreadKeyForCurrencyPair(MultiplesStrategy.class, BTCMER), new BigDecimal(-0.00002).setScale(1,BigDecimal.ROUND_HALF_UP));

		//0.18350019


	}

	
	public static void main(String [] args){

		logger.info("Starting Rippler Algo..");

		String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		String exchangePropsPath = rootPath + EXCHANGE_PROPERTIES_FILE_NAME ;
		String ripplerAlgoPropsPath = rootPath + RIPPLER_ALGO_PROPERTIES_FILE_NAME;


		Properties exchangeProps = new Properties();
		Properties ripplerAlgoProps = new Properties();

		try {
			exchangeProps.load(new FileInputStream(exchangePropsPath));
			ripplerAlgoProps.load(new FileInputStream(ripplerAlgoPropsPath));
			String bittrexApiKey = exchangeProps.getProperty("BittrexExchange.api.key");
			String bittrexApiSecretKey = exchangeProps.getProperty("BittrexExchange.api.secret");


			//Setup Distributed Data Cache
			Config cfg = new Config();
			HazelcastInstance distributedCacheInstance = Hazelcast.newHazelcastInstance(cfg);

			////This is an AccountManget for Mani@carrusltd.com Todo - get list of Wallet Ids and create an Account Manager per wallet id - for now supply a null walletId and Account Info will defauly to the first wallet
			AccountManager bitterexAccountManager = new AccountManager(BittrexExchange.class, bittrexApiKey, bittrexApiSecretKey, distributedCacheInstance, null);


			ExecutionEngine bittrexExecutionEngine = new ExecutionEngine(bitterexAccountManager);



			/**Start Camel, bind the distributed cache and start routes for updating OrderBooks in realtime and performing Algo based analysis of order books and trade execution**/
			Main camelMainComponent = new Main();
			camelMainComponent.bind("hazelcastInstance", distributedCacheInstance);



			/** Setup Market Summaries TODO - UNCOMMENT FOR MARKET SUMMARIES ANALYSIS**/
			//setupMarketSummaries(BittrexExchange.class, camelMainComponent, ripplerAlgoProps, "log:console", distributedCacheInstance);


			/** Setup supported Exchanges ***/
			SupportedExchangesManager sem = new SupportedExchangesManager(distributedCacheInstance);
			sem.initialiseExchanges();
			OrderBookManager orderBookManager = new OrderBookManager(distributedCacheInstance);


			/**
			 * Setup Camel Route for processing Bid/Ask OrderBook updates for BTCNEO
			 	In orderBook management package Should have a generic Bid OrderBook processor that knows how to process updates for this exchange across currencies
				In orderBook management package Should have a generic Bid OrderBook processor that knows how to process updates for this exchange across currencies
				Have a generic Route class, that takes a Message Bus topic name using the MessageBusTopicIdentifierHelper (Exchange + CurrencyPair as id) and has a
			    processeor which takes relevant bid/ask order book cache and keeps it updated
			 **/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update And metrics collection route for BTC MER***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCMER, "direct:"+BTC_MER_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_MER_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);

			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_MER_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class,BTCMER , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_MER_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCMER , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));


			//camelMainComponent.addRouteBuilder(new BuyingOpportunityForMultiplesStrategyDetectorRoute(BittrexExchange.class,SupportedCurrencyPairInternalCodes.BTCNEO,CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCNEO , Constants.BIDS_SUM_ORDER_BOOK),CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCNEO , Constants.ASKS_SUM_ORDER_BOOK),MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE, "15000","log:console" ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC MER***/





			/*** START...Setup Bid/Ask Bitterex OrderBook with Update And metrics collection route for BTC NEO***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCNEO, "direct:"+BTC_NEO_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_NEO_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_NEO_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_NEO_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));


			//camelMainComponent.addRouteBuilder(new BuyingOpportunityForMultiplesStrategyDetectorRoute(BittrexExchange.class,SupportedCurrencyPairInternalCodes.BTCNEO,CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCNEO , Constants.BIDS_SUM_ORDER_BOOK),CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCNEO , Constants.ASKS_SUM_ORDER_BOOK),MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE, "15000","log:console" ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC NEO***/



			/*** START...Setup Bid/Ask Bitterex OrderBook with Update And metrics collection route for BTC BCC***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCNEO, "direct:"+BTC_BCC_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_BCC_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_BCC_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_BCC_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO, Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			//camelMainComponent.addRouteBuilder(new BuyingOpportunityForMultiplesStrategyDetectorRoute(BittrexExchange.class,SupportedCurrencyPairInternalCodes.BTCBCC,CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCBCC, Constants.BIDS_SUM_ORDER_BOOK),CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCBCC , Constants.ASKS_SUM_ORDER_BOOK),MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE, "15000","log:console" ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC BCC***/




			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metrics collection route for BTC USDT***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCUSDT, "direct:"+BTC_USDT_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_USDT_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE,BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_USDT_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCUSDT , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_USDT_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCUSDT , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			//camelMainComponent.addRouteBuilder(new BuyingOpportunityForMultiplesStrategyDetectorRoute(BittrexExchange.class,SupportedCurrencyPairInternalCodes.BTCUSDT,CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCUSDT, Constants.BIDS_SUM_ORDER_BOOK),CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCUSDT , Constants.ASKS_SUM_ORDER_BOOK),MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE, "15000","log:console" ));


			//*************Run Multiples strategy for BTC USDT - for now run once and ignore source endpoint for now
			//camelMainComponent.addRouteBuilder(new MultiplesAlgoStrategyRoute(MULTIPLES_STRATEGY_POLLING_STRATEGY,SupportedCurrencyPairInternalCodes.BTCUSDT, Currency.USDT, Currency.BTC, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCUSDT , Constants.BIDS_SUM_ORDER_BOOK), CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, SupportedCurrencyPairInternalCodes.BTCUSDT , Constants.ASKS_SUM_ORDER_BOOK), bitterexAccountManager, MULTIPLES_STRATEGY_ORDERBOOK_PAGE_SIZE ,  "log:console"  ));

			//***TESTING MANUAL UPLOADING OF LIMIT ORDERS*****///

			/**
			BigDecimal testQuantity = new BigDecimal(0.00100000);
			BigDecimal testPrice = new BigDecimal(7340);
			Enum testSide = Constants.ASK;
			camelMainComponent.addRouteBuilder(new ManualLimitOrderPlacementRoute(MULTIPLES_STRATEGY_POLLING_STRATEGY, SupportedCurrencyPairInternalCodes.BTCUSDT, Currency.USDT, Currency.BTC, testQuantity, testPrice, testSide, bitterexAccountManager,  "log:console"));
			**/

			//*** END TESTING MANUAL UPLOADING OF LIMIT ORDERS*****///

			/*** END...Setup Bitterex OrderBook with Update route for BTC USDT ***/



			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metric collections route for BTC OMG***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCOMG, "direct:"+BTC_OMG_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_OMG_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_OMG_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_OMG_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC OMG ***/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metric collection route for BTC PAY***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCOMG, "direct:"+BTC_PAY_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_PAY_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_PAY_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_PAY_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC PAY ***/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update route for BTC XRP***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCXRP, "direct:"+BTC_XRP_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_XRP_ASK_ORDER_BOOK_METRIC_ENDPOINT,"log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_XRP_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXRP , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_XRP_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXRP , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));

			/*** END...Setup Bitterex OrderBook with Update route for BTC XRP ***/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metrics collection route for BTC ETH***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCETH, "direct:"+BTC_ETH_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_ETH_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_ETH_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCETH , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_ETH_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCETH , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC ETH ***/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metrics collection route for BTC VTC***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCVTC, "direct:"+BTC_VTC_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_VTC_ASK_ORDER_BOOK_METRIC_ENDPOINT,"log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_VTC_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCVTC , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_VTC_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCVTC , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC VTC ***/


			/*** START...Setup Bid/Ask Bitterex OrderBook with Update and metrics collection route for BTC XLM ***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCXLM, "direct:"+BTC_XLM_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_XLM_ASK_ORDER_BOOK_METRIC_ENDPOINT, "log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_XLM_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXLM , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_XLM_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXLM , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC XLM ***/

			/*** START...Setup Bid/Ask Bitterex OrderBook with Update route for BTC LTC***/
			setupOrderBookForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, BTCLTC, "direct:"+BTC_LTC_BID_ORDER_BOOK_METRIC_ENDPOINT, "direct:"+BTC_LTC_ASK_ORDER_BOOK_METRIC_ENDPOINT,"log:console", BittrexExchange.class, BITTREX_EXCHANGE_FEE, BITREX_INVERSE_EXCHANGE_FEE);
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_LTC_BID_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCLTC , Constants.BIDS_SUM_ORDER_BOOK), Constants.BID ));
			camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(BTC_LTC_ASK_ORDER_BOOK_METRIC_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCLTC , Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK ));
			/*** END...Setup Bitterex OrderBook with Update route for BTC LTC ***/



			/** OPTIONAL - FOR DEBUGGING ADD OrderBookViewer for debugging - make sure the endpoint used is a target for one or more of the above routes **/
			//addOrderBookViewerToRCurrencyPairs(camelMainComponent);


			camelMainComponent.enableTrace();
			camelMainComponent.run(args);

		} catch (IOException e) {
			logger.error("Error Starting Rippler Algo", e);
		} catch (Exception e) {
			logger.error("Error Starting Rippler Algo", e);
		}


	}

	private static void addOrderBookViewerToRCurrencyPairs(Main camelMainComponent) {
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_NEO_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_NEO_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCNEO , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_OMG_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_OMG_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCOMG , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_PAY_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCPAY , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_PAY_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCPAY , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_XRP_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXRP , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_XRP_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXRP , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_ETH_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCETH , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_ETH_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCETH , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_VTC_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCVTC , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_VTC_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCVTC , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_XLM_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXLM , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_XLM_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCXLM, Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_LTC_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCLTC , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_LTC_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCLTC, Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));

		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_USDT_BID_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCUSDT , Constants.BIDS_SUM_ORDER_BOOK), "desc", true ));
		camelMainComponent.addRouteBuilder(new OrderBookViewerRoute(BTC_USDT_ASK_ORDER_BOOK_VIEWER_ENDPOINT, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, BTCUSDT , Constants.ASKS_SUM_ORDER_BOOK), "asc", true ));
	}

	private static void setupOrderBookForCurrencyPair(OrderBookManager orderBookManager, Properties ripplerAlgoProps, SupportedExchangesManager sem, Main camelMainComponent, CurrencyPair internalCurrencyPairId, String aBidsTargetEndpoint, String aAsksTargetEndpoint, String filledTargetEndpoint, Class exchange, BigDecimal exchangeFee, BigDecimal exchangeInverseFee) {
		//Setup Bittrex OrderBook for BTCNEO
		sem.setUpBootstrapOrderBookForCurrencyPairAndExchange(BittrexExchange.class.getName(), internalCurrencyPairId, orderBookManager);
		//Setup Message Bus Topic for processing OrderBook updates for BTCNEO - will be same for both Bid/Ask - they will recieve same message in parallel, so can process in parallel
		String currencyPairOrderBookUpdateMessageBusTopic = MessageBusTopicIdentifierHelper.getTopicIdentifer(BittrexExchange.class, internalCurrencyPairId, null);
		String currencyPairBidOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.BIDS_ORDER_BOOK);
		String currencyPairBidSumsOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.BIDS_SUM_ORDER_BOOK);
		String currencyPairAskOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.ASKS_ORDER_BOOK);
		String currencyPairAskSumsOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.ASKS_SUM_ORDER_BOOK);

		String buysVolumeForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencyBuysVolumeMapId(BittrexExchange.class, internalCurrencyPairId);
		String sellsVolumeForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencySellsVolumeMapId(BittrexExchange.class, internalCurrencyPairId);


		String buysFillCountForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencyBuysFillCountMapId(BittrexExchange.class, internalCurrencyPairId);
		String sellsFillCountForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencySellsFillCountMapId(BittrexExchange.class, internalCurrencyPairId);

		camelMainComponent.addRouteBuilder(new BidOrderBookUpdateRoute(currencyPairOrderBookUpdateMessageBusTopic, "group1", currencyPairBidOrderBookCacheId,currencyPairBidSumsOrderBookCacheId, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT),aBidsTargetEndpoint, BittrexExchange.class));
		camelMainComponent.addRouteBuilder(new AskOrderBookUpdateRoute(currencyPairOrderBookUpdateMessageBusTopic,"group2", currencyPairAskOrderBookCacheId, currencyPairAskSumsOrderBookCacheId, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), aAsksTargetEndpoint, BittrexExchange.class));
		// TODO: Market Service Not needed yet - maybe needed later MarketDataService exchangeMarketDataService = sem.getMarketDataServiceForExchange(exchange.getName());

		//*** GETs FILLED ORDER UPDATES FOR PROCESSING***//
		//camelMainComponent.addRouteBuilder(new FilledOrderBookUpdateRoute(supportedCrossExchangeCurrencyPairs.get(internalCurrencyPairId),currencyPairOrderBookUpdateMessageBusTopic,"group3",buysVolumeForExchangeCurrencyPair , sellsVolumeForExchangeCurrencyPair, buysFillCountForExchangeCurrencyPair, sellsFillCountForExchangeCurrencyPair, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), filledTargetEndpoint, exchangeFee, exchangeInverseFee));
		//camelMainComponent.addRouteBuilder(new FilledOrderBookROCRoute(supportedCrossExchangeCurrencyPairs.get(internalCurrencyPairId),currencyPairOrderBookUpdateMessageBusTopic,"group4",buysVolumeForExchangeCurrencyPair , sellsVolumeForExchangeCurrencyPair, buysFillCountForExchangeCurrencyPair, sellsFillCountForExchangeCurrencyPair, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), filledTargetEndpoint, exchangeFee, exchangeInverseFee));

	}

	/**
	private static void setupMarketSummaries(Class exchange, Main camelMainComponent, Properties ripplerAlgoProps, String targetEndpoint, HazelcastInstance distributedCache){


		IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchange));
		exchangeVolumesPerCurrencyPair.clear();


		IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5SecROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume5SecROCTrackerId(exchange));
		exchangeVolumesPerCurrencyPair5SecROC.clear();

		IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair50SecROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume50SecROCTrackerId(exchange));
		exchangeVolumesPerCurrencyPair50SecROC.clear();

		IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5MinROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume5minSecROCTrackerId(exchange));
		exchangeVolumesPerCurrencyPair5MinROC.clear();

		IMap<String, MarketSummary> marketSummaryMap = distributedCache.getMap(CacheIdentifierHelper.getMarketSummaryMapId(exchange));
		marketSummaryMap.clear();

		IMap<String, Integer> currencyAggregatedSignalCountMap = distributedCache.getMap(CacheIdentifierHelper.getExchangeAggregatedSignalCount(exchange));
		currencyAggregatedSignalCountMap.clear();

		//Create the route to consume exchange volumes in real time
		camelMainComponent.addRouteBuilder(new MarketSummariesRoute(exchange, MessageBusTopicIdentifierHelper.getSummariesTopicIdentifier(exchange), ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), "summaries11", targetEndpoint));
		//create & clear the volume cache for this exchange

		//TODO: Change log;console endpoint to a kafka topic that the GUI will subscribe too..
		camelMainComponent.addRouteBuilder(new MarketSummaries5SecVolROCRoute(exchange, "kafka:cryptoalgoalerts2"));
		camelMainComponent.addRouteBuilder(new MarketSummaries5SecAggregatorVolROCRoute(exchange, "kafka:cryptoalgoalertsAggregated"));
		camelMainComponent.addRouteBuilder(new MarketSummaries5SecVolROCStricterRoute(exchange, "kafka:cryptoalgoalertsStrict"));
		//camelMainComponent.addRouteBuilder(new MarketSummaries50SecVolROCRoute(exchange, "log:console"));
		//camelMainComponent.addRouteBuilder(new MarketSummaries5MinVolROCRoute(exchange, "log:console"));

	}**/


}