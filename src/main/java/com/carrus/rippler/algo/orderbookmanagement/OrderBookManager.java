package com.carrus.rippler.algo.orderbookmanagement;


import com.carrus.rippler.algo.RipplerAlgo;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.PriceRoundingHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.service.marketdata.MarketDataService;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class OrderBookManager {

    final static Logger logger = Logger.getLogger(OrderBookManager.class);

    private HazelcastInstance instance;

    public OrderBookManager(HazelcastInstance instance){
        super();

        this.instance = instance;
        setupMetricsCaches();

    }


    public void setupRipplerVirtualOrderBookForExchangeAndCurrencyPair(Class exchangeClass, CurrencyPair aCurrencyPair, MarketDataService marketDataService){
        //Populate Bid and Ask order Books

        logger.info("Populating Order Book Bid/Ask (sums) caches");
        try {

            if(marketDataService!=null) {


                OrderBook orderBook = marketDataService.getOrderBook(aCurrencyPair, 100000000);

                //Identifier for any Hazelcast cache must be prexfixed by exchangeclassname_currencypair_nameofcache - user CacheIdentifierHelper Utility to get the right name


                //Prepare Bid And Ask Order Book Caches  - where limitPrice maps to tradableAmount of LimitOrder

                logger.info("*****Preparing Order Book Bid/Ask (sums) caches for " + exchangeClass.getName() + " " + aCurrencyPair + "**********");

                String bidOrderBookCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(exchangeClass, aCurrencyPair, Constants.BIDS_ORDER_BOOK);
                IMap<BigDecimal, BigDecimal> orderBookBids = instance.getMap(bidOrderBookCacheIdentifier);
                //clear the orderbook incase populated
                orderBookBids.clear();

                String askOrderBookCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(exchangeClass, aCurrencyPair, Constants.ASKS_ORDER_BOOK);
                IMap<BigDecimal, BigDecimal> orderBookAsks = instance.getMap(askOrderBookCacheIdentifier);
                //clear the orderbook incase populated
                orderBookAsks.clear();

                //Prepare Bid And Ask Order Book caches with sums - where limitPrice maps to limitPrice*tradeableAmount (Sum) of LimitOrder

                String bidSumOrderBookCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(exchangeClass, aCurrencyPair, Constants.BIDS_SUM_ORDER_BOOK);
                IMap<BigDecimal, BigDecimal> orderBookBidsSum = instance.getMap(bidSumOrderBookCacheIdentifier);
                //clear the orderbook incase populated
                orderBookBidsSum.clear();

                String askSumOrderBookCacheIdentifier = CacheIdentifierHelper.getCacheIdentifer(exchangeClass, aCurrencyPair, Constants.ASKS_SUM_ORDER_BOOK);
                IMap<BigDecimal, BigDecimal> orderBookAsksSum = instance.getMap(askSumOrderBookCacheIdentifier);
                //clear the orderbook incase populated
                orderBookAsksSum.clear();
                logger.info("Order Book Bid/Ask (sums) caches setup complete");

                //Obtain starting volume for this currency pair
                String buysExchangeCurrencyPairVolumeKey = CacheIdentifierHelper.getExchangeCurrencyBuysVolumeMapId(exchangeClass, aCurrencyPair);
                IMap<String, BigDecimal> buysVolumeForCurrencyPair = instance.getMap(buysExchangeCurrencyPairVolumeKey);
                buysVolumeForCurrencyPair.clear();

                String sellsExchangeCurrencyPairVolumeKey = CacheIdentifierHelper.getExchangeCurrencySellsVolumeMapId(exchangeClass, aCurrencyPair);
                IMap<String, BigDecimal> sellsVolumeForCurrencyPair = instance.getMap(sellsExchangeCurrencyPairVolumeKey);
                sellsVolumeForCurrencyPair.clear();

                //Obtain starting counts for this currency pair
                String buysExchangeCurrencyPairCountKey = CacheIdentifierHelper.getExchangeCurrencyBuysFillCountMapId(exchangeClass, aCurrencyPair);
                IMap<String, Integer> buysFillCountForCurrencyPair = instance.getMap(buysExchangeCurrencyPairCountKey);
                buysFillCountForCurrencyPair.clear();

                String sellsExchangeCurrencyPairFillsCountKey = CacheIdentifierHelper.getExchangeCurrencySellsFillCountMapId(exchangeClass, aCurrencyPair);
                IMap<String, Integer> sellsFillCountForCurrencyPair = instance.getMap(sellsExchangeCurrencyPairFillsCountKey);
                sellsFillCountForCurrencyPair.clear();

                IMap<String, Date> orderFeedUpdateMap = instance.getMap(CacheIdentifierHelper.getOrderFeedStatusTimerCache(exchangeClass));
                orderFeedUpdateMap.clear();

                IMap<String, BigDecimal> vrocCurrencyExchangeMap = instance.getMap(CacheIdentifierHelper.getVROCForCurrencyExchange(exchangeClass));
                vrocCurrencyExchangeMap.clear();




                //Populate Bids & bidSums
                for (LimitOrder limitOrder : orderBook.getBids()) {

                    orderBookBids.put(PriceRoundingHelper.getRoundedAndFormattedPrice(limitOrder.getLimitPrice()), limitOrder.getRemainingAmount());

                    orderBookBidsSum.put(PriceRoundingHelper.getRoundedAndFormattedPrice(limitOrder.getLimitPrice()), limitOrder.getRemainingAmount().multiply(limitOrder.getLimitPrice()));

                }
                logger.info("Order Book caches populated: Bids " + orderBookBids.size());


                //Populate Asks & askSums
                for (LimitOrder limitOrder : orderBook.getAsks()) {

                    orderBookAsks.put(PriceRoundingHelper.getRoundedAndFormattedPrice(limitOrder.getLimitPrice()), limitOrder.getRemainingAmount());

                    orderBookAsksSum.put(PriceRoundingHelper.getRoundedAndFormattedPrice(limitOrder.getLimitPrice()), limitOrder.getRemainingAmount().multiply(limitOrder.getLimitPrice()));

                }
                logger.info("Order Book caches populated: Asks " + orderBookAsks.size());


                logger.info("*****COMPLETED Preparing Order Book Bid/Ask (sums) caches for " + exchangeClass.getName() + " " + aCurrencyPair + "**********");

            }else{

                logger.warn("Market Data Service not returned for " + aCurrencyPair + " implies the market must be OFFLINE");
            }

        } catch (IOException e) {

            logger.error("ERROR SETTING UP ORDER BOOK CACHES", e);
        }


    }

    private void setupMetricsCaches(){

        /** Get Algo Metrics Cache and clear it**/
        IMap<String, BigDecimal> algoMetricCache  = instance.getMap(Constants.ALGO_METRICS_CACHE.name());
        algoMetricCache.clear();

        /** Get cache for the multiples per currency pair detector **/
        IMap<String, Boolean> buyingOpportunityPerCurrencyPairCache = instance.getMap(Constants.BUY_OPPORTUNITY_FOR_MULTIPLES_STRATEGY_DETECTOR_CACHE.name());
        buyingOpportunityPerCurrencyPairCache.clear();


    }




}
