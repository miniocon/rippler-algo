package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes;


import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors.FilledOrderBookUpdateProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;


/**
 * Created by usmankhan on 21/10/2017.
 */
public class FilledOrderBookUpdateRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(FilledOrderBookUpdateRoute.class);


    private String messageBusTopic;
    private String exchangeCurrencyBuysVolumeCacheId;
    private String exchangeCurrencySellsVolumeCacheId;
    private String exchangeCurrencyBuysFillCountCacheId;
    private String exchangeCurrencySellsFillCountCacheId;
    private String messageBrokerURL;
    private String messageBrokerPort;
    private String groupId;
    private CurrencyPair currencyPair;
    private String targetEndpoint;
    private BigDecimal exchangeFee;
    private BigDecimal exchangeInverseFee;



    public FilledOrderBookUpdateRoute(CurrencyPair currencyPair, String messageBusTopic, String groupId, String exchangeCurrencyBuysVolumeCacheId, String exchangeCurrencySellsVolumeCacheId,String exchangeCurrencyBuysFillCountCacheId, String exchangeCurrencySellsFillCountCacheId, String messageBrokerURL, String messageBrokerPort, String targetEndpoint, BigDecimal exchangeFee, BigDecimal exchangeInverseFee){
        super();

        this.currencyPair = currencyPair;
        this.messageBusTopic = messageBusTopic;
        this.exchangeCurrencyBuysVolumeCacheId = exchangeCurrencyBuysVolumeCacheId;
        this.exchangeCurrencySellsVolumeCacheId = exchangeCurrencySellsVolumeCacheId;

        this.exchangeCurrencyBuysFillCountCacheId = exchangeCurrencyBuysFillCountCacheId;
        this.exchangeCurrencySellsFillCountCacheId = exchangeCurrencySellsFillCountCacheId;
        this.messageBrokerURL = messageBrokerURL;
        this.messageBrokerPort = messageBrokerPort;
        this.groupId = groupId;
        this.targetEndpoint = targetEndpoint;
        this.exchangeFee = exchangeFee;
        this.exchangeInverseFee = exchangeInverseFee;

    }


    public void configure() throws Exception {



        logger.info("Setting up FilledOrderBookUpdate Route..with: " + messageBusTopic + ", " + exchangeCurrencyBuysVolumeCacheId +  ", " + this.targetEndpoint);

        setupBroker();
       //Example: from("timer://runOnce?repeatCount=5").process(new BidOrderBookUpdateProcessor(this.exchangeCurrencyBuysVolumeCacheId)).to("log:console");

        from("kafka:"+messageBusTopic+"?groupId="+groupId+"&autoOffsetReset=latest&consumersCount=1")
                .unmarshal().json(JsonLibrary.Jackson, OrderBookUpdate.class)
                .process(new FilledOrderBookUpdateProcessor(this.exchangeCurrencyBuysVolumeCacheId,this.exchangeCurrencySellsVolumeCacheId, exchangeCurrencyBuysFillCountCacheId, exchangeCurrencySellsFillCountCacheId, currencyPair, exchangeFee, exchangeInverseFee))
                .to(targetEndpoint);



        logger.info("Completed setting up Route for FilledOrderBookUpdate Route...with " + messageBusTopic + ", " + exchangeCurrencyBuysVolumeCacheId +  ", " + this.targetEndpoint);

    }


    private void setupBroker(){


        if(this.getContext().getComponent("kafka")==null) {


            KafkaComponent kafka = new KafkaComponent();
            kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);

            this.getContext().addComponent("kafka", kafka);
            logger.info("NEW KAFKA BROKER Component created in CAMEL CONTEXT");
        }else{



            //Check that Kafka component has its brokers set
            KafkaComponent kafka = (KafkaComponent)this.getContext().getComponent("kafka");

            logger.info("KAFKA BROKER FOUND IN CAMEL CONTEXT");

            if(kafka.getBrokers()==null ||  "".equals(kafka.getBrokers()) ){

                kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);
                logger.info("KAFKA BROKERS WERE NOT SET and ARE NOW SET: " + kafka.getBrokers());
            }else{

                logger.info("KAFKA BROKERS DO EXIST: " + kafka.getBrokers());
            }


        }



    }
}
