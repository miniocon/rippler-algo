package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.LimitOrderUpdate;
import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Map;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class AskOrderBookUpdateProcessor implements Processor {

    final static Logger logger = Logger.getLogger(AskOrderBookUpdateProcessor.class);

    private String askOrderBookCacheId;
    private String askSumsOrderBookCacheId;
    private Class exchangeClass;

    public AskOrderBookUpdateProcessor(String askOrderBookCacheId, String askSumsOrderBookCacheId, Class exchangeClass){
        super();
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.askSumsOrderBookCacheId = askSumsOrderBookCacheId;
        this.exchangeClass = exchangeClass;
        logger.info("AskOrderBookUpdated Processor created for : " + askOrderBookCacheId +", "+askSumsOrderBookCacheId+ ", exchange: " + exchangeClass.getSimpleName());

    }



    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");
        logger.debug("Obtained distributedCacheInstance");

        IMap<String, Date> orderFeedStatusUpdate = distributedCacheInstance.getMap(CacheIdentifierHelper.getOrderFeedStatusTimerCache(exchangeClass));

        orderFeedStatusUpdate.put(Constants.LAST_ORDER_UPDATE.name(), new Date());

        Map<BigDecimal, BigDecimal> orderBookAsks = distributedCacheInstance.getMap(askOrderBookCacheId);

        Map<BigDecimal, BigDecimal> orderBookAsksSums = distributedCacheInstance.getMap(askSumsOrderBookCacheId);


        logger.debug("Obtained map of orderBookAsks & askSums");

        logger.debug(exchange.getIn().getBody());

        OrderBookUpdate deltas = (OrderBookUpdate) exchange.getIn().getBody();

        LimitOrderUpdate[] askDeltas = deltas.getAsks();


        for(LimitOrderUpdate update : askDeltas){

            if(update.getType() == 0){

                orderBookAsksSums.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity().multiply(update.getRate()));
                orderBookAsks.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity());

                logger.debug("[ASK ORDER DELTA PROCESSOR] New Order added: rate=" + update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " , qty=" + update.getQuantity());
            } else if(update.getType() == 1){


                orderBookAsksSums.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookAsks.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());

                logger.debug("[ASK ORDER DELTA PROCESSOR] Order removed: " + "rate="+update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " , qty=" + update.getQuantity());

            } else if(update.getType() == 2){

                orderBookAsksSums.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookAsksSums.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity().multiply(update.getRate()));

                orderBookAsks.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookAsks.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity());

                logger.debug("[ASK ORDER DELTA PROCESSOR] Order updated : " + "rate=" + update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " qty="+update.getQuantity());


            }else{

                logger.debug("Type not understood: " + update.getType());
            }




        }




    }
}
