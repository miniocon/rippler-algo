package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors;

import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookManager;
import com.carrus.rippler.algo.supportedexchanges.SupportedExchangesManager;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.util.List;

/**
 * Created by usmankhan on 26/12/2017.
 */
public class OrderBootstrapProcessor implements Processor {

    final static Logger logger = Logger.getLogger(OrderBootstrapProcessor.class);

    private OrderBookManager orderBookManager;
    private SupportedExchangesManager sem;
    private List<CurrencyPair> currencyPairs;
    private Class exchangeName;


    public OrderBootstrapProcessor(Class exchangeName, OrderBookManager orderBookManager, SupportedExchangesManager sem, List<CurrencyPair> currencyPairs){

        this.orderBookManager = orderBookManager;
        this.sem = sem;
        this.currencyPairs = currencyPairs;
        this.exchangeName = exchangeName;



    }

    @Override
    public void process(Exchange exchange)  {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

        IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);



        try {

            logger.info("Setting up Bootstraps per currency pair");
            //Loop through currency pairs and bootstrap

            int currencyCount = 0;
            for (CurrencyPair currencyPair : currencyPairs) {


                if (RipplerAlgoAutoMultiCurrency.currencyPairIgnoreList.get(currencyPair.toString()) == null) {

                    sem.setUpBootstrapOrderBookForCurrencyPairAndExchange(exchangeName.getName(), currencyPair, orderBookManager);
                    currencyCount++;
                    logger.info("Completed bootstrap of " + currencyCount + " " + currencyPair);

                }else{
                    //do nothing
                }


            }

            logger.info("Completed Bootstraps for : " + currencyCount + " currency pairs");

             bootstrapStatusMap.put(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY, true);

        }catch(Exception e){

            logger.error("Failed to bootstrap orders", e);
        }


    }
}
