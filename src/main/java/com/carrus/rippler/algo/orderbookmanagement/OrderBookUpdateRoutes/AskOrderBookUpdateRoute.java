package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes;


import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors.AskOrderBookUpdateProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;
import org.knowm.xchange.Exchange;


/**
 * Created by usmankhan on 21/10/2017.
 */
public class AskOrderBookUpdateRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(AskOrderBookUpdateRoute.class);

    private String messageBusTopic;
    private String askOrderBookCacheId;
    private String askSumsOrderBookCacheId;
    private String messageBrokerURL;
    private String messageBrokerPort;
    private String groupId;
    private String targetEndpoint;
    private Class exchangeClass;


    public AskOrderBookUpdateRoute(String messageBusTopic,String groupId, String askOrderBookCacheId, String askSumsOrderBookCacheId, String messageBrokerURL, String messageBrokerPort, String targetEndpoint, Class exchangeClass){
        super();
        this.messageBusTopic = messageBusTopic;
        this.askOrderBookCacheId = askOrderBookCacheId;
        this.messageBrokerURL = messageBrokerURL;
        this.messageBrokerPort = messageBrokerPort;
        this.askSumsOrderBookCacheId = askSumsOrderBookCacheId;
        this.groupId = groupId;
        this.targetEndpoint = targetEndpoint;
        this.exchangeClass = exchangeClass;

    }


    public void configure()  {


        try{
            logger.info("Setting up AskOrderBookUpdate Route..with: " + messageBusTopic + ", " + askOrderBookCacheId + ", " + askSumsOrderBookCacheId + ", " + this.targetEndpoint);

            setupBroker();
            //Example: from("timer://runOnce?repeatCount=5").process(new AskOrderBookUpdateProcessor(this.bidOrderBookCacheId)).to("log:console");

            from("kafka:"+messageBusTopic+"?groupId="+groupId+"&autoOffsetReset=latest&consumersCount=1")
                    .unmarshal().json(JsonLibrary.Jackson, OrderBookUpdate.class)
                    .process(new AskOrderBookUpdateProcessor(this.askOrderBookCacheId, this.askSumsOrderBookCacheId, this.exchangeClass))
                    .to(targetEndpoint);



            logger.info("Completed setting up Route for AskOrderBookUpdate Route...with " + messageBusTopic + ", " + askOrderBookCacheId + ", " + askSumsOrderBookCacheId + ", " + this.targetEndpoint);

        }catch(Exception e){
            logger.error("Error setting up route ", e);
        }

    }


    private void setupBroker() throws Exception{


        if(this.getContext().getComponent("kafka")==null) {


            KafkaComponent kafka = new KafkaComponent();
            kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);

            this.getContext().addComponent("kafka", kafka);
            logger.info("NEW KAFKA BROKER Component created in CAMEL CONTEXT");
        }else{



            //Check that Kafka component has its brokers set
            KafkaComponent kafka = (KafkaComponent)this.getContext().getComponent("kafka");

            logger.info("KAFKA BROKER FOUND IN CAMEL CONTEXT");

            if(kafka.getBrokers()==null ||  "".equals(kafka.getBrokers()) ){

                kafka.setBrokers(messageBrokerURL+":"+messageBrokerPort);
                logger.info("KAFKA BROKERS WERE NOT SET and ARE NOW SET: " + kafka.getBrokers());
            }else{

                logger.info("KAFKA BROKERS DO EXIST: " + kafka.getBrokers());
            }


        }



    }
}
