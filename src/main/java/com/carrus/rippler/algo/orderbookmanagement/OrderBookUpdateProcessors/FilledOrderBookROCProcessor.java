package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.FillOrderUpdate;
import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.hazelcast.core.HazelcastInstance;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class FilledOrderBookROCProcessor implements Processor {

    final static Logger logger = Logger.getLogger(FilledOrderBookROCProcessor.class);

    //Exchange level orderBook Cache - Key = currency pair value = volume
    private String buysfilledOrderVolumeForCurrencyPair;
    private String sellsfilledOrderVolumeForCurrencyPair;

    private String buysfilledOrderCountForCurrencyPair;
    private String sellsfilledOrderCountForCurrencyPair;


    private CurrencyPair currencyPair;
    private BigDecimal exchangeFee;
    private BigDecimal exchangeInverseFee;

    private BigDecimal currentBuysFilledVolume;
    private BigDecimal currentSellsFilledVolume;
    private int currentBuysFillsCount = 0;
    private int currentSellsFillsCount = 0;

    private BigDecimal pastBuysFilledVolume;
    private BigDecimal pastSellsFilledVolume;
    private int pastBuysFillsCount = -1;
    private int pastSellsFillsCount = -1;

    private BigDecimal buysVolDifference;
    private Integer buysCountDifference;

    private BigDecimal sellsVolDifference;
    private Integer sellsCountDifference;



    public FilledOrderBookROCProcessor(String buysFilledOrderVolumeForCurrencyPairId, String sellsfilledOrderVolumeForCurrencyPair, String buysfilledOrderCountForCurrencyPair, String sellsfilledOrderCountForCurrencyPair, CurrencyPair currencyPair, BigDecimal exchangeFee, BigDecimal exchangeInverseFee){
        super();
        this.buysfilledOrderVolumeForCurrencyPair = buysFilledOrderVolumeForCurrencyPairId;
        this.sellsfilledOrderVolumeForCurrencyPair = sellsfilledOrderVolumeForCurrencyPair;
        this.buysfilledOrderCountForCurrencyPair = buysfilledOrderCountForCurrencyPair;
        this.sellsfilledOrderCountForCurrencyPair = sellsfilledOrderCountForCurrencyPair;
        this.currencyPair = currencyPair;
        this.exchangeFee = exchangeFee;
        this.exchangeInverseFee = exchangeInverseFee;

        logger.info("buys FilledOrderBookUpdated Processor created for : " + buysfilledOrderVolumeForCurrencyPair);
        logger.info("sells FilledOrderBookUpdated Processor created for : " + sellsfilledOrderVolumeForCurrencyPair);

    }



    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");
        //logger.debug("Obtained distributedCacheInstance");

        Map<String,BigDecimal> currentFilledBuysOrderVolume = distributedCacheInstance.getMap(buysfilledOrderVolumeForCurrencyPair);
        Map<String,BigDecimal> currentFilledSellsOrderVolume = distributedCacheInstance.getMap(sellsfilledOrderVolumeForCurrencyPair);

        Map<String, Integer> currentFilledBuysOrderCount = distributedCacheInstance.getMap(buysfilledOrderCountForCurrencyPair);
        Map<String, Integer> currentFilledSellsOrderCount = distributedCacheInstance.getMap(sellsfilledOrderCountForCurrencyPair);


       currentBuysFilledVolume = currentFilledBuysOrderVolume.get(buysfilledOrderVolumeForCurrencyPair);
       currentSellsFilledVolume = currentFilledSellsOrderVolume.get(sellsfilledOrderVolumeForCurrencyPair);
       currentBuysFillsCount = currentFilledBuysOrderCount.get(buysfilledOrderCountForCurrencyPair);
       currentSellsFillsCount = currentFilledSellsOrderCount.get(sellsfilledOrderCountForCurrencyPair);

        //First time, all the past values are null, so set them to the current value
        if(pastBuysFilledVolume == null && pastBuysFillsCount == -1 && pastSellsFilledVolume == null && pastSellsFillsCount == -1){
            pastBuysFilledVolume = currentBuysFilledVolume;
            pastBuysFillsCount = currentBuysFillsCount;
            pastSellsFilledVolume = currentSellsFilledVolume;
            pastSellsFillsCount = currentSellsFillsCount;
        }else{

            //If its invoked the second time, onwards, calculate the differences

            buysVolDifference = currentBuysFilledVolume.subtract(pastBuysFilledVolume);
            buysCountDifference = currentBuysFillsCount - pastBuysFillsCount;

            sellsVolDifference = currentSellsFilledVolume.subtract(pastSellsFilledVolume);
            sellsCountDifference = currentSellsFillsCount - pastSellsFillsCount;

        }


       logger.info(currencyPair + " B Fill Vol Diff: " + buysVolDifference + " count Diff: " + buysCountDifference);
       logger.info(currencyPair + " S Fill Vol Diff: " + sellsVolDifference+ " count Diff: " + sellsCountDifference);



    }
}
