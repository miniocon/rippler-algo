package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.LimitOrderUpdate;
import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Map;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class BidOrderBookUpdateProcessor implements Processor {

    final static Logger logger = Logger.getLogger(BidOrderBookUpdateProcessor.class);

    private String bidOrderBookCacheId;
    private String bidSumsOrderBookCacheId;
    private Class exchangeClass;

    public BidOrderBookUpdateProcessor(String bidOrderBookCacheId, String bidSumsOrderBookCacheId, Class exchangeClass){
        super();
        this.bidOrderBookCacheId = bidOrderBookCacheId;
        this.bidSumsOrderBookCacheId = bidSumsOrderBookCacheId;
        this.exchangeClass = exchangeClass;
        logger.info("BidOrderBookUpdated Processor created for : " + bidOrderBookCacheId +", "+bidSumsOrderBookCacheId + ", exchange:" + exchangeClass.getSimpleName());

    }



    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");
        logger.debug("Obtained distributedCacheInstance");



        IMap<String, Date> orderFeedStatusUpdate = distributedCacheInstance.getMap(CacheIdentifierHelper.getOrderFeedStatusTimerCache(exchangeClass));

        orderFeedStatusUpdate.put(Constants.LAST_ORDER_UPDATE.name(), new Date());

        Map<BigDecimal, BigDecimal> orderBookBids = distributedCacheInstance.getMap(bidOrderBookCacheId);

        Map<BigDecimal, BigDecimal> orderBookBidsSums = distributedCacheInstance.getMap(bidSumsOrderBookCacheId);


        logger.debug("Obtained map of orderBookBids & bidSums");

        logger.debug(exchange.getIn().getBody());

        OrderBookUpdate deltas = (OrderBookUpdate) exchange.getIn().getBody();



        LimitOrderUpdate[] bidDeltas = deltas.getBids();


        for(LimitOrderUpdate update : bidDeltas){

            if(update.getType() == 0){

                orderBookBidsSums.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity().multiply(update.getRate()));
                orderBookBids.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity());

                logger.debug("[BUY ORDER DELTA PROCESSOR] New Order added: rate=" + update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " , qty=" + update.getQuantity());
            } else if(update.getType() == 1){


                orderBookBidsSums.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookBids.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());

                 logger.debug("[BUY ORDER DELTA PROCESSOR] Order removed: " + "rate="+update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " , qty=" + update.getQuantity());

            } else if(update.getType() == 2){

                orderBookBidsSums.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookBidsSums.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity().multiply(update.getRate()));

                orderBookBids.remove(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros());
                orderBookBids.put(update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros(), update.getQuantity());

                 logger.debug("[BUY ORDER DELTA PROCESSOR] Order updated : " + "rate=" + update.getRate().setScale(8, RoundingMode.HALF_UP).stripTrailingZeros() + " qty="+update.getQuantity());


            }else{

                logger.debug("Type not understood: " + update.getType());
            }




        }




    }
}
