package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes;

import com.carrus.rippler.algo.orderbookmanagement.OrderBookManager;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors.OrderBootstrapProcessor;
import com.carrus.rippler.algo.supportedexchanges.SupportedExchangesManager;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.util.List;

/**
 * Created by usmankhan on 26/12/2017.
 */
public class BootstrapOrdersRoute extends RouteBuilder {

    final static Logger logger = Logger.getLogger(BootstrapOrdersRoute.class);

    private OrderBookManager orderBookManager;
    private SupportedExchangesManager sem;
    private List<CurrencyPair> currencyPairs;
    private Class exchangeName;


    public BootstrapOrdersRoute(Class exchangeName, OrderBookManager orderBookManager, SupportedExchangesManager sem, List<CurrencyPair> currencyPairs){

        this.orderBookManager = orderBookManager;
        this.sem = sem;
        this.currencyPairs = currencyPairs;
        this.exchangeName = exchangeName;



    }

    @Override
    public void configure() throws Exception {

        logger.info("Setting up Orderbook Boostrapping for currency pairs");

        from("timer://bootstrap?repeatCount=1&delay=60000").process(new OrderBootstrapProcessor(exchangeName, orderBookManager,sem,currencyPairs)).end();

    }
}
