package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes;


import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors.FilledOrderBookROCProcessor;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors.FilledOrderBookUpdateProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by usmankhan on 21/10/2017.
 */
public class FilledOrderBookROCRoute extends RouteBuilder{

    final static Logger logger = Logger.getLogger(FilledOrderBookROCRoute.class);


    private String messageBusTopic;
    private String exchangeCurrencyBuysVolumeCacheId;
    private String exchangeCurrencySellsVolumeCacheId;
    private String exchangeCurrencyBuysFillCountCacheId;
    private String exchangeCurrencySellsFillCountCacheId;
    private String messageBrokerURL;
    private String messageBrokerPort;
    private String groupId;
    private CurrencyPair currencyPair;
    private String targetEndpoint;
    private BigDecimal exchangeFee;
    private BigDecimal exchangeInverseFee;



    public FilledOrderBookROCRoute(CurrencyPair currencyPair, String messageBusTopic, String groupId, String exchangeCurrencyBuysVolumeCacheId, String exchangeCurrencySellsVolumeCacheId, String exchangeCurrencyBuysFillCountCacheId, String exchangeCurrencySellsFillCountCacheId, String messageBrokerURL, String messageBrokerPort, String targetEndpoint, BigDecimal exchangeFee, BigDecimal exchangeInverseFee){
        super();

        this.currencyPair = currencyPair;
        this.messageBusTopic = messageBusTopic;
        this.exchangeCurrencyBuysVolumeCacheId = exchangeCurrencyBuysVolumeCacheId;
        this.exchangeCurrencySellsVolumeCacheId = exchangeCurrencySellsVolumeCacheId;

        this.exchangeCurrencyBuysFillCountCacheId = exchangeCurrencyBuysFillCountCacheId;
        this.exchangeCurrencySellsFillCountCacheId = exchangeCurrencySellsFillCountCacheId;
        this.messageBrokerURL = messageBrokerURL;
        this.messageBrokerPort = messageBrokerPort;
        this.groupId = groupId;
        this.targetEndpoint = targetEndpoint;
        this.exchangeFee = exchangeFee;
        this.exchangeInverseFee = exchangeInverseFee;

    }


    public void configure() throws Exception {



        logger.info("Setting up FilledOrderBookROC Route..with: " + messageBusTopic + ", " + exchangeCurrencyBuysVolumeCacheId +  ", " + this.targetEndpoint);


        from("timer://evaluateFilledVolCountROCs"+new Date().getTime()+"?fixedRate=true&period=10000&delay=30000").process(new FilledOrderBookROCProcessor(this.exchangeCurrencyBuysVolumeCacheId,this.exchangeCurrencySellsVolumeCacheId, exchangeCurrencyBuysFillCountCacheId, exchangeCurrencySellsFillCountCacheId, currencyPair, exchangeFee, exchangeInverseFee)).to(this.targetEndpoint);



        /**

        from("kafka:"+messageBusTopic+"?groupId="+groupId+"&autoOffsetReset=latest&consumersCount=1")
                .unmarshal().json(JsonLibrary.Jackson, OrderBookUpdate.class)
                .process(new FilledOrderBookUpdateProcessor(this.exchangeCurrencyBuysVolumeCacheId,this.exchangeCurrencySellsVolumeCacheId, exchangeCurrencyBuysFillCountCacheId, exchangeCurrencySellsFillCountCacheId, currencyPair, exchangeFee, exchangeInverseFee))
                .to(targetEndpoint);  **/



        logger.info("Completed setting up Route for FilledOrderBookROC Route...with " + messageBusTopic + ", " + exchangeCurrencyBuysVolumeCacheId +  ", " + this.targetEndpoint);

    }


}
