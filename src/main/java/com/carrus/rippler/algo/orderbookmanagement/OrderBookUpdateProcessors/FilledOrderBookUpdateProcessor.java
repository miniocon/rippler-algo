package com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateProcessors;

import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.FillOrderUpdate;
import com.carrus.rippler.algo.model.OrderBookUpdate;
import com.hazelcast.core.HazelcastInstance;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.service.marketdata.MarketDataService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class FilledOrderBookUpdateProcessor implements Processor {

    final static Logger logger = Logger.getLogger(FilledOrderBookUpdateProcessor.class);

    //Exchange level orderBook Cache - Key = currency pair value = volume
    private String buysfilledOrderVolumeForCurrencyPair;
    private String sellsfilledOrderVolumeForCurrencyPair;

    private String buysfilledOrderCountForCurrencyPair;
    private String sellsfilledOrderCountForCurrencyPair;


    private boolean hasMarketDataServiceBeenInvoked;
    private CurrencyPair currencyPair;
    private BigDecimal exchangeFee;
    private BigDecimal exchangeInverseFee;
    private BigDecimal currentBuysFilledVolume;
    private BigDecimal currentsellsFilledVolume;
    private int buysFillsCount = 0;
    private int sellsFillsCount = 0;

    public FilledOrderBookUpdateProcessor(String buysFilledOrderVolumeForCurrencyPairId, String sellsfilledOrderVolumeForCurrencyPair, String buysfilledOrderCountForCurrencyPair, String sellsfilledOrderCountForCurrencyPair, CurrencyPair currencyPair,  BigDecimal exchangeFee, BigDecimal exchangeInverseFee){
        super();
        this.buysfilledOrderVolumeForCurrencyPair = buysFilledOrderVolumeForCurrencyPairId;
        this.sellsfilledOrderVolumeForCurrencyPair = sellsfilledOrderVolumeForCurrencyPair;
        this.buysfilledOrderCountForCurrencyPair = buysfilledOrderCountForCurrencyPair;
        this.sellsfilledOrderCountForCurrencyPair = sellsfilledOrderCountForCurrencyPair;
        this.currencyPair = currencyPair;
        this.hasMarketDataServiceBeenInvoked = false;
        this.exchangeFee = exchangeFee;
        this.exchangeInverseFee = exchangeInverseFee;

        logger.info("buys FilledOrderBookUpdated Processor created for : " + buysfilledOrderVolumeForCurrencyPair);
        logger.info("sells FilledOrderBookUpdated Processor created for : " + sellsfilledOrderVolumeForCurrencyPair);

    }



    public void process(Exchange exchange) throws Exception {

        HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");
        //logger.debug("Obtained distributedCacheInstance");

        Map<String,BigDecimal> currentFilledBuysOrderVolume = distributedCacheInstance.getMap(buysfilledOrderVolumeForCurrencyPair);
        Map<String,BigDecimal> currentFilledSellsOrderVolume = distributedCacheInstance.getMap(sellsfilledOrderVolumeForCurrencyPair);

        Map<String, Integer> currentFilledBuysOrderCount = distributedCacheInstance.getMap(buysfilledOrderCountForCurrencyPair);
        Map<String, Integer> currentFilledSellsOrderCount = distributedCacheInstance.getMap(sellsfilledOrderCountForCurrencyPair);


        logger.debug(exchange.getIn().getBody());

        OrderBookUpdate deltas = (OrderBookUpdate) exchange.getIn().getBody();


        FillOrderUpdate[] fillDeltas = deltas.getFills();





        for(FillOrderUpdate update : fillDeltas){


            if(Constants.BUY.toString().equals(update.getOrderType())){

                BigDecimal price = update.getPrice().stripTrailingZeros();
                BigDecimal qty = update.getAmount().stripTrailingZeros();


                BigDecimal sum = price.multiply(qty);

                if(currentBuysFilledVolume == null){
                    currentBuysFilledVolume = sum;
                }else{
                    currentBuysFilledVolume =currentBuysFilledVolume.add(sum);
                }


                buysFillsCount++;

                logger.debug(currencyPair + " Filled Buys volume:  " + currentBuysFilledVolume );



            }else if(Constants.SELL.toString().equals(update.getOrderType())){

                BigDecimal price = update.getPrice().stripTrailingZeros();
                BigDecimal qty = update.getAmount().stripTrailingZeros();


                BigDecimal sum = price.multiply(qty);

                if(currentsellsFilledVolume==null){
                    currentsellsFilledVolume = sum;

                }else{
                    currentsellsFilledVolume = currentsellsFilledVolume.add(sum);

                }


                sellsFillsCount ++;
                logger.debug(currencyPair + "Filled Sells: " + currentsellsFilledVolume);


            }else{

                logger.debug("Order Type not understood: " + update.getOrderType());
            }


        }

        if(currentBuysFilledVolume!=null) {

            logger.debug(currencyPair + " B Fills " + currentBuysFilledVolume + " count: " + buysFillsCount);

            currentFilledBuysOrderVolume.put(buysfilledOrderVolumeForCurrencyPair, currentBuysFilledVolume);
            currentFilledBuysOrderCount.put(buysfilledOrderCountForCurrencyPair, buysFillsCount);
        }

        if(currentsellsFilledVolume!=null) {

            logger.debug(currencyPair + " S Fills " + currentsellsFilledVolume + " count: " + sellsFillsCount);

            currentFilledSellsOrderVolume.put(sellsfilledOrderVolumeForCurrencyPair, currentsellsFilledVolume);
            currentFilledSellsOrderCount.put(sellsfilledOrderCountForCurrencyPair, sellsFillsCount);
        }



    }
}
