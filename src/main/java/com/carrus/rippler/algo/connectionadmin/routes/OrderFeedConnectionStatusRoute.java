package com.carrus.rippler.algo.connectionadmin.routes;

import com.carrus.rippler.algo.connectionadmin.processors.OrderFeedConnectionStatusProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import java.util.Date;

public class OrderFeedConnectionStatusRoute extends RouteBuilder {

    private Class exchange;
    private String orderFeedKillScriptLocation;

    public OrderFeedConnectionStatusRoute(Class exchange, String orderFeedKillScriptLocation){

        this.exchange = exchange;
        this.orderFeedKillScriptLocation = orderFeedKillScriptLocation;


    }


    final static Logger logger = Logger.getLogger(OrderFeedConnectionStatusRoute.class);

    @Override
    public void configure()  {

        try{

            //Every 10 seconds check if message has come through

            from("timer://checkLastOrderUpdate"+new Date().getTime()+"?fixedRate=true&period=5000&delay=60000").process(new OrderFeedConnectionStatusProcessor(exchange, orderFeedKillScriptLocation)).to("log:console");


        }catch(Exception e){

            logger.error("Error starting route", e);

        }







    }
}
