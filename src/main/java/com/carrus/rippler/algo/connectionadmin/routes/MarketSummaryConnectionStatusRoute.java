package com.carrus.rippler.algo.connectionadmin.routes;

import com.carrus.rippler.algo.connectionadmin.processors.MarketSummaryConnectionStatusProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import java.util.Date;

public class MarketSummaryConnectionStatusRoute extends RouteBuilder {

    private Class exchange;
    private String summariesKillScriptLocation;

    public MarketSummaryConnectionStatusRoute(Class exchange, String summariesKillScriptLocation){

        this.exchange = exchange;
        this.summariesKillScriptLocation = summariesKillScriptLocation;


    }


    final static Logger logger = Logger.getLogger(MarketSummaryConnectionStatusRoute.class);

    @Override
    public void configure()  {

        try{

            //Every 10 seconds check if message has come through

            from("timer://checkLastMarketSummaryUpdate"+new Date().getTime()+"?fixedRate=true&period=5000&delay=60000").process(new MarketSummaryConnectionStatusProcessor(exchange, summariesKillScriptLocation)).to("log:console");


        }catch(Exception e){

            logger.error("Error starting route", e);

        }







    }
}
