package com.carrus.rippler.algo.connectionadmin.processors;


import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

public class MarketSummaryConnectionStatusProcessor implements Processor {

    final static Logger logger = Logger.getLogger(MarketSummaryConnectionStatusProcessor .class);

    private long timeIntervalInMilliseconds  = 30000;

    private Class exchangeClass;
    private String summariesKillScriptLocation;
    private int lastUpdateDeltaNullCount = 0;

    public MarketSummaryConnectionStatusProcessor(Class exchangeClass, String summariesKillScriptLocation){

        this.exchangeClass = exchangeClass;
        this.summariesKillScriptLocation = summariesKillScriptLocation;


    }


    @Override
    public void process(Exchange exchange)  {

        try{

            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

            IMap<String, Date> marketSummaryUpdateMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getMarketSummaryStatusTimerCache(exchangeClass));

            Date lastUpdated = marketSummaryUpdateMap.get(Constants.LAST_MARKET_SUMMARY_UPDATE.name());

            Date now = new Date();

            //if now - lastUpdated is >=30 sec - then this implies connection gone stale - so update
            if(lastUpdated!=null){

                long lastUpdatedDelta = now.getTime() - lastUpdated.getTime();


                if(lastUpdatedDelta >= timeIntervalInMilliseconds){


                    //It did not update..
                    logger.info("PROBLEM WITH MARKET SUMMARY FEED, restarting feed and clearing caches");
                    RipplerAlgoAutoMultiCurrency.resetMarketSummaries(exchangeClass, distributedCacheInstance);

                    //Restart node service
                    restartMarketSummaryNodeService();
                    lastUpdateDeltaNullCount = 0;

                }else{


                    logger.debug("MARKET SUMMARY FEED ok...");
                }


            }else{

                //do nothing
                logger.info("Last Updated Time not set... count: " +lastUpdateDeltaNullCount);
                lastUpdateDeltaNullCount++;
                if(lastUpdateDeltaNullCount > 20){

                    logger.info("restarting everything again as something not right.. ");
                    //It did not update..
                    logger.info("PROBLEM WITH MARKET SUMMARY FEED, restarting feed and clearing caches");
                    RipplerAlgoAutoMultiCurrency.resetMarketSummaries(exchangeClass, distributedCacheInstance);

                    //Restart node service
                    restartMarketSummaryNodeService();
                    lastUpdateDeltaNullCount = 0;

                }else{

                    //do nothing
                }
            }



        }catch (Exception e){

            logger.error("Error processing", e);

        }

    }


    private void restartMarketSummaryNodeService(){

            //Get the processId of the node summary server
            //kill the process
            // re-run process and confirm it has come up

        logger.info("Executing kill and start script");
        String message = executeCommand(summariesKillScriptLocation);


        logger.info(message + " executed");

    }

    private String executeCommand(String command) {
        try {
            StringBuffer output = new StringBuffer();

            Process p;

                p = Runtime.getRuntime().exec(command);
                p.waitFor();
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                    output.append(line + "\n");
                }



            return output!=null?output.toString():null;

        } catch (Exception e) {
            logger.error("Error executing commandline", e);
        }

        return  null;

    }
}
