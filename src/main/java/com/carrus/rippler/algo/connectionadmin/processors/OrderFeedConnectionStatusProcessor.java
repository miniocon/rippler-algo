package com.carrus.rippler.algo.connectionadmin.processors;


import com.carrus.rippler.algo.RipplerAlgoAutoMultiCurrency;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

public class OrderFeedConnectionStatusProcessor implements Processor {

    final static Logger logger = Logger.getLogger(OrderFeedConnectionStatusProcessor.class);

    private long timeIntervalInMilliseconds  = 10000;

    private Class exchangeClass;
    private String orderFeedKillScriptLocation;
    private int lastUpdateDeltaNullCount = 0;

    public OrderFeedConnectionStatusProcessor(Class exchangeClass, String orderFeedKillScriptLocation){

        this.exchangeClass = exchangeClass;
        this.orderFeedKillScriptLocation = orderFeedKillScriptLocation;


    }


    @Override
    public void process(Exchange exchange)  {   //DO ALL THIS ONLY IF BOOTSTRAP IS COMPLETE...

        try {

            HazelcastInstance distributedCacheInstance = (HazelcastInstance) exchange.getContext().getRegistry().lookupByName("hazelcastInstance");

            IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            Boolean boostrapStatus = bootstrapStatusMap.get(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);

            IMap<String, Date> orderFeedUpdateMap = distributedCacheInstance.getMap(CacheIdentifierHelper.getOrderFeedStatusTimerCache(exchangeClass));

            Date lastUpdated = orderFeedUpdateMap.get(Constants.LAST_ORDER_UPDATE.name());

            Date now = new Date();

            //if now - lastUpdated is >=30 sec - then this implies connection gone stale - so update

            if(boostrapStatus){

                if (lastUpdated != null) {

                    long lastUpdatedDelta = now.getTime() - lastUpdated.getTime();


                    if (lastUpdatedDelta >= timeIntervalInMilliseconds) {


                        //Restart node service
                        restartOrderFeedNodeService();

                        //It did not update..
                        logger.info("PROBLEM WITH ORDER FEED, restarting feed and clearing caches");
                        RipplerAlgoAutoMultiCurrency.resetOrderBooks(exchangeClass, distributedCacheInstance);
                        logger.info("COMPLETED, restarting feed and clearing caches");
                        lastUpdateDeltaNullCount = 0;

                    } else {


                        logger.debug("Order FEED ok...");
                    }


                } else {

                    //do nothing

                    lastUpdateDeltaNullCount++;
                    logger.info("Last Updated Time not set... count: " + lastUpdateDeltaNullCount);

                    if (lastUpdateDeltaNullCount > 60) {
                    //if(false){

                        logger.info("restarting everything again as something not right.. ");
                        //It did not update..
                        logger.info("PROBLEM WITH ORDER FEED, restarting feed and clearing caches");
                        RipplerAlgoAutoMultiCurrency.resetOrderBooks(exchangeClass, distributedCacheInstance);
                        logger.info("COMPLETED, restarting feed and clearing caches");

                        //Restart node service
                        restartOrderFeedNodeService();
                        lastUpdateDeltaNullCount = 0;

                    } else {

                        //do nothing
                    }
                }

            }else{

                //do nothing
            }



        }catch (Exception e){

            logger.error("Error processing", e);

        }

    }


    private void restartOrderFeedNodeService(){

            //Get the processId of the node summary server
            //kill the process
            // re-run process and confirm it has come up

        logger.info("Executing kill and start script");
        String message = executeCommand(orderFeedKillScriptLocation);


        logger.info(message + " executed");

    }

    private String executeCommand(String command) {
        try {
            StringBuffer output = new StringBuffer();

            Process p;

                p = Runtime.getRuntime().exec(command);
                p.waitFor();
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                while ((line = reader.readLine())!= null) {
                    output.append(line + "\n");
                }



            return output!=null?output.toString():null;

        } catch (Exception e) {
            logger.error("Error executing commandline", e);
        }

        return  null;

    }
}
