package com.carrus.rippler.algo;


import com.carrus.rippler.algo.algos.accountmanagement.AccountManager;
import com.carrus.rippler.algo.algos.execution.ExecutionEngine;
import com.carrus.rippler.algo.algos.routes.*;
import com.carrus.rippler.algo.connectionadmin.routes.MarketSummaryConnectionStatusRoute;
import com.carrus.rippler.algo.connectionadmin.routes.OrderFeedConnectionStatusRoute;
import com.carrus.rippler.algo.model.Constants;
import com.carrus.rippler.algo.model.MarketSummary;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookManager;
import com.carrus.rippler.algo.orderbookmanagement.OrderBookUpdateRoutes.*;
import com.carrus.rippler.algo.supportedexchanges.SupportedExchangesManager;
import com.carrus.rippler.algo.utils.CacheIdentifierHelper;
import com.carrus.rippler.algo.utils.MessageBusTopicIdentifierHelper;
import com.hazelcast.config.Config;
import com.hazelcast.config.ManagementCenterConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.camel.main.Main;
import org.apache.log4j.Logger;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.trade.LimitOrder;
import org.knowm.xchange.dto.trade.OpenOrders;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParam;
import org.knowm.xchange.service.trade.params.orders.DefaultOpenOrdersParamCurrencyPair;
import org.knowm.xchange.service.trade.params.orders.OpenOrdersParams;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by usmankhan on 25/12/2017.
 */
public class RipplerAlgoAutoMultiCurrency {

    final static Logger logger = Logger.getLogger(RipplerAlgoAutoMultiCurrency.class);

    public final static String RIPPLER_ALGO_PROPERTIES_FILE_NAME = "rippleralgo.properties";
    public final static String EXCHANGE_PROPERTIES_FILE_NAME = "exchange.properties";

    final static String KAFKA_HOST = "kafka.host";
    final static String KAFKA_PORT = "kafka.port";

    public final static String BOOTSTRAP_STATUS_KEY = "boostrapStatusMap";
    public static Boolean BOOTSTRAP_COMPLETE_FLAG = false;

    public static AccountManager bitterexAccountManager;

    public static ExecutionEngine bittrexExecutionEngine;

    public static SupportedExchangesManager sem;

    public static OrderBookManager orderBookManager;

    public static List<CurrencyPair> currencyPairs;

    public static Main camelMainComponent;

    public static HazelcastInstance distributedCacheInstance;


    public static Map<String, String > currencyPairIgnoreList = new HashMap<String, String>();

    public static Map<String, BigDecimal> CURRENCY_PAIR_MULTIPLES = new HashMap<String, BigDecimal>();

    public static Map<String, BigDecimal> CURRENCY_PAIR_DT_THRESHOLDS = new HashMap<String, BigDecimal>();

    public static BigDecimal DEFAULT_CURRENCY_PAIR_MUTLIPLE = new BigDecimal(3);

    public static BigDecimal DEFAULT_CURRENCY_PAIR_DT_THRESHOLDS = new BigDecimal(1);

    //TODO: create map that maps market cap to CURRENCY PAIR MULTIPLE??


    static{

        currencyPairIgnoreList.put("GAM/BTC", "GAM/BTC");
        currencyPairIgnoreList.put("SWIFT/BTC", "SWIFT/BTC");
        currencyPairIgnoreList.put("ARDR/BTC", "ARDR/BTC");
        currencyPairIgnoreList.put("SHIFT/BTC", "SHIFT/BTC");
        currencyPairIgnoreList.put("CLUB/BTC", "CLUB/BTC");
        currencyPairIgnoreList.put("XVC/BTC", "XVC/BTC");

    }



        public static void main(String [] args) {

            logger.info("Starting Rippler Algo..");

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String exchangePropsPath = rootPath + EXCHANGE_PROPERTIES_FILE_NAME;
            String ripplerAlgoPropsPath = rootPath + RIPPLER_ALGO_PROPERTIES_FILE_NAME;


            Properties exchangeProps = new Properties();
            Properties ripplerAlgoProps = new Properties();

            try {
                exchangeProps.load(new FileInputStream(exchangePropsPath));
                ripplerAlgoProps.load(new FileInputStream(ripplerAlgoPropsPath));
                String bittrexApiKey = exchangeProps.getProperty("BittrexExchange.api.key");
                String bittrexApiSecretKey = exchangeProps.getProperty("BittrexExchange.api.secret");
                String summariesKillScriptLocation = ripplerAlgoProps.getProperty("marketsummaries.killscript.location");
                String orderbookKillScriptLocation = ripplerAlgoProps.getProperty("orderbook.killscript.location");


                //Setup Distributed Data Cache
                Config cfg = new Config();
                ManagementCenterConfig manCfg = new ManagementCenterConfig();
                manCfg.setUrl("http://localhost:8080/hazelcast-mancenter");

                cfg.setInstanceName("sigma");
                cfg.setManagementCenterConfig(manCfg);
                distributedCacheInstance = Hazelcast.newHazelcastInstance(cfg);

                IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(BOOTSTRAP_STATUS_KEY);
                bootstrapStatusMap.clear();
                bootstrapStatusMap.put(BOOTSTRAP_STATUS_KEY, BOOTSTRAP_COMPLETE_FLAG);

                ////This is an AccountManget for Mani@carrusltd.com Todo - get list of Wallet Ids and create an Account Manager per wallet id - for now supply a null walletId and Account Info will defauly to the first wallet
                bitterexAccountManager = new AccountManager(BittrexExchange.class, bittrexApiKey, bittrexApiSecretKey, distributedCacheInstance, null);



                bittrexExecutionEngine = new ExecutionEngine(bitterexAccountManager);




                /**Start Camel, bind the distributed cache and start routes for updating OrderBooks in realtime and performing Algo based analysis of order books and trade execution**/
                camelMainComponent = new Main();
                camelMainComponent.bind("hazelcastInstance", distributedCacheInstance);


               setupSigma03Bot(camelMainComponent, null);


                /** Setup Market Summaries - MARKET SUMMARIES ANALYSIS -- REMEMBER TO TURN BOOTSTRAP OVERRIDE TO FALSE WHEN RUNNIGN THIS WITH ALGO!!! !**/
                setupMarketSummaries(BittrexExchange.class, camelMainComponent, ripplerAlgoProps, "log:console", distributedCacheInstance, summariesKillScriptLocation, bootstrapStatusMap,false);


                /** Setup supported Exchanges ***/
                sem = new SupportedExchangesManager(distributedCacheInstance);
                sem.initialiseExchanges();
                orderBookManager = new OrderBookManager(distributedCacheInstance);


                currencyPairs =  bitterexAccountManager.getExchange().getExchangeSymbols();



                setUpAlgos(camelMainComponent,bittrexExecutionEngine, bitterexAccountManager);






                //Setup order books
                setUpOrderBookRoutes(currencyPairs, camelMainComponent, orderBookManager, ripplerAlgoProps, sem, orderbookKillScriptLocation);

                camelMainComponent.addRouteBuilder(new BootstrapOrdersRoute(BittrexExchange.class,orderBookManager,sem,currencyPairs));


                logger.info("Setting up Bootstraps per currency pair");


                int currencyCount=0;
                for(CurrencyPair currencyPair : currencyPairs){

                    if (RipplerAlgoAutoMultiCurrency.currencyPairIgnoreList.get(currencyPair.toString()) == null) {

                        if(currencyPair.counter.equals(Currency.BTC)) {  //TODO::  UPDATED IN OTHER METHODS AS NEEDED

                            bootStrapOrderBookForCurrencyPair(orderBookManager, sem, currencyPair);

                            currencyCount++;
                        }else{
                            //not supported currency pair
                        }

                    }else{

                        logger.info("ignoring bootstrap for currency: " + currencyPair);
                    }
                    //Thread.sleep(1000);
                }

                logger.info("Bootstraps per currency pair completed for " + currencyCount + " currency pairs");


                logger.info("Setting up momemtum indicators for currency pairs");


                //Momemtum Indicators line was here..

                setUpBidAskSpreadMomentumIndicatorRoutes(currencyPairs);






                camelMainComponent.enableTrace();
                camelMainComponent.run(args);




            } catch (Exception e) {

                logger.error("Error Starting Rippler Algo for multiple currencies", e);
            }
        }


        public static void setUpBidAskSpreadMomentumIndicatorRoutes(List<CurrencyPair> currencyPairs){

            int currencyCount=0;
            logger.info("Setting up BidASK Momentum Indicators for currency pairs ");
            for(CurrencyPair currencyPair : currencyPairs){
                if (currencyPairIgnoreList.get(currencyPair.toString()) == null) {

                    //TODO: Change target endpoint to the ALGO - Indicator will generate a signal..
                    camelMainComponent.addRouteBuilder(new BidAskSpreadMomentumIndicatorRoute(BittrexExchange.class, currencyPair,"direct:signals"));
                    currencyCount++;

                }else{

                    logger.info("Ignoring currency pair " + currencyPair);
                }


            }

            logger.info("Momentum indicators created for " + currencyCount + " currency pairs");


        }



        public static void setUpOrderBookRoutes(List<CurrencyPair> currencyPairs, Main camelMainComponent, OrderBookManager orderBookManager, Properties ripplerAlgoProps, SupportedExchangesManager sem, String orderbookKillScriptLocation){

            int currencyCount=0;
            logger.info("Setting up orderbook update and metric collection Subscriptions for the following currency pairs: ");
            for(CurrencyPair currencyPair : currencyPairs){



                if (currencyPairIgnoreList.get(currencyPair.toString()) == null) {

                    if(currencyPair.counter.equals(Currency.BTC)) {

                        String aBidsTargetEndpoint = MessageBusTopicIdentifierHelper.getOrderBookTargetTopicIdentifier(BittrexExchange.class, currencyPair, Constants.BID);
                        String aAsksTargetEndpoint = MessageBusTopicIdentifierHelper.getOrderBookTargetTopicIdentifier(BittrexExchange.class, currencyPair, Constants.ASK);


                        camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(aBidsTargetEndpoint, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.BIDS_SUM_ORDER_BOOK), Constants.BID));
                        camelMainComponent.addRouteBuilder(new OrderBookMetricCollectorRoute(aAsksTargetEndpoint, CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, currencyPair, Constants.ASKS_SUM_ORDER_BOOK), Constants.ASK));

                        setupOrderBookUpdateEndPointsForCurrencyPair(orderBookManager, ripplerAlgoProps, sem, camelMainComponent, currencyPair, "direct:" + aBidsTargetEndpoint, "direct:" + aAsksTargetEndpoint);
                        currencyCount++;
                    }else{

                        /*******N O T E  !!!!!!!    Not a BTC based currency.. Only supporting BTC currencies at the moment - IF undoing, also undo in the reBootStrapOrderBooks method ****/
                    }


                    //logger.info(currencyCount++ + " " + currencyPair);
                } else {

                    logger.info("Ignoring currency pair " + currencyPair);
                }



            }

            camelMainComponent.addRouteBuilder(new OrderFeedConnectionStatusRoute(BittrexExchange.class, orderbookKillScriptLocation));

            logger.info("OrderBook update and metric subscriptions created for " + currencyCount + " currency pairs");


        }


    public static void resetOrderBooks(Class exchange, HazelcastInstance distributedCache){

        //Clear all orderbook caches
        //cycle through all orderbook caches and clear them

        IMap<String, Boolean> bootstrapStatusMap = distributedCache.getMap(BOOTSTRAP_STATUS_KEY);

        bootstrapStatusMap.put(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY, false);

        IMap<String, Date> orderBookFeedUpdateMap = distributedCache.getMap(CacheIdentifierHelper.getOrderFeedStatusTimerCache(exchange));
        orderBookFeedUpdateMap.clear();

        //Trigger cache clearing and bootstrapping

        reBootStrapOrderBooks(exchange);

        //camelMainComponent.addRouteBuilder(new BootstrapOrdersRoute(BittrexExchange.class,orderBookManager,sem,currencyPairs));

        bootstrapStatusMap.put(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY, true);


    }

    public static void reBootStrapOrderBooks(Class exchangeName){


        IMap<String, Boolean> bootstrapStatusMap = distributedCacheInstance.getMap(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY);



        try {

            logger.info("Setting up Bootstraps per currency pair");
            //Loop through currency pairs and bootstrap

            int currencyCount = 0;
            for (CurrencyPair currencyPair : currencyPairs) {


                if (RipplerAlgoAutoMultiCurrency.currencyPairIgnoreList.get(currencyPair.toString()) == null) {

                    if(currencyPair.counter.equals(Currency.BTC)) {

                        sem.setUpBootstrapOrderBookForCurrencyPairAndExchange(exchangeName.getName(), currencyPair, orderBookManager);
                        currencyCount++;
                        logger.info("Just Completed bootstrap of " + currencyCount + " " + currencyPair);
                    }else{
                        // ONLY BTC SUPPORTED FOR NOW
                    }

                }else{
                    //do nothing
                }


            }

            logger.info("Just Completed Bootstraps for : " + currencyCount + " currency pairs");

            bootstrapStatusMap.put(RipplerAlgoAutoMultiCurrency.BOOTSTRAP_STATUS_KEY, true);

        }catch(Exception e){

            logger.error("Failed to bootstrap orders", e);
        }



    }

    public static void resetMarketSummaries(Class exchange, HazelcastInstance distributedCache){

        IMap<String, BigDecimal> exchangeVolumesPerCurrencyPair = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolumeTrackerId(exchange));
        exchangeVolumesPerCurrencyPair.clear();

        IMap<String, Date> marketSummaryUpdateMap = distributedCache.getMap(CacheIdentifierHelper.getMarketSummaryStatusTimerCache(exchange));
        marketSummaryUpdateMap.clear();

        IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5SecROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume5SecROCTrackerId(exchange));
        exchangeVolumesPerCurrencyPair5SecROC.clear();

        IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair50SecROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume50SecROCTrackerId(exchange));
        exchangeVolumesPerCurrencyPair50SecROC.clear();

        IMap<String, BigDecimal>  exchangeVolumesPerCurrencyPair5MinROC = distributedCache.getMap(CacheIdentifierHelper.getExchangeVolume5minSecROCTrackerId(exchange));
        exchangeVolumesPerCurrencyPair5MinROC.clear();

        IMap<String, MarketSummary> marketSummaryMap = distributedCache.getMap(CacheIdentifierHelper.getMarketSummaryMapId(exchange));
        marketSummaryMap.clear();

        IMap<String, Integer> currencyAggregatedSignalCountMap = distributedCache.getMap(CacheIdentifierHelper.getExchangeAggregatedSignalCount(exchange));
        currencyAggregatedSignalCountMap.clear();

    }

    private static void setupMarketSummaries(Class exchange, Main camelMainComponent, Properties ripplerAlgoProps, String targetEndpoint, HazelcastInstance distributedCache,  String summariesKillScriptLocation, IMap<String, Boolean> bootstrapStatusMap,  boolean bootstrapFlagCheckOverride){

        if(bootstrapFlagCheckOverride){

            bootstrapStatusMap.put(BOOTSTRAP_STATUS_KEY, true);
        }else{
            //do nothing
        }

        resetMarketSummaries(exchange, distributedCache);


        //Create the route to consume exchange volumes in real time
        camelMainComponent.addRouteBuilder(new MarketSummariesRoute(exchange, MessageBusTopicIdentifierHelper.getSummariesTopicIdentifier(exchange), ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), "summaries11", targetEndpoint));
        //create & clear the volume cache for this exchange

        //TODO: Change log;console endpoint to a kafka topic that the GUI will subscribe too..
        camelMainComponent.addRouteBuilder(new MarketSummaries5SecVolROCRoute(exchange, "kafka:cryptoalgoalerts2"));

        //camelMainComponent.addRouteBuilder(new MarketSummaries5SecVolROCRoute(exchange, "direct:cryptoalgoalertsAggregated"));


        camelMainComponent.addRouteBuilder(new MarketSummaries5SecAggregatorVolROCRoute(exchange, "log:console"));


        camelMainComponent.addRouteBuilder(new MarketSummaryConnectionStatusRoute(exchange,summariesKillScriptLocation));



        //camelMainComponent.addRouteBuilder(new MarketSummaries5SecVolROCStricterRoute(exchange, "kafka:cryptoalgoalertsStrict"));


        //camelMainComponent.addRouteBuilder(new MarketSummaries50SecVolROCRoute(exchange, "log:console"));
        //camelMainComponent.addRouteBuilder(new MarketSummaries5MinVolROCRoute(exchange, "log:console"));

    }

    private static void setupSigma03Bot(Main camelMainComponent, String walletId){

        /**Listens for reporting commands from the chat bot and retrieves results from the IMDG e.g. Pnl, status, balance**/
        camelMainComponent.addRouteBuilder(new Sigma03BotRoute("kafka:sigma03botcommands?groupId=group10&autoOffsetReset=latest&consumersCount=1", "kafka:sigma03Signals2", BittrexExchange.class, walletId));

        /** Listens for Algo commands like buy btc-xrp or sell or pause - sends on to Algo as a signal **/
        camelMainComponent.addRouteBuilder(new Sigma03BotAlgoSignalGeneratorRoute("kafka:sigma03botcommands?groupId=group11&autoOffsetReset=latest&consumersCount=1", "direct:signals", BittrexExchange.class));

        /**Listens to Algo's output and sends on to chat bot **/
        camelMainComponent.addRouteBuilder(new Sigma03BoAlgoSignalResponseConsumertRoute("direct:sigma03botAlgoUpdates", "kafka:sigma03Signals2", BittrexExchange.class));
    }

    private static void setUpAlgos(Main camelMainComponent, ExecutionEngine executionEngine, AccountManager accountManager){

        //camelMainComponent.addRouteBuilder(new BittrexTrinityAlgoRoute(BittrexExchange.class, "direct:cryptoalgoalertsAggregated", accountManager, executionEngine));

        camelMainComponent.addRouteBuilder(new Sigma03AlgoOneRoute(BittrexExchange.class, "direct:signals", accountManager, executionEngine, "direct:sigma03botAlgoUpdates"));


    }

    /** NOTE: Bootstrapping after camel process is run, doesnt work**/
    private static void bootStrapOrderBookForCurrencyPair(OrderBookManager orderBookManager,  SupportedExchangesManager sem,  CurrencyPair internalCurrencyPairId) {
        //Setup Bittrex OrderBook for BTCNEO
        sem.setUpBootstrapOrderBookForCurrencyPairAndExchange(BittrexExchange.class.getName(), internalCurrencyPairId, orderBookManager);

    }



    private static void setupOrderBookUpdateEndPointsForCurrencyPair(OrderBookManager orderBookManager, Properties ripplerAlgoProps, SupportedExchangesManager sem, Main camelMainComponent, CurrencyPair internalCurrencyPairId, String aBidsTargetEndpoint, String aAsksTargetEndpoint) {
        //Setup Bittrex OrderBook for BTCNEO
        //Setup Message Bus Topic for processing OrderBook updates for BTCNEO - will be same for both Bid/Ask - they will recieve same message in parallel, so can process in parallel
        String currencyPairOrderBookUpdateMessageBusTopic = MessageBusTopicIdentifierHelper.getTopicIdentifer(BittrexExchange.class, internalCurrencyPairId, null);
        String currencyPairBidOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.BIDS_ORDER_BOOK);
        String currencyPairBidSumsOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.BIDS_SUM_ORDER_BOOK);
        String currencyPairAskOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.ASKS_ORDER_BOOK);
        String currencyPairAskSumsOrderBookCacheId =  CacheIdentifierHelper.getCacheIdentifer(BittrexExchange.class, internalCurrencyPairId , Constants.ASKS_SUM_ORDER_BOOK);

        logger.info(currencyPairOrderBookUpdateMessageBusTopic);
        logger.info(aBidsTargetEndpoint);
        logger.info(aAsksTargetEndpoint);

        camelMainComponent.addRouteBuilder(new BidOrderBookUpdateRoute(currencyPairOrderBookUpdateMessageBusTopic, "group1", currencyPairBidOrderBookCacheId,currencyPairBidSumsOrderBookCacheId, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT),aBidsTargetEndpoint, BittrexExchange.class));
        camelMainComponent.addRouteBuilder(new AskOrderBookUpdateRoute(currencyPairOrderBookUpdateMessageBusTopic,"group2", currencyPairAskOrderBookCacheId, currencyPairAskSumsOrderBookCacheId, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), aAsksTargetEndpoint, BittrexExchange.class));



    }


    private static void setupFilledOrderBookForCurrencyPair(Properties ripplerAlgoProps, Main camelMainComponent, CurrencyPair internalCurrencyPairId, String filledTargetEndpoint,  BigDecimal exchangeFee, BigDecimal exchangeInverseFee) {

        String currencyPairOrderBookUpdateMessageBusTopic = MessageBusTopicIdentifierHelper.getTopicIdentifer(BittrexExchange.class, internalCurrencyPairId, null);

        String buysVolumeForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencyBuysVolumeMapId(BittrexExchange.class, internalCurrencyPairId);
        String sellsVolumeForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencySellsVolumeMapId(BittrexExchange.class, internalCurrencyPairId);


        String buysFillCountForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencyBuysFillCountMapId(BittrexExchange.class, internalCurrencyPairId);
        String sellsFillCountForExchangeCurrencyPair = CacheIdentifierHelper.getExchangeCurrencySellsFillCountMapId(BittrexExchange.class, internalCurrencyPairId);

        //*** GETs FILLED ORDER UPDATES FOR PROCESSING***//
        camelMainComponent.addRouteBuilder(new FilledOrderBookUpdateRoute(internalCurrencyPairId,currencyPairOrderBookUpdateMessageBusTopic,"group3",buysVolumeForExchangeCurrencyPair , sellsVolumeForExchangeCurrencyPair, buysFillCountForExchangeCurrencyPair, sellsFillCountForExchangeCurrencyPair, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), filledTargetEndpoint, exchangeFee, exchangeInverseFee));
        camelMainComponent.addRouteBuilder(new FilledOrderBookROCRoute(internalCurrencyPairId,currencyPairOrderBookUpdateMessageBusTopic,"group4",buysVolumeForExchangeCurrencyPair , sellsVolumeForExchangeCurrencyPair, buysFillCountForExchangeCurrencyPair, sellsFillCountForExchangeCurrencyPair, ripplerAlgoProps.getProperty(KAFKA_HOST), ripplerAlgoProps.getProperty(KAFKA_PORT), filledTargetEndpoint, exchangeFee, exchangeInverseFee));

    }



    private static void testExtractionOfOpenOrders(){

        ///TESTING!!!!

        OpenOrdersParams defaultOpenOrdersParams = new DefaultOpenOrdersParam() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };

        DefaultOpenOrdersParamCurrencyPair defaultOpenOrdersParamCurrencyPair = new DefaultOpenOrdersParamCurrencyPair(CurrencyPair.ETH_BTC);

        OpenOrders openOrders= null;
        try {
            openOrders = bitterexAccountManager.getExchange().getTradeService().getOpenOrders(defaultOpenOrdersParamCurrencyPair);

        } catch (IOException e) {
            logger.error(e);
        }

        List<LimitOrder> userOpenOrders = openOrders.getOpenOrders();

        logger.info("Open Order Test...");

        for(LimitOrder limitOrder : userOpenOrders){


            logger.info("limitOrder id" + limitOrder.getId());


        }

        logger.info("Open Order Test... COMPLETE");
        ////END TESTING !!!
    }





}
