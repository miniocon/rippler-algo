package com.carrus.rippler.algo.supportedexchanges;

import com.carrus.rippler.algo.orderbookmanagement.OrderBookManager;
import com.hazelcast.core.HazelcastInstance;
import org.apache.log4j.Logger;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.service.marketdata.MarketDataService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by usmankhan on 21/10/2017.
 */
public class SupportedExchangesManager {


    final static Logger logger = Logger.getLogger(SupportedExchangesManager.class);

    public Map<String, Exchange> exchangeInstances = new HashMap<String, Exchange>();



    private static Map<String, Class> exchanges = new HashMap<String, Class>();


    //EXCHANGES ARE NAMED HERE
    static{

        exchanges.put(BittrexExchange.class.getName(), BittrexExchange.class);

    }

    private HazelcastInstance instance;

    public SupportedExchangesManager(HazelcastInstance instance){
        super();
        this.instance = instance;
    }

    public void initialiseExchanges(){

        logger.info("Initialising Exchanages..");

        for(String exchangeClassName : exchanges.keySet()){

           Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exchangeClassName);

            exchangeInstances.put(exchangeClassName, exchange);
            logger.info("Exchange initialised: " + exchangeClassName);


        }


    }


    public MarketDataService getMarketDataServiceForExchange(String exchangeClassName){

        Exchange exchange =  exchangeInstances.get(exchangeClassName);

        if(exchange!=null){
            logger.info("Obtained MarketService for Exchange: " + exchangeClassName);

            return  exchange.getMarketDataService();
        }else{

            logger.error("Exchange not Found: " + exchangeClassName);
            return null;
        }


    }

    public List<CurrencyPair> getSupportedCurrencyPairsForExchange(String exchangeClassName){


        Exchange exchange =  exchangeInstances.get(exchangeClassName);

        if(exchange!=null){
            logger.info("Obtaining currency pairs for Exchange: " + exchangeClassName);


            return exchange.getExchangeSymbols();


        }else{

            logger.error("Exchange not Found: " + exchangeClassName);
            return null;

        }

    }


    public void setUpBootstrapOrderBookForCurrencyPairAndExchange(String exchangeClassName, CurrencyPair currencyPair, OrderBookManager orderBookManager){

       logger.info("**** BOOTSTRAPPING ORDER BOOKS FOR " + exchangeClassName + " , " + currencyPair);

        MarketDataService marketDataService = getMarketDataServiceForExchange(exchangeClassName);


        orderBookManager.setupRipplerVirtualOrderBookForExchangeAndCurrencyPair(exchanges.get(exchangeClassName), currencyPair, marketDataService);

        logger.info("**** COMPLETED BOOTSTRAPPING ORDER BOOKS FOR " + exchangeClassName + " , " + currencyPair);

    }





}
