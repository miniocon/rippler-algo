package com.carrus.rippler.algo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class OrderBookUpdate {

    private final String marketName;
    private final Integer nounce;
    private final LimitOrderUpdate[] asks;
    private final LimitOrderUpdate[] bids;
    private final FillOrderUpdate[] fills;



    public OrderBookUpdate(
                            @JsonProperty("Sells") LimitOrderUpdate[] asks,
                            @JsonProperty("Buys") LimitOrderUpdate[] bids,
                            @JsonProperty("Fills") FillOrderUpdate[] fills,
                            @JsonProperty("MarketName") String marketName,
                            @JsonProperty("Nounce") Integer nounce

    ) {
        this.asks = asks;
        this.bids = bids;
        this.fills = fills;
        this.marketName = marketName;
        this.nounce = nounce;
    }

    public LimitOrderUpdate[] getAsks() {
        return this.asks;
    }

    public LimitOrderUpdate[] getBids() {
        return this.bids;
    }


    public String getMarketName() {
        return this.marketName;
    }

    public Integer getNounce() {
        return this.nounce;
    }

    public FillOrderUpdate[] getFills() {
        return this.fills;
    }





    public String toString() {
        return "OrderBookUpdate " + "MarketName= " + this.marketName + ", Nounce=" + this.nounce + ", [asks=" + Arrays.toString(this.asks) + ", bids=" + Arrays.toString(this.bids) + ", fills="+ Arrays.toString(this.fills) +"]";
    }
}
