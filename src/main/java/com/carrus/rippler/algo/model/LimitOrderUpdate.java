package com.carrus.rippler.algo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by usmankhan on 03/09/2017.
 */
public class LimitOrderUpdate  {

    private final Integer type;
    private final BigDecimal rate;
    private final BigDecimal quantity;


    public LimitOrderUpdate(@JsonProperty("Type") Integer type,
                            @JsonProperty("Rate") BigDecimal rate,
                            @JsonProperty("Quantity") BigDecimal quantity ){


        this.type = type;
        this.rate = rate;
        this.quantity = quantity;
    }

    public Integer getType() {
        return type;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return
        " LimitOrderUpdate [rate=" + rate + ", quantity=" + this.quantity + "] , type=" + type
                ;
    }
}
