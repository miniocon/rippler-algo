package com.carrus.rippler.algo.model;

/**
 * Created by usmankhan on 21/10/2017.
 */
public enum Constants {


    BIDS_ORDER_BOOK,
    ASKS_ORDER_BOOK,
    BIDS_SUM_ORDER_BOOK,
    ASKS_SUM_ORDER_BOOK,
    BID,
    ASK,
    BUY,
    SELL,
    TOP_10_SUM,
    TOP_100_SUM,
    BEST_LEVEL,
    BEST_QTY,
    ALGO_METRICS_CACHE,
    BUY_OPPORTUNITY_FOR_MULTIPLES_STRATEGY_DETECTOR_CACHE,
    LAST_MARKET_SUMMARY_UPDATE,
    LAST_ORDER_UPDATE,
    REPORT_TO_SEND_TO_BOT

}
