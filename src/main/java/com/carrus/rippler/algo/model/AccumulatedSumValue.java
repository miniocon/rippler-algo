package com.carrus.rippler.algo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by usmankhan on 24/09/2017.
 */

public class AccumulatedSumValue {

    @JsonProperty("date")
    private String date;

    @JsonProperty("sum")
    private BigDecimal sum;

    public AccumulatedSumValue(){
        super();

    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }
}
