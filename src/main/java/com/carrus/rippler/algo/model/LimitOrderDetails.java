package com.carrus.rippler.algo.model;

import org.knowm.xchange.currency.CurrencyPair;

import java.math.BigDecimal;

public class LimitOrderDetails {

    private Enum orderType;
    private CurrencyPair pair;
    private BigDecimal quantity;
    private BigDecimal price;

    public LimitOrderDetails(Enum orderType, CurrencyPair pair, BigDecimal quantity, BigDecimal price){

        this.orderType = orderType;
        this.pair = pair;
        this.quantity = quantity;
        this.price = price;
    }


    public Enum getOrderType() {
        return orderType;
    }

    public void setOrderType(Enum orderType) {
        this.orderType = orderType;
    }

    public CurrencyPair getPair() {
        return pair;
    }

    public void setPair(CurrencyPair pair) {
        this.pair = pair;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
