package com.carrus.rippler.algo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by usmankhan on 03/09/2017.
 */
public class FillOrderUpdate {


    private final String orderType;
    private final BigDecimal rate;
    private final BigDecimal quantity;
    private final Date timeStamp;


    public FillOrderUpdate(@JsonProperty("OrderType") String orderType,
                           @JsonProperty("Rate") BigDecimal rate,
                           @JsonProperty("Quantity") BigDecimal quantity,
                           @JsonProperty("TimeStamp") Date timeStamp
    ) {
        this.rate = rate;
        this.quantity = quantity;
        this.orderType = orderType;
        this.timeStamp = timeStamp;

    }

    public BigDecimal getPrice() {
        return this.rate;
    }

    public BigDecimal getAmount() {
        return this.quantity;
    }

    public String getOrderType(){return this.orderType; }

    public String toString() {
        return "FillOrderUpdate [rate=" + this.rate + ", quantity=" + this.quantity + ", orderType="+this.orderType + ", timestamp="+this.timeStamp +"]";
    }



}
