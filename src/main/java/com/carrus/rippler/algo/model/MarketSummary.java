package com.carrus.rippler.algo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * Created by usmankhan on 26/11/2017.
 *
 * {
 *
 * "MarketName":"USDT-ETC",
 * "High":22.30547962,
 * "Low":20.59032633,
 * "Volume":63712.9171552,
 * "Last":21.61002172,
 * "BaseVolume":1368696.64930287,
 * "TimeStamp":"2017-11-26T15:00:46.09",
 * "Bid":21.61002172,"Ask":21.749,
 * "OpenBuyOrders":893,
 * "OpenSellOrders":1298,
 * "PrevDay":22.12781341,
 * "Created":"2017-07-14T17:10:10.72"
 *
 * }]
 *
 *
 *
 */
public class MarketSummary implements Serializable {


    private static final long serialVersionUID = 4288846657244451756L;

    private String MarketName;
    private BigDecimal High;
    private BigDecimal Low;
    private BigDecimal Volume;
    private BigDecimal Last;
    private BigDecimal BaseVolume;
    private String TimeStamp;
    private BigDecimal Bid;
    private BigDecimal OpenBuyOrders;
    private BigDecimal OpenSellOrders;
    private BigDecimal PrevDay;
    private String Created;

    public MarketSummary( @JsonProperty("MarketName") String MarketName,
                          @JsonProperty("High") BigDecimal High,
                          @JsonProperty("Low") BigDecimal Low,
                          @JsonProperty("Volume") BigDecimal Volume,
                          @JsonProperty("Last") BigDecimal Last,
                          @JsonProperty("BaseVolume") BigDecimal BaseVolume,
                          @JsonProperty("TimeStamp") String TimeStamp,
                          @JsonProperty("Bid") BigDecimal Bid,
                          @JsonProperty("OpenBuyOrders") BigDecimal OpenBuyOrders,
                          @JsonProperty("OpenSellOrders") BigDecimal OpenSellOrders,
                          @JsonProperty("PrevDay") BigDecimal PrevDay,
                          @JsonProperty("Created") String Created
                          ){

        this.MarketName = MarketName;
        this.High = High;
        this.Low = Low;
        this.Volume = Volume;
        this.Last = Last;
        this.BaseVolume = BaseVolume;
        this.TimeStamp = TimeStamp;
        this.Created = Created;
        this.Bid = Bid;
        this.OpenBuyOrders = OpenBuyOrders;
        this.OpenSellOrders = OpenSellOrders;
        this.PrevDay = PrevDay;
        this.Created = Created;


    }

    public String getMarketName() {
        return MarketName;
    }

    public void setMarketName(String marketName) {
        MarketName = marketName;
    }

    public BigDecimal getHigh() {
        return High;
    }

    public void setHigh(BigDecimal high) {
        High = high;
    }

    public BigDecimal getLow() {
        return Low;
    }

    public void setLow(BigDecimal low) {
        Low = low;
    }

    public BigDecimal getVolume() {
        return Volume;
    }

    public void setVolume(BigDecimal volume) {
        Volume = volume;
    }

    public BigDecimal getLast() {
        return Last;
    }

    public void setLast(BigDecimal last) {
        Last = last;
    }

    public BigDecimal getBaseVolume() {
        return BaseVolume;
    }

    public void setBaseVolume(BigDecimal baseVolume) {
        BaseVolume = baseVolume;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public BigDecimal getBid() {
        return Bid;
    }

    public void setBid(BigDecimal bid) {
        Bid = bid;
    }

    public BigDecimal getOpenBuyOrders() {
        return OpenBuyOrders;
    }

    public void setOpenBuyOrders(BigDecimal openBuyOrders) {
        OpenBuyOrders = openBuyOrders;
    }

    public BigDecimal getOpenSellOrders() {
        return OpenSellOrders;
    }

    public void setOpenSellOrders(BigDecimal openSellOrders) {
        OpenSellOrders = openSellOrders;
    }

    public BigDecimal getPrevDay() {
        return PrevDay;
    }

    public void setPrevDay(BigDecimal prevDay) {
        PrevDay = prevDay;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }


}
